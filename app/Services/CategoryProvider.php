<?php

namespace App\Services;

use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;

class CategoryProvider {

    public function getAll(){
        $data = Category::with('attributes.values','children')->get();

        $results = [];

        foreach ($data as $id => $cat){

            $results[$id]['name'] = $cat->name;
            $results[$id]['slug'] = $cat->slug;
            $results[$id]['attributes'] = $cat->attributes;
            $results[$id]['icon'] = $cat->icon;
            $results[$id]['banner_square'] = $cat->banner_square;
            $results[$id]['banner_long'] = $cat->banner_long;

            $results[$id]['children'] = [];

            foreach($cat->children as $childId => $child){
                $results[$id]['children'][$childId]['name'] = $child->name;
                $results[$id]['children'][$childId]['icon'] = $cat->icon;
                $results[$id]['children'][$childId]['banner_square'] = $cat->banner_square;
                $results[$id]['children'][$childId]['banner_long'] = $cat->banner_long;
                $results[$id]['children'][$childId]['slug'] = $child->slug;
                $results[$id]['children'][$childId]['attributes'] = $child->attributes;
            }
        }

        return $results;
    }

    public function getCategoryAttributes($slug){

        if($slug=='all' || $slug == null )
            $cat[] = Category::with('attributes.values','children')->first();
        else
            $cat[] = Category::with('attributes.values','children')->where('slug',$slug)->first();

        $results = [];

        if($cat){

            foreach ($cat as $categ){

                $loopitem = $categ->attributes;

                foreach ($loopitem as $id => $cat){

                    $results[$id]['name'] = $cat->name;
                    $results[$id]['slug'] = $cat->slug;

                    foreach ($cat->values as $valId => $value){
                        $results[$id]['values'][$valId]['name'] = $value->value;
                        $results[$id]['values'][$valId]['id'] = $value->id;
                    }

                }
            }
        }

        return $results;
    }
}
