<?php
namespace App\Exports;

use App\Models\Subscriber;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SubscriberExports implements FromView
{
    use Exportable;

    public function view(): View
    {
        return view('exports.subscribers', [
            'data' => Subscriber::all()
        ]);
    }
}
