<?php
namespace App\Exports;

use App\Models\ContactEntry;
use App\Models\Subscriber;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ContactExports implements FromView
{
    use Exportable;

    public function view(): View
    {
        $results = ContactEntry::where('source','contact-us')->orWhere('source','propert-listings')->with('items')->orderBy('created_at','DESC')->get();

        $data = [];
        $count = 0;

        foreach ($results as $id=>$result){
            foreach ($result->items as $valId=>$item)
                $data['rows'][$count]['items'][$item->key] = $item->value;

            $data['rows'][$count]['id'] = $result->id;
            $data['rows'][$count]['date'] = $result->created_at;

            $count++;
        }

        $subscribers = Subscriber::orderBy('id','DESC')->get();

        foreach ($subscribers as $id=>$subscriber){
            $data['rows'][$count]['items']['type'] = 'newsletter';
            $data['rows'][$count]['items']['name'] = null;
            $data['rows'][$count]['items']['email'] = $subscriber->email;
            $data['rows'][$count]['id'] = $subscriber->id;
            $data['rows'][$count]['date'] = $subscriber->created_at;

            $count++;
        }

        $data['headers'] = ['date','type','name','email','phone','country','message'];

        array_multisort(array_map('strtotime',array_column($data['rows'],'date')),
            SORT_DESC,
            $data['rows']);

        return view('exports.contacts', [
            'data' => $data
        ]);
    }

}
