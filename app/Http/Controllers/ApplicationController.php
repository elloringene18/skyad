<?php

namespace App\Http\Controllers;

use App\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ApplicationController extends Controller
{
    public function store(Request $request){
        $honeypot = $request->input('user_id');

        if($honeypot)
            return redirect()->back();

        $type = $request->input('type');

        $request->validate([
            'contact' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required|max:500',
            'cv' => 'required|file|max:2000|mimes:pdf,doc,docx,ppt,pptx'
        ]);

        $input = $request->except('_token','user_id');
        $lang = $request->input('lang');
        $file = $request->file('cv');

        $destinationPath = 'public/uploads/cv';
        $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
        $file->move($destinationPath, $newFileName);

        $input['cv'] = 'uploads/cv/'.$newFileName;

        $entry = Application::create($input);

        if($entry){

            if($lang=="ar")
                Session::flash('message','شكراً لتواصلك معنا. سنتواصل معك قريباً.');
            else
                Session::flash('message','Thank you for your contacting us. We will get back to you soon.');

            $emailData = $input;

            $subject = 'SKY AD. Developments Website - Careers Application';

            try {
                Mail::send('mail.contact', ['data' => $emailData], function ($message) use ($subject) {
                    $message->from('artists@ajmantourism.ae', 'Murabbaa Website')->to('gene@thisishatch.com', 'Gene')->subject($subject);
                });
            }
            catch (\Exception $e) {
                dd($e);
            }

            return redirect()->back();
        }

        Session::flash('error','An error has occurred. Please try again later.');

        return redirect()->back();
    }
}
