<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Community;
use App\Models\Floorplan;
use App\Models\Product;
use App\Models\Type;
use App\Models\Upload;
use App\Services\Uploaders\ProductImagesUploader;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class FloorPlanController extends Controller
{
    use CanCreateSlug;

    public function __construct(Floorplan $model)
    {
        $this->model = $model;
    }

    public function index($propery_id){
        $property = Product::find($propery_id);
        return view('admin.floorplans.index',compact('property'));
    }

    public function create($propery_id){
        $model = class_basename($this->model);
        $property = Product::find($propery_id);
        return view('admin.floorplans.create',compact('property','model'));
    }

    public function show(){
        $pages = $this->model->get();
        return view('admin.products.show',compact('pages'));
    }

    public function store(Request $request){

        $input = $request->input();

        $request->validate([
            'name' => 'required|max:255',
            'area' => 'required|max:255',
            'floors' => 'required|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);

        $input = [
            'name' => $input['name'],
            'name_ar' => $input['name_ar'],
            'area' => $input['area'],
            'floors' => $input['floors'],
            'product_id' => $input['property_id'],
        ];

        $newFloorplan = Floorplan::create($input);

        $image = $request->file('image');

        if($image){
            $image =  new UploadedFile( $image, 'tmp.jpg', 'image/jpeg',null,true);

            $img = Image::make($image);
            $img->resize(260, 260);

            $destinationPath = 'uploads/properties/floorplans';
            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            Image::make($image->getRealPath())->save($destinationPath.'/'.$newFileName);

            $newFloorplan->photos()->create(['url'=>'uploads/properties/floorplans/'. $newFileName]);
        }

        $rooms = $request->input('rooms');

        if($rooms){
            foreach($rooms as $room){
                if($room['name'] && $room['value']){
                    $room['name_ar'] = $room['name'];
                    $newFloorplan->rooms()->create($room);
                }
            }
        }

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($floorplan_id){
        $item = Floorplan::find($floorplan_id);
        return view('admin.floorplans.edit',compact('item'));
    }



    public function update(Request $request){

        $input = $request->input();

        $request->validate([
            'name' => 'required|max:255',
            'area' => 'required|max:255',
            'floors' => 'required|max:255',
        ]);

        $target = Floorplan::find($request->input('property_id'));

        $input = [
            'name' => $input['name'],
            'name_ar' => $input['name_ar'],
            'area' => $input['area'],
            'floors' => $input['floors'],
            'product_id' => $input['property_id'],
        ];

        $image = $request->file('image');

        if($image){
            $image =  new UploadedFile( $image, 'tmp.jpg', 'image/jpeg',null,true);

            $img = Image::make($image);
            $img->resize(260, 260);

            $destinationPath = 'uploads/properties/floorplans';
            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            Image::make($image->getRealPath())->save($destinationPath.'/'.$newFileName);

            $target->photos()->delete();
            $target->photos()->create(['url'=>'uploads/properties/floorplans/'. $newFileName]);
        }

        $rooms = $request->input('rooms');

        if($rooms){
            $target->rooms()->delete();
            foreach($rooms as $room){
                if($room['name'] && $room['value']){
                    $room['name_ar'] = $room['name'];
                    $target->rooms()->create($room);
                }
            }
        }

        $target->update($input);

        Session::flash('success','Item successfully updated.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    /**
     * Resizes a image using the InterventionImage package.
     *
     * @param object $file
     * @param string $fileNameToStore
     * @author Niklas Fandrich
     * @return bool
     */
    public function resizeImage($file) {
        // Resize image
        $resize = Image::make($file)->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $resize;
    }

}
