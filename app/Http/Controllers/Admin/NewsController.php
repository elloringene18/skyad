<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleType;
use App\Models\Category;
use App\Models\Community;
use App\Models\Floorplan;
use App\Models\Product;
use App\Models\Type;
use App\Models\Upload;
use App\Services\Uploaders\ProductImagesUploader;
use App\Traits\CanCreateSlug;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    use CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Article::get();
        return view('admin.news.index',compact('data'));
    }

    public function create(){
        $types = ArticleType::get();
        return view('admin.news.create',compact('types'));
    }

    public function edit($id){
        $types = ArticleType::get();
        $item = Article::find($id);
        return view('admin.news.edit',compact('types','item'));
    }

    public function store(Request $request){

        $input = $request->input();

        $request->validate([
            'title' => 'required|max:255',
            'title_ar' => 'required|max:255',
            'content' => 'required',
            'photo' => 'mimes:jpeg,jpg,png,gif|required|max:80000' // max 10000kb
        ]);

        $input['slug'] = $this->generateSlug($input['title']);
        $input['publish_date'] = Carbon::parse($input['publish_date']);
        $article = \App\Models\Article::create($input);

        $photo = $request->file('photo');



        if($request->file('photo')){
            $file= $request->file('photo');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(public_path('public/uploads/articles'), $filename);
            $article['photo']= $filename;
        }
        $article->save();


      //  if($photo){
      //  $image =  new \Illuminate\Http\UploadedFile( $photo, 'tmp.jpg', 'image/jpeg',null,true);
      //
      //  $img = Image::make($image);
      //  $img->resize(260, 260);
//
      //      $destinationPath = 'public/uploads/articles';
      //      $newFileName = \Illuminate\Support\Str::random(32).'.'.$image->getClientOriginalExtension();
//
      //      Image::make($image->getRealPath())->fit(398, 274)->save($destinationPath.'/'.$newFileName);
      //      $article['photo'] = 'uploads/articles/'. $newFileName;
//
      //      $newFileName = \Illuminate\Support\Str::random(32).'.'.$image->getClientOriginalExtension();
      //      Image::make($image->getRealPath())->fit(812, 268)->save($destinationPath.'/'.$newFileName);
      //      $article['photo_full'] = 'uploads/articles/'. $newFileName;
//
      //      $article->save();
      //  }

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function update(Request $request){

        $input = $request->except('_token','id');

        $request->validate([
            'title' => 'required|max:255',
        ]);

        $article = Article::find($request->input('id'));

        if(!$article){
            Session::flash('error','Item not found.');
            return redirect()->back();
        }

        $input['publish_date'] = Carbon::parse($input['publish_date']);

        $photo = $request->file('photo');

        if($photo){
            $image =  new \Illuminate\Http\UploadedFile( $photo, 'tmp.jpg', 'image/jpeg',null,true);

            $img = Image::make($image);
            $img->resize(260, 260);

            $destinationPath = 'public/uploads/articles';
            $newFileName = \Illuminate\Support\Str::random(32).'.'.$image->getClientOriginalExtension();

            Image::make($image->getRealPath())->fit(398, 274)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/articles/'. $newFileName;

            $newFileName = \Illuminate\Support\Str::random(32).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->fit(1296, 428)->save($destinationPath.'/'.$newFileName);
            $input['photo_full'] = 'uploads/articles/'. $newFileName;

        }

        $article->update($input);

        Session::flash('success','Item successfully updated.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    /**
     * Resizes a image using the InterventionImage package.
     *
     * @param object $file
     * @param string $fileNameToStore
     * @author Niklas Fandrich
     * @return bool
     */
    public function resizeImage($file) {
        // Resize image
        $resize = Image::make($file)->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $resize;
    }

}
