<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Category;
use App\Models\Community;
use App\Models\Product;
use App\Models\Type;
use App\Models\Upload;
use App\Services\Uploaders\ProductImagesUploader;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    use CanCreateSlug;

    public function __construct(Product $model, ProductImagesUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    public function index(){
        $data = Product::with('category','type','community')->paginate(100);

        return view('admin.products.index',compact('data'));
    }

    public function create(){
        $categories = Category::get();
        $types = Type::get();
        $communities = Community::get();
        $model = class_basename($this->model);
        return view('admin.products.create',compact('categories','communities','types','model'));
    }

    public function show(){
        $pages = $this->model->get();
        return view('admin.products.show',compact('pages'));
    }

    public function store(Request $request){

        $input = $request->input();
        $request->validate([
            'title' => 'required|max:255',
            'area' => 'required|max:255',
            'price' => 'required|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);

        $category = Category::find($input['category_id'])->id;
        $type = Type::find($input['type_id'])->id;
        $community = Community::find($input['community_id'])->id;

        $input = [
            'title' => $input['title'],
            'title_ar' => $input['title_ar'],
            'slug' => $this->generateSlug($input['title']),
            'photo' => null,
            'area' => $input['area'],
            'photo_full' => null,
            'is_faved' => 0,
            'bedrooms' => $input['bedrooms'],
            'description' => $input['description'],
            'description_ar' => $input['description_ar'],
            'price' => isset($input['price']) ? floatval($input['price']) : 0,
            'category_id' => $category,
            'type_id' => $type,
            'community_id' => $community
        ];

        $image = $request->file('image');

        if($image){
            $img = Image::make($image);

            $destinationPath = 'uploads/products';
            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            Image::make($image->getRealPath())->fit(398)->crop(398,274)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/products/'. $newFileName;

            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            if ($img->height()>$img->width())
                Image::make($image->getRealPath())->fit(398, 274)->save($destinationPath.'/'.$newFileName);
            else
                Image::make($image->getRealPath())->resize(null,600, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$newFileName);

            $input['photo_full'] = 'uploads/products/'. $newFileName;
        }

        $gallery = [];
        $galleryPhotos = $request->file('gallery');

        if(isset($galleryPhotos)){
            foreach ($galleryPhotos as $photoUrl) {
                $image =  new UploadedFile( $photoUrl, 'tmp.jpg', 'image/jpeg',null,true);
                $photo = ($image != null ? $this->uploader->upload($image) : false);
                if ($photo)
                    $gallery[] = $photo;
            }
        }

        $newData = $this->model->create($input);

        if($newData)
            foreach ($gallery as $photo)
            $newData->uploads()->createMany($photo);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $categories = Category::get();
        $types = Type::get();
        $communities = Community::get();
        $model = class_basename($this->model);
        $item = $this->model->with('community','type','category')->find($id);

        return view('admin.products.edit',compact('item','categories','types','communities','model'));
    }



    public function update(Request $request){
        $target = Product::find($request->input('product_id'));

        $input = $request->input();
        $request->validate([
            'title' => 'required|max:255',
            'area' => 'required|max:255',
            'price' => 'required|max:255',
        ]);

        $category = Category::find($input['category_id'])->id;
        $type = Type::find($input['type_id'])->id;
        $community = Community::find($input['community_id'])->id;

        $input = [
            'title' => $input['title'],
            'title_ar' => $input['title_ar'],
            'slug' => $this->generateSlug($input['title']),
            'area' => $input['area'],
            'description' => $input['description'],
            'description_ar' => $input['description_ar'],
            'price' => isset($input['price']) ? floatval($input['price']) : 0,
            'category_id' => $category,
            'type_id' => $type,
            'bedrooms' => $input['bedrooms'],
            'community_id' => $community
        ];

        $image = $request->file('image');

        if($image){
            $img = Image::make($image);

            $destinationPath = 'uploads/products';
            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            Image::make($image->getRealPath())->fit(360)->crop(360,360)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/products/'. $newFileName;

            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            if ($img->height()>$img->width())
                Image::make($image->getRealPath())->fit(398, 274)->save($destinationPath.'/'.$newFileName);
            else
                Image::make($image->getRealPath())->resize(null,600, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$newFileName);

            $input['photo_full'] = 'uploads/products/'. $newFileName;
        }

        $gallery = [];
        $galleryPhotos = $request->file('gallery');

        if($galleryPhotos){
            foreach ($galleryPhotos as $photoUrl) {
                $image =  new UploadedFile( $photoUrl, 'tmp.jpg', 'image/jpeg',null,true);
                $photo = ($image != null ? $this->uploader->upload($image) : false);
                if ($photo)
                    $gallery[] = $photo;
            }
        }

        $target->update($input);

        foreach ($gallery as $photo)
            $target->uploads()->createMany($photo);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    public function deleteSlide($id){
        $page = Upload::find($id);

        if($page)
            $page->delete();

        Session::flash('success','Item successfully deleted.');
        return redirect()->back();
    }

    /**
     * Resizes a image using the InterventionImage package.
     *
     * @param object $file
     * @param string $fileNameToStore
     * @author Niklas Fandrich
     * @return bool
     */
    public function resizeImage($file) {
        // Resize image
        $resize = Image::make($file)->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $resize;
    }

}
