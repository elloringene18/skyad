<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function showPage($page=null){
        $lang = 'en';

        if(view()->exists($page)){
            return view($page,compact('lang'));
        } else if (!$page){
            return view('home',compact('lang'));
        } else {
            return view('404',compact('lang'));
        }

    }

    public function showPageAR($page=null){
        $lang = 'ar';

        if(view()->exists($page)){
            return view($page,compact('lang'));
        } else if (!$page){
            return view('home',compact('lang'));
        } else {
            return view('404',compact('lang'));
        }
    }

    public function showBlog(){
        $lang = 'en';

        return view('article', compact('lang'));
    }

    public function showBlogAR(){
        $lang = 'ar';

        return view('article', compact('lang'));
    }
}
