<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Community;
use App\Models\Product;
use App\Models\Type;
use Illuminate\Http\Request;

class PropertiesController extends Controller
{
    public function __construct()
    {
        $this->pagination = 8;
    }

    public function index(Request $request, $cat = null){
        $activeAttrs = [];
        $category = null;

        if(!$cat){
            $cat = Category::first();
            $category = $cat->slug;
        } else {
            $category = $cat;
        }

        $filters = $request->except('brand','page','order','sort','price_min','price_max','community','type');

        $query = Product::query();
//
//        $query->with('category.attributes.values','attributes')
//            ->whereHas('category',function($query) use($category) {
//                return $query->where('slug',$category);
//            });
//
//        foreach($filters as $slug=>$value)
//            if($value)
//                $query->whereHas('attributes',function($q) use($slug,$value) {
//                    return $q->where('value',$value);
//                });
//
//        if($request->input('price_min'))
//            $query->where('price','>=', $request->input('price_min'));

        if($request->input('price')){
            if($request->input('price')=='1000000-'){
                $query->where('price','<', 1000000);
            }

            elseif($request->input('price')=='1000000-2000000'){
                $query->whereBetween('price',[1000000,2000000]);
                $query->orWhereBetween('price_max',[1000000,2000000]);
            }

            elseif($request->input('price')=='2000000-3000000'){
                $query->whereBetween('price',[2000000,3000000]);
                $query->orWhereBetween('price_max',[2000000,3000000]);
            }

            elseif($request->input('price')=='3000000-4000000'){
                $query->whereBetween('price',[3000000,4000000]);
                $query->orWhereBetween('price_max',[3000000,4000000]);
            }

            elseif($request->input('price')=='4000000-5000000'){
                $query->whereBetween('price',[4000000,5000000]);
                $query->orWhereBetween('price_max',[4000000,5000000]);

            }
        }


        if($request->input('category'))
            $query->whereHas('category', function($q) use($request){
                return $q->where('slug',$request->input('category'));
            });

        if($request->input('type'))
            $query->whereHas('type', function($q) use($request){
                return $q->where('slug',$request->input('type'));
            });

        if($request->input('community'))
            $query->whereHas('community', function($q) use($request){
                return $q->where('slug',$request->input('community'));
            });

        if($request->input('bedrooms'))
            $query->where('bedrooms',$request->input('bedrooms'));

//        if($request->input('sort')){
//            if($request->input('sort')=="lprice")
//                $query->orderBy('price','ASC');
//            elseif($request->input('sort')=="hprice")
//                $query->orderBy('price','DESC');
//            elseif($request->input('sort')=="aname")
//                $query->orderBy('title','ASC');
//            elseif($request->input('sort')=="dname")
//                $query->orderBy('title','DESC');
//        }

        $products = $query->orderBy('id','DESC')->paginate($this->pagination);

        $currentCategory = Category::where('slug',$category)->first();
        $attributes = $currentCategory->attributes;

        $lang = 'en';
        return view('property-listings',compact('products','currentCategory','attributes','lang'));
    }

    public function indexAr(Request $request, $cat = null){
        $activeAttrs = [];
        $category = null;

        if(!$cat){
            $cat = Category::first();
            $category = $cat->slug;
        } else {
            $category = $cat;
        }

        $filters = $request->except('brand','page','order','sort','price_min','price_max','community','type');

        $query = Product::query();
//
//        $query->with('category.attributes.values','attributes')
//            ->whereHas('category',function($query) use($category) {
//                return $query->where('slug',$category);
//            });
//
//        foreach($filters as $slug=>$value)
//            if($value)
//                $query->whereHas('attributes',function($q) use($slug,$value) {
//                    return $q->where('value',$value);
//                });
//
//        if($request->input('price_min'))
//            $query->where('price','>=', $request->input('price_min'));

        if($request->input('price')){
            if($request->input('price')=='1000000-'){
                $query->where('price','<', 1000000);
            }

            elseif($request->input('price')=='1000000-2000000'){
                $query->whereBetween('price',[1000000,2000000]);
                $query->orWhereBetween('price_max',[1000000,2000000]);
            }

            elseif($request->input('price')=='2000000-3000000'){
                $query->whereBetween('price',[2000000,3000000]);
                $query->orWhereBetween('price_max',[2000000,3000000]);
            }

            elseif($request->input('price')=='3000000-4000000'){
                $query->whereBetween('price',[3000000,4000000]);
                $query->orWhereBetween('price_max',[3000000,4000000]);
            }

            elseif($request->input('price')=='4000000-5000000'){
                $query->whereBetween('price',[4000000,5000000]);
                $query->orWhereBetween('price_max',[4000000,5000000]);

            }
        }


        if($request->input('category'))
            $query->whereHas('category', function($q) use($request){
                return $q->where('slug',$request->input('category'));
            });

        if($request->input('type'))
            $query->whereHas('type', function($q) use($request){
                return $q->where('slug',$request->input('type'));
            });

        if($request->input('community'))
            $query->whereHas('community', function($q) use($request){
                return $q->where('slug',$request->input('community'));
            });

        if($request->input('bedrooms'))
            $query->where('bedrooms',$request->input('bedrooms'));

//        if($request->input('sort')){
//            if($request->input('sort')=="lprice")
//                $query->orderBy('price','ASC');
//            elseif($request->input('sort')=="hprice")
//                $query->orderBy('price','DESC');
//            elseif($request->input('sort')=="aname")
//                $query->orderBy('title','ASC');
//            elseif($request->input('sort')=="dname")
//                $query->orderBy('title','DESC');
//        }

        $products = $query->orderBy('id','DESC')->paginate($this->pagination);

        $currentCategory = Category::where('slug',$category)->first();
        $attributes = $currentCategory->attributes;

        $lang = 'ar';
        return view('property-listings',compact('products','currentCategory','attributes','lang'));
    }

    public function show($slug){

        $post = Product::with('floorplans.rooms')->where('slug',$slug)->first();
        $others = Product::where('slug','!=',$slug)->limit(3)->get();

        $lang = 'en';
        return view('property',compact('post','others','lang'));
    }

    public function showAr($slug){

        $post = Product::with('floorplans.rooms')->where('slug',$slug)->first();
        $others = Product::where('slug','!=',$slug)->limit(3)->get();

        $lang = 'ar';
        return view('property',compact('post','others','lang'));
    }

    public function collection(Request $request, $cat = null){
        $activeAttrs = [];
        $category = null;

        if(!$cat){
            $cat = Category::first();
            $category = $cat->slug;
        } else {
            $category = $cat;
        }

        $query = Product::query();

        $query->with('category.attributes.values','attributes')
            ->whereHas('category',function($query) use($category) {
                return $query->where('slug',$category);
            });

        if($request->input('sort')){
            if($request->input('sort')=="lprice")
                $query->orderBy('price','ASC');
            elseif($request->input('sort')=="hprice")
                $query->orderBy('price','DESC');
            elseif($request->input('sort')=="aname")
                $query->orderBy('title','ASC');
            elseif($request->input('sort')=="dname")
                $query->orderBy('title','DESC');
        }

        $products = $query->orderBy('id','DESC')->paginate($this->pagination);

        $currentCategory = Category::where('slug',$category)->first();
        $brands = Seller::all();
        $attributes = $currentCategory->attributes;


        return view('collection-single',compact('products','currentCategory','attributes','brands'));
    }

    public function seller(Request $request, $cat = null){
        $seller = null;

        if(!$cat){
            $cat = Seller::first();
            $seller = $cat->slug;
        } else {
            $seller = $cat;
        }

        $filters = $request->except('page','order','sort','price_min','price_max');

        $query = Product::query();

        $query->with('seller')
            ->whereHas('seller',function($query) use($seller) {
                return $query->where('slug',$seller);
            });
//
//        if($request->input('price_min'))
//            $query->where('price','>=', $request->input('price_min'));
//
//        if($request->input('price_max'))
//            $query->where('price','<=', $request->input('price_max'));

        if($request->input('category'))
            $query->whereHas('category', function($query) use($request){
                return $query->where('slug',$request->input('category'));
            });

        if($request->input('sort')){
            if($request->input('sort')=="lprice")
                $query->orderBy('price','ASC');
            elseif($request->input('sort')=="hprice")
                $query->orderBy('price','DESC');
            elseif($request->input('sort')=="aname")
                $query->orderBy('title','ASC');
            elseif($request->input('sort')=="dname")
                $query->orderBy('title','DESC');
        }

        $products = $query->orderBy('id','DESC')->paginate($this->pagination);
        $categories = Category::get();

        $currentSeller = Seller::where('slug',$seller)->first();

        return view('seller',compact('products','currentSeller','categories'));
    }

    private function buildData($data){
        $results = [];

        foreach ($data as $item){
            $results[$item->id]['title'] = $item->title;
            $results[$item->id]['id'] = $item->id;
            $results[$item->id]['slug'] = $item->slug;
            $results[$item->id]['description'] = $item->description;
            $results[$item->id]['photo'] = $item->thumbnailUrl;
            $results[$item->id]['price'] = $item->price;
            $results[$item->id]['year'] = $item->year;
            $results[$item->id]['is_faved'] = $item->is_faved;
            $results[$item->id]['category'] = $item->category;
            $results[$item->id]['attributes'] = $item->attributes;
            $results[$item->id]['for_sale'] = $item->for_sale;
        }

        return $results;
    }
}
