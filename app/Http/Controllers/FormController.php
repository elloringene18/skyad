<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\ArtistInteraction;
use App\Models\ContactEntry;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FormController extends Controller
{
    public function __construct(Subscriber $model)
    {
        $this->model = $model;
    }

    public function subscribe(Request $request){
        $input = $request->input('email');
        $lang = $request->input('lang');

        if(Subscriber::where('email',$input)->count())
            return 'This email has already been registered.';

        $success = Subscriber::create(['email'=>$input]);

        if($success){
            if($lang=='en')
                $message = 'Thank you for subscribing to our newsletter.';
            else
                $message = 'Thank you for subscribing to our newsletter.';
        }

        return $message;
    }

    public function sendEmail($input){

        ArtistInteraction::create([
            'name' => $input['name'],
            'contact' => $input['contact'],
            'message' => $input['message'],
            'artist_id' => $input['artist_id'],
        ]);

        $emailData = [
            'name' => $input['name'],
            'contact' => $input['contact'],
            'message' => $input['message'],
        ];

        $subject = 'Murabbaa Website - Artwork Inquiry';
        $artist = Artist::find($input['artist_id']);

        try {
            Mail::send('mail.send-artist', ['data' => $emailData], function ($message) use ($input, $artist,$subject) {
                $message->from('Artists@ajmantourism.ae', 'Murabbaa Website')->to('gene@thisishatch.com', 'Gene')->subject($subject);
//                $message->from('info@ajmantourism.ae', $input['name'])->to('info@ajmantourism.ae', 'Ajman Tourism')->subject($subject);
            });
        }
        catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function contact(Request $request){

//        $honeypot = $request->input('user_id');
//
//        if($honeypot)
//            return redirect()->back();

        $type = $request->input('type');

        if($type=='contact-us')
            $request->validate([
                'phone' => 'required|max:255',
                'name' => 'required|max:255',
                'email' => 'required|email|max:255',
                'message' => 'required|max:500',
                'g-recaptcha-response' => 'required',
            ]);
        elseif($type=='property-listing')
            $request->validate([
                'phone' => 'required|max:255',
                'name' => 'required|max:255',
                'email' => 'required|email|max:255',
                'g-recaptcha-response' => 'required',
            ]);

        $input = $request->except('_token','user_id','lang');
        $lang = $request->input('lang');

        $entry = ContactEntry::create(['ip'=>$request->ip(),'source'=>$type]);

        if($entry){

            foreach ($input as $key=>$item){
                $entry->items()->create(['key'=>$key,'value'=>$item]);
            }

            if($lang=="ar")
                Session::flash('message','شكراً لتواصلك معنا. سنتواصل معك قريباً.');
            else
                Session::flash('message','Thank you for your contacting us. We will get back to you soon.');

            $emailData = $input;

            $subject = 'Murabbaa Website - Contact Form';

            try {
                Mail::send('mail.contact', ['data' => $emailData], function ($message) use ($subject) {
                    $message->from('artists@ajmantourism.ae', 'Murabbaa Website')->to('gene@thisishatch.com', 'Gene')->subject($subject);
//                $message->from('info@ajmantourism.ae', $input['name'])->to('info@ajmantourism.ae', 'Ajman Tourism')->subject($subject);
                });
            }
            catch (\Exception $e) {
                dd($e);
            }

            return redirect()->back();
        }


        Session::flash('error','An error has occurred. Please try again later.');

        return redirect()->back();
    }
}
