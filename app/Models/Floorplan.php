<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Floorplan extends Model
{
    use HasFactory;

    protected $fillable = ['name','name_ar','area','floors','product_id'];

    public function rooms(){
        return $this->hasMany('App\Models\Room');
    }

    public function getRoomsAttribute(){
        return $this->rooms()->get();
    }

    public function amenities(){
        return $this->hasMany('App\Models\Amenity');
    }

    public function photos(){
        return $this->hasMany('App\Models\FloorplanPhoto');
    }

    public function levels(){
        return $this->hasMany('App\Models\Level');
    }

    public function getLevelsAttribute(){
        return $this->levels()->get();
    }

    public function getAmenitiesAttribute(){
        return $this->amenities()->get();
    }

    public function getPhotosAttribute(){
        return $this->photos()->get();
    }
}
