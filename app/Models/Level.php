<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    protected $fillable = ['name','name_ar','photo'];

    public function rooms(){
        return $this->hasMany('App\Models\LevelRoom');
    }

    public function getRoomsAttribute(){
        return $this->rooms()->get();
    }

    public function photos(){
        return $this->hasMany('App\Models\LevelPhoto');
    }

    public function getPhotosAttribute(){
        return $this->photos()->get();
    }
}
