<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    use HasFactory;

    protected $fillable = ['value','value_ar','slug','attribute_id'];

    public function attribute(){
        return $this->hasOne('App\Models\Attribute','id','attribute_id');
    }

    public function getAttributeAttribute(){
        return $this->attribute()->first();
    }
}
