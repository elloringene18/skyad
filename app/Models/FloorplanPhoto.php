<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FloorplanPhoto extends Model
{
    use HasFactory;

    protected $fillable = ['caption','caption_ar','url','floorplan_id'];
}
