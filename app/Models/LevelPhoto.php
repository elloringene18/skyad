<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LevelPhoto extends Model
{
    use HasFactory;

    protected $fillable = ['caption','caption_ar','url','level_id'];
}
