<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Product extends Model implements Searchable
{
    use HasFactory;

    public function getSearchResult(): SearchResult
    {
        $url = url('search');

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->title,
            $url
        );
    }

    protected $fillable = ['slug','title','title_ar','area','photo_full','bedrooms','type_id','community_id','description','description_ar','photo','price','price_max','is_faved','category_id','stock'];

    public function category(){
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function getCategoryAttribute(){
        return $this->category()->first();
    }

    public function type(){
        return $this->hasOne('App\Models\Type','id','type_id');
    }

    public function getTypeAttribute(){
        return $this->type()->first();
    }

    public function community(){
        return $this->hasOne('App\Models\Community','id','community_id');
    }

    public function getCommunityAttribute(){
        return $this->type()->first();
    }

    public function floorplans(){
        return $this->hasMany('App\Models\Floorplan');
    }

    public function getFloorplansAttribute(){
        return $this->floorplans()->get();
    }

    public function attributes(){
        return $this->belongsToMany('App\Models\AttributeValue','product_attributes','product_id','attribute_value_id');
    }

    public function getAttributesAttribute(){
        return $this->attributes()->get();
    }

    public function getThumbnailUrlAttribute(){
        if($this->photo)
            return asset($this->photo);

        return asset('img/shop/placeholder.jpg');
    }

    public function getPhotoUrlAttribute(){

        if($this->photo)
            return asset($this->photo);

        return asset('img/shop/placeholder.jpg');
    }

    /**
     * A user has a profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('App\Models\Upload', 'uploadable');
    }

    public function getGalleryPhotosAttribute(){

        $uploads = $this->uploads()->where('template','slider')->get();

        if(count($uploads)){
            $data = [];

            foreach($uploads as $photo)
                $data[] = asset(''.$photo->path.'/'.$photo->file_name);

            return $data;
        }

        return [];
    }

    public function getGalleryPhotosFullAttribute(){

        $uploads = $this->uploads()->where('template','slider')->get();

        if(count($uploads)){
            $data = [];

            foreach($uploads as $photo){
                $data[$photo->id] = asset(''.$photo->path.'/'.$photo->file_name);
            }

            return $data;
        }

        return [];
    }

}
