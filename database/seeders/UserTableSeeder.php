<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = [
            [
                'user' => [
                    'name' => 'Gene',
                    'email' => 'gene@thisishatch.com',
                    'password' => \Illuminate\Support\Facades\Hash::make('secret'),
                ],
                'role' => 'admin'
            ],
            [
                'user' => [
                    'name' => 'Admin',
                    'email' => 'admin@skyabudhabi.ae',
                    'password' => \Illuminate\Support\Facades\Hash::make('skyabudhabi@2021!HTX'),
                ],
                'role' => 'admin',
            ]
        ];

        foreach ($users as $index=>$user)
        {
            $newUser = User::create($user['user']);
            $roleId = \App\Models\Role::where('slug',$user['role'])->pluck('id')->first();
            $newUser->role()->sync($roleId);
        }
    }
}
