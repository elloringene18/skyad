<?php

namespace Database\Seeders;

use App\Models\Type;
use App\Traits\CanCreateSlug;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Type $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'name' => "Horizon Apartment"
            ],
            [
                'name' => "Garden Apartment"
            ],
            [
                'name' => "Typical Apartment"
            ],
            [
                'name' => "Corner Apartment"
            ],
            [
                'name' => "Terrace Apartment"
            ],
            [
                'name' => "Sky Duplex"
            ],
            [
                'name' => "Garden Duplex"
            ],
            [
                'name' => "Deluxe Apartment"
            ],
            [
                'name' => "Premium Apartment"
            ]
        ];

        foreach ($data as $item)
        {
            $data = [];
            $data['name'] = $item['name'];
            $data['slug'] = $this->generateSlug($item['name']);

            Type::create($data);
        }
    }
}
