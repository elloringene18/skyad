<?php

namespace Database\Seeders;

use App\Models\ContactEntry;
use App\Models\Partner;
use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'type' => 'contact-us',
                'email' => 'admij@vhfgjg.ag',
                'created_at' => '2021-10-18 09:49:03',
            ],
            [
                'type' => 'propert-listings',
                'email' => 'admij@vhfgjg.ae',
                'created_at' => '2021-10-18 09:48:35',
            ],
            [
                'type' => 'propert-listings',
                'email' => 'nouramostafa45@yahoo.com',
                'created_at' => '2021-10-18 09:47:49',
            ],
            [
                'type' => 'contact-us',
                'email' => 'nouramostafa45@yahoo.com',
                'created_at' => '2021-10-18 09:47:26',
            ],
            [
                'type' => 'contact-us',
                'email' => 'Marco.nashaat@outlook.com',
                'created_at' => '2021-10-13 13:25:58',
            ],
            [
                'type' => 'contact-us',
                'email' => 'kareemmohamed200n@gmail.com',
                'created_at' => '	2021-10-10 14:15:13',
            ],
            [
                'type' => 'contact-us',
                'email' => 'kareemmohamed200n@gmail.com',
                'created_at' => '2021-10-10 14:14:31',
            ],
            [
                'type' => 'propert-listings',
                'email' => 'ahmedfouad99@gmail.com',
                'created_at' => '2021-10-10 06:00:05',
            ],
            [
                'type' => 'contact-us',
                'email' => 'phde7711@gmail.com',
                'created_at' => '2021-10-07 09:26:17',
            ],
            [
                'type' => 'propert-listings',
                'email' => 'Mahmoud.Menefie@akqa.com',
                'created_at' => '2021-10-03 11:39:40',
            ],
            [
                'type' => 'propert-listings',
                'email' => 'zancoo@me.com',
                'created_at' => '2021-10-03 11:22:35',
            ],
            [
                'type' => 'propert-listings',
                'email' => 'Nourhanelmalah@aucegypt.edu',
                'created_at' => '	2021-09-30 14:50:35',
            ],
            [
                'type' => 'contact-us',
                'email' => 'nouramostafa45@yahoo.com',
                'created_at' => '2021-09-30 12:24:52',
            ],

        ];

        foreach ($data as $item){
            $entry = ContactEntry::create(
                [
                    'created_at' => $item['created_at'],
                    'source' => $item['type'],
                    'ip' => '::1',
                ]
            );


            $entry->items()->create(
                [
                    'key'=>'type',
                    'value'=>$item['type']
                ]
            );

            $entry->items()->create(
                [
                    'key'=>'name',
                    'value'=>' '
                ]
            );
            
            $entry->items()->create(
                [
                    'key'=>'email',
                    'value'=>$item['email']
                ]
            );

        }
    }
}
