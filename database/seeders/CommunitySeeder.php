<?php

namespace Database\Seeders;

use App\Models\Community;
use App\Traits\CanCreateSlug;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class CommunitySeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Community $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [
            [
                'name' => "Residence eight"
            ]
        ];

        foreach ($data as $item)
        {
            $type = [];
            $type['name'] = $item['name'];
            $type['slug'] = $this->generateSlug($item['name']);

            Community::create($type);
        }
    }
}
