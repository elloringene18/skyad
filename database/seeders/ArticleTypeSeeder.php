<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ArticleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Press Release', 'slug' => 'press-release' ],
        ];

        foreach($data as $item)
            \App\Models\ArticleType::create($item);
    }
}
