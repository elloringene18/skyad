<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Traits\CanCreateSlug;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use SebastianBergmann\Environment\Console;
use Symfony\Component\Console\Output\ConsoleOutput;

class CategorySeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [
            [
                'name' => "Residential"
            ],
            [
                'name' => "Commercial"
            ],
        ];

        foreach ($data as $item)
        {
            $category = [];
            $category['name'] = $item['name'];
            $category['slug'] = $this->generateSlug($item['name']);

            Category::create($category);
        }
    }
}

