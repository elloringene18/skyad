<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Article;
use Intervention\Image\Facades\Image;

class ArticleSeeder extends Seeder
{
    use \App\Traits\CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title_en','title_ar','title','slug','content_en','content_ar','content','publish_date','thumbnail','featured_image'

        $data = [
            [
                'title' => 'SKY AD. Developments wins bt100 award for Leading Investment performance for a real estate developer in NAC for 2020',
                'title_ar' => 'SKY AD. Developments wins bt100 award for Leading Investment performance for a real estate developer in NAC for 2020',
                'content' => '
<p>The bt100 Awards Ceremony honored SKY AD. Developments as “Leading Investment performance for a real estate developer in NAC for 2020”. SKY AD. Developments had a very solid start in the local market with the launch of its flagship project in the New Administrative Capital “Residence Eight” and an ambitious investment plan of EGP 15bn.
</p>
<p>
This year’s edition of the Business Today Awards (bt100) honored a number of Egyptian ministries, leaders, and resilient companies who were able to keep the country\'s economy, as well as their own businesses afloat during this tough year as the country suffered with the still on-going pandemic. 
</p>
',
                'content_ar' => '
<p>The bt100 Awards Ceremony honored SKY AD. Developments as “Leading Investment performance for a real estate developer in NAC for 2020”. SKY AD. Developments had a very solid start in the local market with the launch of its flagship project in the New Administrative Capital “Residence Eight” and an ambitious investment plan of EGP 15bn.
</p>
<p>
This year’s edition of the Business Today Awards (bt100) honored a number of Egyptian ministries, leaders, and resilient companies who were able to keep the country\'s economy, as well as their own businesses afloat during this tough year as the country suffered with the still on-going pandemic. 
</p>',
                'publish_date' => '2021-02-08',
                'photo' => 'img/press/full/2.jpg',
                'external_link' => '',
                'article_type_id' => 1,
            ],
            [
                'title' => 'SKY AD. Developments signs a cooperation protocol with FAB for mortgage loans covering up to 85% of unit value
',
                'title_ar' => 'SKY AD. Developments signs a cooperation protocol with FAB for mortgage loans covering up to 85% of unit value
',
                'content' => '
<p>SKY AD. Developments has signed a cooperation protocol with First Abu Dhabi Bank (FAB) to provide mortgage finance to customers. Eng. Abdelrahman Agami, CEO of Diamond Group and SKY AD. Developments, and Mohamed Fayed, CEO of FAB-Egypt signed the protocol. It was signed in the presence of Saleh bin Nasra, owner of Diamond Group, and Mostafa Salah, CCO of SKY AD. Developments.
</p>
<p>The financing for the mortgage loans aims to offer different payment facilities, covering up to 85% of the ready-to-move units and a repayment period of up to 15 years with a competitive interest rate and a maximum loan amount of EGP 15m.The agreement comes as part of FAB and SKY AD. Development’s eagerness to support Egypt’s real estate sector.
</p>
',
                'content_ar' => '<p>
<p>SKY AD. Developments has signed a cooperation protocol with First Abu Dhabi Bank (FAB) to provide mortgage finance to customers. Eng. Abdelrahman Agami, CEO of Diamond Group and SKY AD. Developments, and Mohamed Fayed, CEO of FAB-Egypt signed the protocol. It was signed in the presence of Saleh bin Nasra, owner of Diamond Group, and Mostafa Salah, CCO of SKY AD. Developments.
</p>
<p>The financing for the mortgage loans aims to offer different payment facilities, covering up to 85% of the ready-to-move units and a repayment period of up to 15 years with a competitive interest rate and a maximum loan amount of EGP 15m.The agreement comes as part of FAB and SKY AD. Development’s eagerness to support Egypt’s real estate sector.
</p>',
                'publish_date' => '2021-02-02',
                'photo' => 'img/press/full/3.jpg',
                'external_link' => '',
                'article_type_id' => 1,
            ],
            [
                'title' => 'GAFI EXPLORES INVESTMENT OPPORTUNITIES WITH SKY AD. Developments',
                'title_ar' => 'GAFI EXPLORES INVESTMENT OPPORTUNITIES WITH SKY AD. Developments',
                'content' => '
                            <p>Mohammed Abdel Wahab, the Executive Director of the Egyptian Authority for Investment and Free Zones (GAFI), met with H.E Saleh Mohammed bin Nasra Al Ameri, owner of Diamond Group and SKY AD. Developments, and Abdel Rahman Agami the CEO of SKY AD. Developments to explore possible future cooperation in investment. Promising investment opportunities in various sectors were presented, in addition to the facilities and measures taken by the Egyptian government to encourage foreign investment.
</p>
                            <p>
A number of officials from the Investment Authority and the Commercial Office of Egypt in Dubai participated in the meeting, which was held in Abu Dhabi, such as Hisham Al-Shall, Director of the Technical Office at the General Investment Authority, Counsellor Amr Nour El-Din, Advisor to the CEO of the General Investment Authority and others.
</p>
                            <p>
H.E Saleh Mohammed bin Nasra Al Ameri stated that the group is looking forward to transferring its experience in real estate development to regional and international markets. Abdel Rahman Agami also said that the company has already taken steps in its plan to inject EGP 15 billion into Egypt over two years.</p>
                            ',
                'content_ar' => '
                            <p>Mohammed Abdel Wahab, the Executive Director of the Egyptian Authority for Investment and Free Zones (GAFI), met with H.E Saleh Mohammed bin Nasra Al Ameri, owner of Diamond Group and SKY AD. Developments, and Abdel Rahman Agami the CEO of SKY AD. Developments to explore possible future cooperation in investment. Promising investment opportunities in various sectors were presented, in addition to the facilities and measures taken by the Egyptian government to encourage foreign investment.
</p>
                            <p>
A number of officials from the Investment Authority and the Commercial Office of Egypt in Dubai participated in the meeting, which was held in Abu Dhabi, such as Hisham Al-Shall, Director of the Technical Office at the General Investment Authority, Counsellor Amr Nour El-Din, Advisor to the CEO of the General Investment Authority and others.
</p>
                            <p>
H.E Saleh Mohammed bin Nasra Al Ameri stated that the group is looking forward to transferring its experience in real estate development to regional and international markets. Abdel Rahman Agami also said that the company has already taken steps in its plan to inject EGP 15 billion into Egypt over two years.</p>',
                'publish_date' => '2021-02-02',
                'photo' => 'img/press/full/4.jpg',
                'external_link' => '',
                'article_type_id' => 1,
            ],
            [
                'title' => 'With investments of 15bn, SKY AD. Developments becomes the first UAE investor in the New Administrative Capital',
                'title_ar' => 'With investments of 15bn, SKY AD. Developments becomes the first UAE investor in the New Administrative Capital',
                'content' => '<p>SKY AD. Developments has announced its entry into the Egyptian real estate market, with a total investment volume of EGP 15bn, through its “Residence Eight” project that is located in a prime location in the New Administrative Capital. </p>
<p>Residence Eight project is set to revolutionize the architecture landscape in Egypt as part of the company\'s regional and international expansion strategy. </p>',
                'content_ar' => '<p>SKY AD. Developments has announced its entry into the Egyptian real estate market, with a total investment volume of EGP 15bn, through its “Residence Eight” project that is located in a prime location in the New Administrative Capital. </p>
<p>Residence Eight project is set to revolutionize the architecture landscape in Egypt as part of the company\'s regional and international expansion strategy. </p>',
                'publish_date' => '2021-02-02',
                'photo' => 'img/press/full/5.jpg',
                'external_link' => '',
                'article_type_id' => 1
            ],
            [
                'title' => 'SKY AD. Developments inaugurates Residence Eight project in the New Capital',
                'title_ar' => 'SKY AD. Developments inaugurates Residence Eight project in the New Capital',
                'content' => "
<p>SKY AD. Developments has announced the inauguration of “Residence Eight”, its first project at Egypt’s New Administrative Capital (NAC). In a bid to encourage foreign direct investment in Egypt, SKY AD. Developments has obtained ministerial approval for the planning and division of Residence Eight’s plot of land. 
</p>
<p>
The company aims to support Egypt’s urban development to accommodate the rapid increase in population. This falls in line with Egypt’s sustainable development strategy in the form of its 2030 vision, whilst establishing integrated communities that provide quality life for Egyptians.  
</p>
",
                'content_ar' => "
<p>SKY AD. Developments has announced the inauguration of “Residence Eight”, its first project at Egypt’s New Administrative Capital (NAC). In a bid to encourage foreign direct investment in Egypt, SKY AD. Developments has obtained ministerial approval for the planning and division of Residence Eight’s plot of land. 
</p>
<p>
The company aims to support Egypt’s urban development to accommodate the rapid increase in population. This falls in line with Egypt’s sustainable development strategy in the form of its 2030 vision, whilst establishing integrated communities that provide quality life for Egyptians.  
</p>",
                'publish_date' => '2021-02-02',
                'photo' => 'img/press/full/6.jpg',
                'external_link' => '',
                'article_type_id' => 1,
            ],
            [
                'title' => 'SKY AD. Developments celebrates the launch of its first project in Egypt with an extraordinary event, featuring Megastar Amr Diab',
                'title_ar' => 'SKY AD. Developments celebrates the launch of its first project in Egypt with an extraordinary event, featuring Megastar Amr Diab',
                'content' => '<p>SKY AD. Developments hosted an extraordinary first event in Cairo at the JW Marriott garden to celebrate the launch of its new project in Egypt, Residence Eight. The Walk on Air event featured Megastar Amr Diab, Eirini Papadopoulou, in the presence of distinguished figures from the business community, media representatives, sports and lifestyle influencers. </p>',
                'content_ar' => '<p>SKY AD. Developments hosted an extraordinary first event in Cairo at the JW Marriott garden to celebrate the launch of its new project in Egypt, Residence Eight. The Walk on Air event featured Megastar Amr Diab, Eirini Papadopoulou, in the presence of distinguished figures from the business community, media representatives, sports and lifestyle influencers. </p>',
                'publish_date' => '2021-02-07',
                'photo' => 'img/press/full/7b.png',
                'external_link' => '',
                'article_type_id' => 1,
            ],
            [
                'title' => 'SKY AD. Developments hosts an exceptional units selection event for its Residence Eight project',
                'title_ar' => 'SKY AD. Developments hosts an exceptional units selection event for its Residence Eight project',
                'content' => '
<p>Reflecting the trust and confidence in the UAE-based developer and in light of SKY AD. Development’s ongoing efforts to promote its first project “Residence Eight”, the company held a two-day units selection event at SKY AD. Developments. headquarters to encourage potential project owners to explore the firm\'s unsurpassed high-quality units and offer them the chance to own their ideal residences. </p>
',
                'content_ar' => '
<p>Reflecting the trust and confidence in the UAE-based developer and in light of SKY AD. Development’s ongoing efforts to promote its first project “Residence Eight”, the company held a two-day units selection event at SKY AD. Developments. headquarters to encourage potential project owners to explore the firm\'s unsurpassed high-quality units and offer them the chance to own their ideal residences. </p>',
                'publish_date' => '2021-02-02',
                'photo' => 'img/press/full/New-Selection-Event-Photo.jpg',
                'external_link' => '',
                'article_type_id' => 1,
            ],
            [
                'title' => 'GAFI explores investment opportunities with SKY AD. Developments',
                'title_ar' => 'GAFI explores investment opportunities with SKY AD. Developments',
                'content' => '
<p>H.E Saleh Mohammed Bin Nasra, owner of Diamond Group, and Eng.Abdelrahman Agami, CEO of Diamond Group and SKY AD. Developments, met with Mohamed Abdel Wahab, Executive Director of the Egyptian General Authority for Investment and Free Zones (GAFI), to discuss ways to increase the group\'s investments in Egypt and to present promising investment opportunities available in various sectors. 
</p>
<p>
Saleh Mohammed Bin Nasra stated that the UAE-based company intends to transfer its real estate development experience to several regional and international markets. He added that the company chose Egypt as its first destination outside the UAE to capitalize on its promising investment opportunities and the continuously improving investment </p>
',
                'content_ar' => '
<p>H.E Saleh Mohammed Bin Nasra, owner of Diamond Group, and Eng.Abdelrahman Agami, CEO of Diamond Group and SKY AD. Developments, met with Mohamed Abdel Wahab, Executive Director of the Egyptian General Authority for Investment and Free Zones (GAFI), to discuss ways to increase the group\'s investments in Egypt and to present promising investment opportunities available in various sectors. 
</p>
<p>
Saleh Mohammed Bin Nasra stated that the UAE-based company intends to transfer its real estate development experience to several regional and international markets. He added that the company chose Egypt as its first destination outside the UAE to capitalize on its promising investment opportunities and the continuously improving investment </p>
',
                'publish_date' => '2021-02-06',
                'photo' => 'img/press/full/10b.png',
                'external_link' => '',
                'article_type_id' => 1,
            ],
            [
                'title' => 'SKY AD. Development’s first project “Residence Eight” achieves 1bn sales worth in a record time in the New Administrative Capital',
                'title_ar' => 'SKY AD. Development’s first project “Residence Eight” achieves 1bn sales worth in a record time in the New Administrative Capital',
                'content' => '
<p>SKY AD. Developments, the subsidiary of Diamond Group and one of the leading Emirati real estate developers, achieved a record of EGP 1bn sales through 600 sold residential units in its first flagship project “Residence 
Eight” in the New Administrative Capital. The project is located in a prime location in the New Administrative Capital in the R8 area, which is surrounded by the Diplomatic Quarter from the west and overlooking 70 acres of 
green areas from the north, as well as the Green River near the downtown from the south. </p>
<br/><img src="http://188.166.44.182/skyad/public/img/press/one-billion.jpg" width="100%">',
                'content_ar' => '<p>SKY AD. Developments, the subsidiary of Diamond Group and one of the leading Emirati real estate developers, achieved a record of EGP 1bn sales through 600 sold residential units in its
 first flagship project “Residence Eight” in the New Administrative Capital. The project is located in a prime location in the New Administrative Capital in the R8 area, which is surrounded by the Diplomatic Quarter from 
 the west and overlooking 70 acres of green areas from the north, as well as the Green River near the downtown from the south. </p>
<br/><img src="http://188.166.44.182/skyad/public/img/press/one-billion.jpg" width="100%">',
                'publish_date' => '2021-10-13',
                'photo' => 'img/press/full/billion.jpg',
                'photo_full' => 'img/press/full/billion-full.jpg',
                'external_link' => '',
                'article_type_id' => 1,
            ],
            [
                'title' => 'SKY AD. Developments launches its first international commercial project “Capital Avenue” overlooking the Green River',
                'title_ar' => 'SKY AD. Developments launches its first international commercial project “Capital Avenue” overlooking the Green River',
                'content' => '<p>SKY AD. Developments has been relentlessly demonstrating its valuable addition to the Egyptian real estate market, particularly in the New Administrative Capital. With EGP 1.7 bn worth of investments, the company launched “Capital Avenue”, a commercial project spanning over an area of more than 21,000 m2 including mixed-use buildings for F&B, clinics, retail, administrative offices, and serviced apartments.
</p>
<p>
Owing to its prime location in the R8 area, “Capital Avenue” will embody the essence of premium living in one the quietest and most competitive locations in the New Administrative Capital, overlooking the diplomatic area from the east, the open club from the north, and the Green River from the south near the city center. 
</p>',
                'content_ar' => '',
                'publish_date' => '2021-10-13',
                'photo' => 'img/press/full/capital.jpg',
                'external_link' => '',
                'article_type_id' => 1,
            ],
            [
                'title' => 'SKY AD. Developments launches a new phase of its Residence Eight project, "Pool Side," with exclusive offers during "Cityscape" exhibition',
                'title_ar' => 'SKY AD. Developments launches a new phase of its Residence Eight project, "Pool Side," with exclusive offers during "Cityscape" exhibition',
                'content' => '<p>Driven by a growing demand on its first project in the New Administrative Capital, Residence Eight, SKY AD. Developments has launched a new phase "Pool Side", which includes a variety of residential units ranging in size from 110 to 360m2, featuring apartments and duplexes overlooking swimming pools. The “Pool Side” phase is now available for reservation with payment plans up to 10 years, catering to the different clients’ needs.  </p>',
                'content_ar' => '<p>Driven by a growing demand on its first project in the New Administrative Capital, Residence Eight, SKY AD. Developments has launched a new phase "Pool Side", which includes a variety of residential units ranging in size from 110 to 360m2, featuring apartments and duplexes overlooking swimming pools. The “Pool Side” phase is now available for reservation with payment plans up to 10 years, catering to the different clients’ needs.  </p>',
                'publish_date' => '2021-10-14',
                'photo' => 'img/press/full/cityscape.jpg',
                'external_link' => '',
                'article_type_id' => 1,
            ],
        ];

        foreach($data as $item){
            $item['slug'] = $this->generateSlug($item['title']);
            $article = \App\Models\Article::create($item);


            if(isset($item['photo']) && !isset($item['photo_full'])){
                $image =  new \Illuminate\Http\UploadedFile( public_path($item['photo']), 'tmp.jpg', 'image/jpeg',null,true);

                $img = Image::make($image);
                $img->resize(260, 260);

                $destinationPath = 'public/uploads/articles';
                $newFileName = \Illuminate\Support\Str::random(32).'.'.$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->fit(398, 274)->save($destinationPath.'/'.$newFileName);
                $article['photo'] = 'uploads/articles/'. $newFileName;

                $newFileName = \Illuminate\Support\Str::random(32).'.'.$image->getClientOriginalExtension();
                Image::make($image->getRealPath())->fit(812, 268)->save($destinationPath.'/'.$newFileName);
                $article['photo_full'] = 'uploads/articles/'. $newFileName;

                $article->save();
            } else {
                $image =  new \Illuminate\Http\UploadedFile( public_path($item['photo']), 'tmp.jpg', 'image/jpeg',null,true);

                $img = Image::make($image);
                $img->resize(260, 260);

                $destinationPath = 'public/uploads/articles';
                $newFileName = \Illuminate\Support\Str::random(32).'.'.$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->fit(398, 274)->save($destinationPath.'/'.$newFileName);
                $article['photo'] = 'uploads/articles/'. $newFileName;

                $image =  new \Illuminate\Http\UploadedFile( public_path($item['photo_full']), 'tmp.jpg', 'image/jpeg',null,true);

                $newFileName = \Illuminate\Support\Str::random(32).'.'.$image->getClientOriginalExtension();
                Image::make($image->getRealPath())->fit(812, 268)->save($destinationPath.'/'.$newFileName);
                $article['photo_full'] = 'uploads/articles/'. $newFileName;

                $article->save();
            }
        }

    }
}
