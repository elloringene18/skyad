<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call(UserTableSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(ArticleTypeSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(TypeSeeder::class);
        $this->call(CommunitySeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(PartnerSeeder::class);
    }
}
