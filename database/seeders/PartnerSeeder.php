<?php

namespace Database\Seeders;

use App\Models\Partner;
use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'image' => 'img/clients-hsbc.png',
                'name' => 'HSBC',
                'content' => '',
                'content_ar' => '',
            ],
            [
                'image' => 'img/clients-fab.png',
                'name' => 'fab',
                'content' => '',
                'content_ar' => '',
            ],
            [
                    'image' => 'img/clients-hany.png',
                    'name' => 'hany',
                    'content' => '',
                    'content_ar' => '',
            ],
            [
                    'image' => 'img/clients-rba.png',
                    'name' => 'rba',
                    'content' => '',
                    'content_ar' => '',
                ],

            ];

        foreach ($data as $item){
            $image =  new \Illuminate\Http\UploadedFile( public_path($item['image']), 'tmp.jpg', 'image/jpeg',null,true);

            $img = Image::make($image);
            $img->resize(260, 260);

            $destinationPath = 'public/uploads/partners';
            $newFileName = \Illuminate\Support\Str::random(32).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->fit(247, 146)->save($destinationPath.'/'.$newFileName);

            $item['image'] = 'uploads/partners/'. $newFileName;

            Partner::create($item);
        }
    }
}
