<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Community;
use App\Models\Level;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\Type;
use App\Models\Upload;
use App\Services\Uploaders\ProductImagesUploader;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Console\Command;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Symfony\Component\Console\Output\ConsoleOutput;

class ProductSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Product $model,Generator $faker, Command $command, ProductImagesUploader $uploader)
    {
        $this->model = $model;
        $this->faker = $faker;
        $this->command = $command;
        $this->uploader = $uploader;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [
            [
                'title' => 'S - GARDEN APARTMENT',
                'title_ar' => 'س – شقة جاردن',
                'area' => '110',
                'price' => '1284000',
                'price_max' => '3200000',
                'brand' => '',
                'bedrooms' => 4,
                'category' => 'residential',
                'description' => 'Nice garden area.',
                'description_ar' => 'Nice garden area.',
                'photo' => 'img/properties/v4/s-garden-apartment.jpg',
                'photo_full' => 'img/properties/v4/s-garden-apartment-full.jpg',
                'type' => 'garden-apartment',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'A-G1/G2',
                        'area' => '210',
                        'floors' => 'Ground Floor',
                        'floors_ar' => 'Ground Floor',
                        'rooms' => [
                            [ 'name' => 'Private Garden', 'value' => '95' ],
                            [ 'name' => 'Terrace 01', 'value' => '1.20 × 1.75' ],
                            [ 'name' => 'Entrance', 'value' => '2.87 × 2.04' ],
                            [ 'name' => 'Bedroom 03',  'value' => '3.72 × 3.91' ],
                            [ 'name' => 'Bedroom 01',  'value' => '3.72 × 4.11' ],
                            [ 'name' => 'Maid Room', 'value' => '2.13 × 1.67' ],
                            [ 'name' => 'Reception', 'value' => '3.99 × 7.55' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 4.11' ],
                            [ 'name' => 'Maid Toilet', 'value' => '2.13 × 1.03' ],
                            [ 'name' => 'Dining',  'value' => '2.44 × 3.64' ],
                            [ 'name' => 'Lobby 01',  'value' => '1.28 × 5.99' ],
                            [ 'name' => 'Kitchen',  'value' => '3.56 × 3.56' ],
                            [ 'name' => 'Guest Toilet',  'value' => '1.48 × 2.31' ],
                            [ 'name' => 'Lobby 02',  'value' => '1.30 × 1.82' ],
                            [ 'name' => 'Master Bedroom',  'value' => '3.79 × 4.09' ],
                            [ 'name' => 'Bathroom',  'value' => '1.82 × 2.81' ],
                            [ 'name' => 'Dressing',  'value' => '2.50 × 2.65' ],
                            [ 'name' => 'M. Bathroom',  'value' => '1.82 × 2.70' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-garden-apartment/ag1g2.JPG']
                    ],
                    [
                        'name' => 'A-G3',
                        'area' => '175',
                        'floors' => 'Ground Floor',
                        'rooms' => [
                            [ 'name' => 'Private Garden', 'value' => '70' ],
                            [ 'name' => 'Terrace 01', 'value' => '1.05 × 2.10' ],
                            [ 'name' => 'Entrance', 'value' => '1.52 × 2.26' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.08' ],
                            [ 'name' => 'Guest Toilet', 'value' => '1.58 × 1.97' ],
                            [ 'name' => 'Reception', 'value' => '8.66 × 4.07' ],
                            [ 'name' => 'Dressing', 'value' => '2.11 × 2.74' ],
                            [ 'name' => 'Bathroom', 'value' => '1.92 × 2.50' ],
                            [ 'name' => 'Living', 'value' => '3.78 × 3.73' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.00× 2.74' ],
                            [ 'name' => 'Kitchen', 'value' => '3.19 × 3.34' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.72' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 1.36' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-garden-apartment/ag3.JPG']
                    ],
                    [
                        'name' => 'B-G2',
                        'area' => '175',
                        'floors' => 'Ground Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.05 × 1.66' ],
                            [ 'name' => 'Private Garden', 'value' => '50' ],
                            [ 'name' => 'Entrance', 'value' => '1.79 × 2.81' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.29' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.74 × 1.32' ],
                            [ 'name' => 'Reception', 'value' => '7.75 × 4.36' ],
                            [ 'name' => 'Dressing', 'value' => '2.74 × 2.38' ],
                            [ 'name' => 'Bathroom', 'value' => '2.74 × 1.89' ],
                            [ 'name' => 'Living', 'value' => '3.66 × 3.72' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.74 × 1.91' ],
                            [ 'name' => 'Kitchen', 'value' => '2.80 × 3.11' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.72' ],
                            [ 'name' => 'Lobby 01', 'value' => '3.72 × 1.28' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-garden-apartment/bg2.JPG']
                    ],
                    [
                        'name' => 'B-G3/G6',
                        'area' => '150',
                        'floors' => 'Ground Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.42 × 1.00' ],
                            [ 'name' => 'Private Garden', 'value' => '75' ],
                            [ 'name' => 'Entrance', 'value' => '1.81 × 2.74' ],
                            [ 'name' => 'Master Bedroom', 'value' => '4.30 × 4.11' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.93 × 3.72' ],
                            [ 'name' => 'Reception', 'value' => '3.91 × 6.89' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.11 × 2.42' ],
                            [ 'name' => 'Bathroom', 'value' => '2.52 × 1.82' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.72 × 1.35' ],
                            [ 'name' => 'Dressing', 'value' => '1.80 × 2.42' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 7.64' ],
                            [ 'name' => 'Kitchen', 'value' => '3.06 × 2.10' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.79 × 3.91' ],
                            [ 'name' => 'Lobby 02', 'value' => '1.41 × 1.22' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-garden-apartment/bg3g6.JPG']
                    ],
                    [
                        'name' => 'B-G4/G5',
                        'area' => '110',
                        'floors' => 'Ground Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.42 × 1.00' ],
                            [ 'name' => 'Private Garden', 'value' => '39' ],
                            [ 'name' => 'Entrance', 'value' => '1.49 × 2.22' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.85 × 3.85' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.59 × 1.88' ],
                            [ 'name' => 'Reception', 'value' => '3.79 × 6.78' ],
                            [ 'name' => 'Bathroom', 'value' => '2.62 × 1.82' ],
                            [ 'name' => 'Lobby', 'value' => '1.32 × 2.72' ],
                            [ 'name' => 'Kitchen', 'value' => '2.50 × 3.45' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.89 × 3.91' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-garden-apartment/bg4g5.JPG']
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/4.jpg'
                ]
            ],
            [
                'title' => 'S - DELUXE APARTMENT',
                'title_ar' => 'س - شقة ديلوكس',
                'area' => '200',
                'price' =>  '2233000' ,
                'price_max' => '2983000',
                'brand' => '',
                'bedrooms' => 4,
                'category' => 'residential',
                'description' => 'Has 4 bedrooms and a nice view overlooking the club.',
                'description_ar' => 'Has 4 bedrooms and a nice view overlooking the club.',
                'photo' => 'img/properties/v4/s-deluxe.jpg',
                'photo_full' => 'img/properties/v4/s-deluxe-full.jpg',
                'type' => 'deluxe-apartment',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'A-11/12/21/22',
                        'floors' => 'First and Second Floors',
                        'area' => '220',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.10 × 8.30' ],
                            [ 'name' => 'Terrace 02', 'value' => '1.20 × 1.75' ],
                            [ 'name' => 'Entrance', 'value' => '2.87 × 2.04' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 4.11' ],
                            [ 'name' => 'Maid Room', 'value' => '2.13 × 1.67' ],
                            [ 'name' => 'Reception', 'value' => '3.99 × 7.55' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 4.11' ],
                            [ 'name' => 'Maid Toilet', 'value' => '2.13 × 1.03' ],
                            [ 'name' => 'Dining', 'value' => '2.44 × 3.64' ],
                            [ 'name' => 'Bedroom 03', 'value' => '3.72 × 3.91' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 5.99' ],
                            [ 'name' => 'Kitchen', 'value' => '3.56 × 3.56' ],
                            [ 'name' => 'Guest Toilet', 'value' => '1.48 × 2.31' ],
                            [ 'name' => 'Lobby 02', 'value' => '1.30 × 1.82' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.79 × 4.09' ],
                            [ 'name' => 'Bathroom', 'value' => '1.82 × 2.81' ],
                            [ 'name' => 'Dressing', 'value' => '2.50 × 2.65' ],
                            [ 'name' => 'M. Bathroom', 'value' => '1.82 × 2.70' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-deluxe-apartment/a11a12.JPG']
                    ],
                    [
                        'name' => 'A-31/32',
                        'area' => '210',
                        'floors' => 'Third Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.20 × 1.75' ],
                            [ 'name' => 'Terrace 02', 'value' => '7.90 × 1.82' ],
                            [ 'name' => 'Entrance', 'value' => '2.36 × 1.51' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 3.91' ],
                            [ 'name' => 'Reception', 'value' => '3.99 × 7.24' ],
                            [ 'name' => 'Bedroom 02', 'value' => '4.11 × 3.91' ],
                            [ 'name' => 'Dining', 'value' => '2.50 × 3.33' ],
                            [ 'name' => 'Bedroom 03', 'value' => '4.11 × 3.72' ],
                            [ 'name' => 'Kitchen', 'value' => '3.11 × 3.70' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.36 × 1.27' ],
                            [ 'name' => 'Master Bedroom', 'value' => '5.31 × 3.80' ],
                            [ 'name' => 'Bathroom', 'value' => '2.81 × 1.82' ],
                            [ 'name' => 'M. Bathroom', 'value' => '1.88 × 2.30' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 3.72' ],
                            [ 'name' => 'Dressing', 'value' => '2.10 × 2.30' ],
                            [ 'name' => 'Lobby 02', 'value' => '2.58 × 1.82' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-deluxe-apartment/a3132.JPG']
                    ],
                    [
                        'name' => 'A-41/42/51/52',
                        'floors' => 'Fourth and Fifth Floors',
                        'area' => '200',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.20 × 1.75' ],
                            [ 'name' => 'Terrace 02', 'value' => '3.85 × 1.42' ],
                            [ 'name' => 'Entrance', 'value' => '2.36 × 1.51' ],
                            [ 'name' => 'Bedroom 02', 'value' => '4.11 × 3.91' ],
                            [ 'name' => 'Reception', 'value' => '3.99 × 7.24' ],
                            [ 'name' => 'Bedroom 03', 'value' => '4.11 × 3.72' ],
                            [ 'name' => 'Dining', 'value' => '2.50 × 3.33' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.36 × 1.33' ],
                            [ 'name' => 'Kitchen', 'value' => '3.11 × 3.70' ],
                            [ 'name' => 'Bathroom', 'value' => '2.81 × 1.82' ],
                            [ 'name' => 'Master Bedroom', 'value' => '5.31 × 3.80' ],
                            [ 'name' => 'M. Bathroom', 'value' => '1.88 × 2.30' ],
                            [ 'name' => 'Dressing', 'value' => '2.10 × 2.30' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 3.72' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 3.91' ],
                            [ 'name' => 'Lobby 02', 'value' => '2.58 × 1.82' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-deluxe-apartment/a4142.JPG']
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/3.jpg'
                ]
            ],
            [
                'title' => 'S - SKY DUPLEX',
                'title_ar' => 'س - سكاي دوبلكس ',
                'area' => '360',
                'price' =>  '3980000' ,
                'price_max' => '4832000',
                'brand' => '',
                'bedrooms' => 3,
                'category' => 'residential',
                'description' => 'Nice view and roof.',
                'description_ar' => 'Nice view and roof.',
                'photo' => 'img/properties/v4/s-sky-duplex.jpg',
                'photo_full' => 'img/properties/v4/s-sky-duplex-full.jpg',
                'type' => 'sky-duplex',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'A-61/62',
                        'area' => '360',
                        'floors' => 'Sixth Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '8.09 × 2.36' ],
                            [ 'name' => 'Terrace 02', 'value' => '3.78 × 3.73' ],
                            [ 'name' => 'Terrace 03', 'value' => '1.20 × 1.75' ],
                            [ 'name' => 'Entrance', 'value' => '2.13 × 2.65' ],
                            [ 'name' => 'Guest Toilet', 'value' => '1.91 × 1.37' ],
                            [ 'name' => 'Lobby', 'value' => '3.06 × 1.28' ],
                            [ 'name' => 'Reception', 'value' => '7.53 × 5.01' ],
                            [ 'name' => 'Laundry', 'value' => '1.28 × 1.78' ],
                            [ 'name' => 'Stair', 'value' => '2.72 × 5.87' ],
                            [ 'name' => 'Dining', 'value' => '4.28 × 5.01' ],
                            [ 'name' => 'Maid Room', 'value' => '2.48 × 2.12' ],
                            [ 'name' => 'Kitchen', 'value' => '4.09 × 5.87' ],
                            [ 'name' => 'Maid Toilet', 'value' => '2.48 × 1.10' ],

                        ],
                        'photos' => ['img/properties/floorplans/V2/s-sky-duplex/a6162-1.jpg'],
                        'levels' => [
                            [
                                'name' => '2nd level',
                                'name_ar' => '2nd level',
                                'rooms' => [
                                    [ 'name' => 'Terrace 01', 'value' => '1.20 × 1.75' ],
                                    [ 'name' => 'Terrace 02', 'value' => '3.73 × 1.20' ],
                                    [ 'name' => 'Stair', 'value' => '2.68 X 5.87' ],
                                    [ 'name' => 'M. Bathroom', 'value' => '2.52 X 1.92' ],
                                    [ 'name' => 'Bedroom 03', 'value' => '4.09 X 3.79' ],
                                    [ 'name' => 'Master Bedroom', 'value' => '3.79 X 6.52' ],
                                    [ 'name' => 'Bedroom 01', 'value' => '3.98 X 5.01' ],
                                    [ 'name' => 'Bathroom', 'value' => '2.89 X 2.08' ],
                                    [ 'name' => 'Dressing', 'value' => '2.46 X 2.67' ],
                                    [ 'name' => 'Bedroom 02', 'value' => '4.11 X 5.01' ],
                                ],
                                'photos' => ['img/properties/floorplans/V2/s-sky-duplex/a6162-2.jpg'],
                            ]
                        ]
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/5.jpg'
                ]
            ],
            [
                'title' => 'S - PREMIUM APARTMENT',
                'title_ar' => 'س – شقة بريميم ',
                'area' => '190',
                'price' =>  '1874560',
                'price_max' => '2415000',
                'brand' => '',
                'bedrooms' => 3,
                'category' => 'residential',
                'description' => 'Spacious area with an extra living room.',
                'description_ar' => 'Spacious area with an extra living room.',
                'photo' => 'img/properties/v4/s-premium.jpg',
                'photo_full' => 'img/properties/v4/s-premium-full.jpg',
                'type' => 'premium-apartment',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'A-13/23/24/33/34/43/44/53/54/63/64/73/74',
                        'area' => '190',
                        'floors' => 'First, Second, Third, Fourth, Fifth, Sixth & Seventh Floors.',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '2.15 × 6.60' ],
                            [ 'name' => 'Terrace 02', 'value' => '0.30 × 2.44' ],
                            [ 'name' => 'Terrace 03', 'value' => '0.30 × 2.44' ],
                            [ 'name' => 'Terrace 04', 'value' => '1.05 × 2.10' ],
                            [ 'name' => 'Entrance', 'value' => '1.52 × 2.26' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.09' ],
                            [ 'name' => 'Guest Toilet', 'value' => '1.58 × 1.97' ],
                            [ 'name' => 'Reception', 'value' => '8.66 × 4.07' ],
                            [ 'name' => 'Dressing', 'value' => '2.11 × 2.75' ],
                            [ 'name' => 'Bathroom', 'value' => '1.92 × 2.50' ],
                            [ 'name' => 'Living', 'value' => '3.78 × 3.73' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.00 × 2.75' ],
                            [ 'name' => 'Kitchen', 'value' => '3.19 × 3.34' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.72' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 1.36' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-premium-apartment/a13.JPG']
                    ],
                    [
                        'name' => 'B-12/21/22',
                        'area' => '190',
                        'floors' => 'First and Second Floors',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.05 × 1.66' ],
                            [ 'name' => 'Terrace 02', 'value' => '0.30 × 2.44' ],
                            [ 'name' => 'Terrace 03', 'value' => '3.32 × 5.93' ],
                            [ 'name' => 'Entrance', 'value' => '1.79 × 2.81' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.29' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.74 × 1.32' ],
                            [ 'name' => 'Reception', 'value' => '7.75 × 4.36' ],
                            [ 'name' => 'Dressing', 'value' => '2.74 × 2.38' ],
                            [ 'name' => 'Bathroom', 'value' => '2.74 × 1.89' ],
                            [ 'name' => 'Living', 'value' => '3.66 × 3.72' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.74 × 1.91' ],
                            [ 'name' => 'Kitchen', 'value' => '2.80 × 3.11' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.72' ],
                            [ 'name' => 'Lobby 01', 'value' => '3.72 × 1.28' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-premium-apartment/b12.JPG']
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/3.jpg'
                ]
            ],
            [
                'title' => 'S - GARDEN DUPLEX',
                'title_ar' => 'س – جاردن دوبلكس',
                'area' => '245',
                'price' =>  '2800730' ,
                'price_max' => '3170000',
                'brand' => '',
                'bedrooms' => 3,
                'category' => 'residential',
                'description' => 'Nice garden area.',
                'description_ar' => 'Nice garden area.',
                'photo' => 'img/properties/v4/s-garden-duplex.jpg',
                'photo_full' => 'img/properties/v4/s-garden-duplex-full.jpg',
                'type' => 'garden-duplex',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'A-G4',
                        'area' => '245',
                        'floors' => 'Ground Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.05 × 2.10' ],
                            [ 'name' => 'Private Garden', 'value' => '58' ],
                            [ 'name' => 'Stairs', 'value' => '2.83 × 3.25' ],
                            [ 'name' => 'Kitchen', 'value' => '3.78 × 4.29' ],
                            [ 'name' => 'Maid Room', 'value' => '1.49 × 3.00' ],
                            [ 'name' => 'Reception', 'value' => '4.19 × 7.57' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.46 × 1.32' ],
                            [ 'name' => 'Maid Toilet', 'value' => '1.88 × 1.18' ],
                            [ 'name' => 'Dining', 'value' => '3.51 × 3.91' ],
                            [ 'name' => 'Laundry', 'value' => '1.12 × 2.75' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.32 × 1.32' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-garden-duplex/ag4-1.JPG'],
                        'levels' => [
                            [
                                'name' => '2nd level',
                                'name_ar' => '2nd level',
                                'rooms' => [
                                    [ 'name' => 'Terrace 01', 'value' => '1.05 × 2.10' ],
                                    [ 'name' => 'Terrace 02', 'value' => '0.30 × 2.44' ],
                                    [ 'name' => 'Terrace 03', 'value' => '0.30 × 2.44' ],
                                    [ 'name' => 'Living', 'value' => '3.79 × 3.78' ],
                                    [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.91' ],
                                    [ 'name' => 'M. Bathroom', 'value' => '2.75 × 2.00' ],
                                    [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.16' ],
                                    [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.79' ],
                                    [ 'name' => 'Lobby', 'value' => '1.85 × 1.28' ],
                                    [ 'name' => 'Dressing', 'value' => '2.12 × 2.75' ],
                                    [ 'name' => 'Bathroom', 'value' => '1.85 × 2.50' ],
                                ],
                                'photos' => ['img/properties/floorplans/V2/s-garden-duplex/ag4-2.JPG'],
                            ]
                        ]
                    ],
                    [
                        'name' => 'B-G1',
                        'area' => '255',
                        'floors' => 'Ground Floor',
                        'rooms' => [
                            [ 'name' => 'Private Garden', 'value' => '55' ],
                            [ 'name' => 'Terrace 01', 'value' => '1.24 × 1.66' ],
                            [ 'name' => 'Stairs', 'value' => '2.74 × 3.43' ],
                            [ 'name' => 'Store', 'value' => '2.74 × 1.00' ],
                            [ 'name' => 'Maid Room', 'value' => '2.74 × 2.40' ],
                            [ 'name' => 'Reception', 'value' => '7.63 × 4.31' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 2.92' ],
                            [ 'name' => 'Maid Toilet', 'value' => '2.74 × 1.19' ],
                            [ 'name' => 'Dining', 'value' => '3.66 × 3.39' ],
                            [ 'name' => 'Guest Toilet', 'value' => '1.12 × 2.75' ],
                            [ 'name' => 'Kitchen', 'value' => '3.72 × 5.05' ],

                        ],
                        'photos' => ['img/properties/floorplans/V2/s-garden-duplex/bg1-1.jpg'],
                        'levels' => [
                            [
                                'name' => '2nd level',
                                'name_ar' => '2nd level',
                                'rooms' => [
                                    [ 'name' => 'Terrace 01', 'value' => '1.05 × 1.65' ],
                                    [ 'name' => 'Terrace 02', 'value' => '0.30 × 2.44' ],
                                    [ 'name' => 'Entrance', 'value' => '1.80 × 1.50' ],
                                    [ 'name' => 'Lobby 01', 'value' => '3.72 × 1.28' ],
                                    [ 'name' => 'M. Bathroom', 'value' => '2.74 × 1.92' ],
                                    [ 'name' => 'Living', 'value' => '7.63 × 4.31' ],
                                    [ 'name' => 'Dressing', 'value' => '2.74 × 2.37' ],
                                    [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.91' ],
                                    [ 'name' => 'Bathroom', 'value' => '2.74 × 1.89' ],
                                    [ 'name' => 'M. Bedroom', 'value' => '3.91 × 4.29' ],
                                    [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.91' ],
                                ],
                                'photos' => ['img/properties/floorplans/V2/s-garden-duplex/bg1-2.jpg'],
                            ]
                        ]
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/7.jpg'
                ]
            ],
            [
                'title' => 'S - TERRACE APARTMENT',
                'title_ar' => 'س – شقق تراس',
                'area' => '180',
                'price' =>   '1776000',
                'price_max' => '2414000',
                'brand' => '',
                'bedrooms' => 3,
                'category' => 'residential',
                'description' => 'They have big terrace.',
                'description_ar' => 'They have big terrace.',
                'photo' => 'img/properties/v4/s-terrace.jpg',
                'photo_full' => 'img/properties/v4/s-terrace-full.jpg',
                'type' => 'terrace-apartment',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'B-31/32',
                        'area' => '195',
                        'floors' => 'Third Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.05 × 1.66' ],
                            [ 'name' => 'Terrace 02', 'value' => '0.30 × 2.44' ],
                            [ 'name' => 'Terrace 03', 'value' => '3.32 × 5.93' ],
                            [ 'name' => 'Terrace 04', 'value' => '2.44 × 3.34' ],
                            [ 'name' => 'Kitchen', 'value' => '2.74 × 3.11' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.74 × 1.32' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.29' ],
                            [ 'name' => 'Bathroom', 'value' => '2.74 × 1.89' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.74 × 1.92' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 3.78' ],
                            [ 'name' => 'Dressing', 'value' => '2.74 × 1.92' ],
                            [ 'name' => 'Lobby 02', 'value' => '2.56 × 1.22' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.72' ],
                            [ 'name' => 'Reception', 'value' => '7.72 × 4.49' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.85 × 4.10' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-terrace-apartment/b3132.JPG']
                    ],
                    [
                        'name' => 'B-41/42/51/52/61/62/71/72',
                        'area' => '180',
                        'floors' => 'Fourth, Fifth, Sixth & Seventh Floors.',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.05 × 1.66' ],
                            [ 'name' => 'Terrace 02', 'value' => '0.30 × 2.47' ],
                            [ 'name' => 'Terrace 03', 'value' => '1.82 × 5.93' ],
                            [ 'name' => 'Terrace 04', 'value' => '2.44 × 1.50' ],
                            [ 'name' => 'Reception', 'value' => '7.72 × 4.49' ],
                            [ 'name' => 'Dressing', 'value' => '2.74 × 2.37' ],
                            [ 'name' => 'Bathroom', 'value' => '2.74 × 1.89' ],
                            [ 'name' => 'Kitchen', 'value' => '2.74 × 3.11' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.72' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 3.78' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.29' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.85 × 4.10' ],
                            [ 'name' => 'Lobby 02', 'value' => '2.56 × 1.22' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.74 × 1.92' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.74 × 1.32' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-terrace-apartment/b4142.JPG']
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/6.jpg'
                ]
            ],
            [
                'title' => 'S - CORNER APARTMENT',
                'title_ar' => 'س – شقة كورنر',
                'area' => '160',
                'price' =>  '1760000' ,
                'price_max' => '2162000',
                'brand' => '',
                'bedrooms' => 3,
                'category' => 'residential',
                'description' => 'Overlooking a nice view as it’s in a corner.',
                'description_ar' => 'Overlooking a nice view as it’s in a corner.',
                'photo' => 'img/properties/v4/s-corner.jpg',
                'photo_full' => 'img/properties/v4/s-corner-full.jpg',
                'type' => 'corner-apartment',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'B-13/16/23/26/33/36/43/46/53/56/63/66/73/76',
                        'area' => '160',
                        'floors' => 'First, Second, Third, Fourth, Fifth, Sixth & Seventh Floors.',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '1.10 × 4.20' ],
                            [ 'name' => 'Terrace 02', 'value' => '0.30 × 1.64' ],
                            [ 'name' => 'Terrace 03', 'value' => '0.30 × 2.44' ],
                            [ 'name' => 'Terrace 04', 'value' => '0.30 × 2.44' ],
                            [ 'name' => 'Terrace 05', 'value' => '1.42 × 1.00' ],
                            [ 'name' => 'Entrance', 'value' => '1.81 × 2.74' ],
                            [ 'name' => 'Dressing', 'value' => '1.80 × 2.42' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.72 × 1.35' ],
                            [ 'name' => 'Reception', 'value' => '3.91 × 6.89' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.93 × 3.72' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.11 × 2.42' ],
                            [ 'name' => 'Kitchen', 'value' => '3.06 × 2.10' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.79 × 3.91' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.28 × 7.64' ],
                            [ 'name' => 'Master Bedroom', 'value' => '4.30 × 4.11' ],
                            [ 'name' => 'Bathroom', 'value' => '2.52 × 1.82' ],
                            [ 'name' => 'Lobby 02', 'value' => '1.41 × 1.22' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-corner-apartment/b1316.JPG']
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/5.jpg'
                ]
            ],
            [
                'title' => 'S - TYPICAL APARTMENT',
                'title_ar' => 'س - شقة تيبيكال',
                'area' => '115',
                'price' =>   '1209980' ,
                'price_max' => '2275000',
                'brand' => '',
                'bedrooms' => 2,
                'category' => 'residential',
                'description' => '',
                'description_ar' => '',
                'photo' => 'img/properties/v4/s-typical.jpg',
                'photo_full' => 'img/properties/v4/s-typical-full.jpg',
                'type' => 'typical-apartment',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'B-14/15/24/25/34/35/44/45/54/55/64/65/74/75',
                        'floors' => 'First, Second, Third, Fourth, Fifth, Sixth & Seventh Floors.',
                        'area' => '115',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '3.72 × 1.40' ],
                            [ 'name' => 'Terrace 02', 'value' => '1.42 × 1.00' ],
                            [ 'name' => 'Terrace 03', 'value' => '2.74 × 0.30' ],
                            [ 'name' => 'Entrance', 'value' => '1.49 × 2.22' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.85 × 3.85' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.89 × 3.91' ],
                            [ 'name' => 'Reception', 'value' => '3.79 × 6.78' ],
                            [ 'name' => 'Lobby', 'value' => '1.32 × 2.72' ],
                            [ 'name' => 'M. Bathroom', 'value' => '2.59 × 1.88' ],
                            [ 'name' => 'Kitchen', 'value' => '2.50 × 3.45' ],
                            [ 'name' => 'Bathroom', 'value' => '2.62 × 1.82' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/s-typical-apartment/b1415.JPG']
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/4.jpg'
                ]
            ],
            [
                'title' => 'K - GARDEN APARTMENT',
                'title_ar' => 'ك - شقة جاردن',
                'area' => '110',
                'price' => '1284000',
                'price_max' => '3200000',
                'brand' => '',
                'bedrooms' => 5,
                'category' => 'residential',
                'description' => 'Nice garden area',
                'description_ar' => 'Nice garden area',
                'photo' => 'img/properties/v4/k-garden.jpg',
                'photo_full' => 'img/properties/v4/k-garden-full.jpg',
                'type' => 'garden-apartment',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'A-G1/G2 B-G3/G4',
                        'area' => '150',
                        'floors' => 'Ground Floor',
                        'rooms' => [
                            [ 'name' => 'Private Garden', 'value' => 'from 45 to 60' ],
                            [ 'name' => 'Entrance', 'value' => '1.86 × 3.52' ],
                            [ 'name' => 'Kitchen', 'value' => '4.02 × 2.40' ],
                            [ 'name' => 'M. Bedroom', 'value' => '3.91 × 4.06' ],
                            [ 'name' => 'Reception', 'value' => '4.41 × 6.56' ],
                            [ 'name' => 'Bathroom', 'value' => '1.85 × 2.46' ],
                            [ 'name' => 'M. Bathroom', 'value' => '1.98 × 2.68' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.19 × 1.57' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 3.91' ],
                            [ 'name' => 'Dressing', 'value' => '1.99 × 2.68' ],
                            [ 'name' => 'Lobby 01', 'value' => '2.54 × 1.28' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'Lobby 02', 'value' => '2.74 × 1.28' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/k-garden-apartment/a12b34.JPG']
                    ],
                    [
                        'name' => 'A-G3/G4 – G1/G2',
                        'area' => '110',
                        'floors' => 'Ground Floor',
                        'rooms' => [
                            [ 'name' => 'Private Garden', 'value' => 'from 35 to 45' ],
                            [ 'name' => 'Reception', 'value' => '4.56 × 5.97' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'Lobby 01', 'value' => '1.25 × 1.52' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.78 × 4.06' ],
                            [ 'name' => 'Kitchen', 'value' => '3.33 × 3.19' ],
                            [ 'name' => 'M. Bathroom', 'value' => '1.98 × 2.68' ],
                            [ 'name' => 'Bathroom', 'value' => '2.73 × 2.22' ],
                            [ 'name' => 'Dressing', 'value' => '1.86 × 2.68' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/k-garden-apartment/a34b12.JPG']
                    ],
//                    [
//                        'name' => 'A- G1/G2 B- G3/G4',
//                        'area' => '150',
//                        'floors' => 'Ground Floor',
//                        'rooms' => [
//                            [ 'name' => 'Private Garden', 'value' => 'from 45 to 60' ],
//                            [ 'name' => 'Entrance', 'value' => '1.86 × 3.52' ],
//                            [ 'name' => 'Kitchen', 'value' => '4.02 × 2.40' ],
//                            [ 'name' => 'M. Bedroom', 'value' => '3.91 × 4.06' ],
//                            [ 'name' => 'Reception', 'value' => '4.41 × 6.56' ],
//                            [ 'name' => 'Bathroom', 'value' => '1.85 × 2.46' ],
//                            [ 'name' => 'M. Bathroom', 'value' => '1.98 × 2.68' ],
//                            [ 'name' => 'Guest Toilet', 'value' => '2.19 × 1.57' ],
//                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 3.91' ],
//                            [ 'name' => 'Dressing', 'value' => '1.99 × 2.68' ],
//                            [ 'name' => 'Lobby 01', 'value' => '2.54 × 1.28' ],
//                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.91' ],
//                            [ 'name' => 'Lobby 02', 'value' => '2.74 × 1.28' ],
//                        ],
//                        'photos' => ['img/properties/floorplans/V2/k-garden-apartment/ag1.JPG']
//                    ],
//                    [
//                        'name' => 'A- G3/G4 B- G1/G2',
//                        'area' => '110',
//                        'floors' => 'Ground Floor',
//                        'rooms' => [
//                            [ 'name' => 'Private Garden', 'value' => 'from 35 to 45' ],
//                            [ 'name' => 'Reception', 'value' => '4.56 × 5.97' ],
//                            [ 'name' => 'Bathroom', 'value' => '2.73 × 2.22' ],
//                            [ 'name' => 'M. Bathroom', 'value' => '1.98 × 2.68' ],
//                            [ 'name' => 'Lobby 01', 'value' => '1.25 × 1.52' ],
//                            [ 'name' => 'Bedroom 01', 'value' => '3.91 × 3.91' ],
//                            [ 'name' => 'Dressing', 'value' => '1.99 × 2.68' ],
//                            [ 'name' => 'Kitchen', 'value' => '3.33 × 3.19' ],
//                            [ 'name' => 'M. Bedroom', 'value' => '3.78 × 4.06' ],
//                        ],
//                        'photos' => ['img/properties/floorplans/V2/k-garden-apartment/ag3.JPG']
//                    ],
//                    [
//                        'name' => 'A-31/32/51/52',
//                        'area' => '160',
//                        'floors' => 'Third & Fourth Floor',
//                        'rooms' => [
//                            [ 'name' => 'Terrace 01', 'value' => '6.77 × 0.73' ],
//                            [ 'name' => 'Terrace 02', 'value' => '4.35 × 1.50' ],
//                            [ 'name' => 'Entrance', 'value' => '1.86 × 3.52' ],
//                            [ 'name' => 'Kitchen', 'value' => '4.02 x 2.40' ],
//                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.91' ],
//                            [ 'name' => 'Reception', 'value' => '4.29 x 6.56' ],
//                            [ 'name' => 'Lobby 02', 'value' => '2.74 x 1.28' ],
//                            [ 'name' => 'M. Bedroom', 'value' => '3.91 × 4.06' ],
//                            [ 'name' => 'Guest Toilet', 'value' => '2.19 × 1.57' ],
//                            [ 'name' => 'Bathroom', 'value' => '1.85 × 2.46' ],
//                            [ 'name' => 'M. Bathroom', 'value' => '1.98 × 2.68' ],
//                            [ 'name' => 'Lobby 01', 'value' => '2.54 × 1.28' ],
//                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 3.91' ],
//                            [ 'name' => 'Dressing', 'value' => '1.99 × 2.68' ],
//                        ],
//                        'photos' => ['img/properties/floorplans/V2/k-garden-apartment/a31.JPG']
//                    ],
                ],
                'gallery' => [
                    'img/properties/interior/3.jpg'
                ]
            ],
            [
                'title' => 'K - TYPICAL APARTMENT',
                'title_ar' => 'ك - شقة تيبيكال',
                'area' => '155',
                'price' => '1209980',
                'price_max' => '2275000',
                'brand' => '',
                'bedrooms' => 3,
                'category' => 'residential',
                'description' => 'Not your typical type of home.',
                'description_ar' => 'Not your typical type of home.',
                'photo' => 'img/properties/v4/k-typical.jpg',
                'photo_full' => 'img/properties/v4/k-typical-full.jpg',
                'type' => 'typical-apartment',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => ' A-11/12/13/14/21/22/23/24/41/42/43/44/61/62/63/64 <br/>B–11/12/13/14/21/22/23/24/41/42/43/44/61/62/63/64',
                        'area' => '155',
                        'floors' => 'First, Second, Fourth, & Sixth Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '2.44 × 0.30' ],
                            [ 'name' => 'Terrace 02', 'value' => '2.44 × 0.30' ],
                            [ 'name' => 'Terrace 03', 'value' => '3.83 × 1.50' ],
                            [ 'name' => 'Entrance', 'value' => '1.63 × 3.52' ],
                            [ 'name' => 'Bathroom', 'value' => '1.85 × 2.46' ],
                            [ 'name' => 'Reception', 'value' => '4.29 x 6.51' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 3.91' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.19 × 1.57' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'Lobby 01', 'value' => '2.54 × 1.28' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.06' ],
                            [ 'name' => 'Kitchen', 'value' => '4.02 x 2.40' ],
                            [ 'name' => 'M. Bathroom', 'value' => '1.98 × 2.68' ],
                            [ 'name' => 'Lobby 02', 'value' => '2.74 x 1.28' ],
                            [ 'name' => 'Dressing', 'value' => '1.99 × 2.68' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/k-typical-apartment/a121314b11121314.JPG']
                    ],
                    [
                        'name' => 'A-33/34/53/54 <br/>B-31/32/33/34/51/52/53/54',
                        'area' => '155',
                        'floors' => 'Third Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '2.44 × 0.30' ],
                            [ 'name' => 'Terrace 02', 'value' => '2.44 × 0.30' ],
                            [ 'name' => 'Terrace 03', 'value' => '4.35 × 1.50' ],
                            [ 'name' => 'Entrance', 'value' => '1.86 × 3.52' ],
                            [ 'name' => 'Bathroom', 'value' => '1.85 × 2.46' ],
                            [ 'name' => 'Reception', 'value' => '4.29 x 6.56' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 3.91' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.19 × 1.57' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'Lobby 01', 'value' => '2.54 × 1.28' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.78 × 4.06' ],
                            [ 'name' => 'Kitchen', 'value' => '4.02 x 2.40' ],
                            [ 'name' => 'M. Bathroom', 'value' => '1.98 × 2.68' ],
                            [ 'name' => 'Lobby 02', 'value' => '2.74 x 1.28' ],
                            [ 'name' => 'Dressing', 'value' => '1.86 × 2.68' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/k-typical-apartment/a3334b31323334.JPG']
                    ],
                    [
                        'name' => 'A-71/73 B-71/73',
                        'area' => '155',
                        'floors' => 'Seventh Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '2.44 × 0.30' ],
                            [ 'name' => 'Terrace 02', 'value' => '2.44 × 0.30' ],
                            [ 'name' => 'Terrace 03', 'value' => '3.95 × 1.23' ],
                            [ 'name' => 'Entrance', 'value' => '1.63 × 3.52' ],
                            [ 'name' => 'Bathroom', 'value' => '1.85 × 2.46' ],
                            [ 'name' => 'Reception', 'value' => '4.29 x 6.51' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 3.91' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.19 × 1.57' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'Lobby 01', 'value' => '2.54 × 1.28' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.06' ],
                            [ 'name' => 'Kitchen', 'value' => '4.02 x 2.40' ],
                            [ 'name' => 'M. Bathroom', 'value' => '1.98 × 2.68' ],
                            [ 'name' => 'Lobby 02', 'value' => '2.74 x 1.28' ],
                            [ 'name' => 'Dressing', 'value' => '1.99 × 2.68' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/k-typical-apartment/a7173b7173.JPG']
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/2.jpg'
                ]
            ],
            [
                'title' => 'K - HORIZON APARTMENT',
                'title_ar' => 'ك - شقة هورزن',
                'area' => '245',
                'price' => '2876000',
                'price_max' => '3637000',
                'brand' => '',
                'bedrooms' => 3,
                'category' => 'residential',
                'description' => 'Spacious outdoor area overlooking the valley',
                    'description_ar' => 'Spacious outdoor area overlooking the valley',
                    'photo' => 'img/properties/v4/k-horizon.jpg',
                    'photo_full' => 'img/properties/v4/k-horizon-full.jpg',
                'type' => 'horizon-apartment',
                'community' => 'residence-eight',
                'floorplans' => [
                    [
                        'name' => 'A-72 B-72',
                        'area' => '245',
                        'floors' => 'Seventh Floor',
                        'rooms' => [
                            [ 'name' => 'Terrace 01', 'value' => '2.44 × 0.30' ],
                            [ 'name' => 'Terrace 02', 'value' => '2.44 × 0.30' ],
                            [ 'name' => 'Terrace 03', 'value' => '4.07 × 1.23' ],
                            [ 'name' => 'Terrace 04', 'value' => '4.55 × 1.50' ],
                            [ 'name' => 'Outdoor Terrace', 'value' => '10.45 × 7.37' ],
                            [ 'name' => 'Entrance', 'value' => '1.81 × 3.22' ],
                            [ 'name' => 'Laundry', 'value' => '1.88 × 2.46' ],
                            [ 'name' => 'Reception', 'value' => '5.06 x 4.69' ],
                            [ 'name' => 'Bathroom', 'value' => '1.85 × 2.46' ],
                            [ 'name' => 'Dining', 'value' => '4.27 × 3.56' ],
                            [ 'name' => 'Bedroom 01', 'value' => '3.72 × 3.91' ],
                            [ 'name' => 'Guest Toilet', 'value' => '2.19 × 1.57' ],
                            [ 'name' => 'Bedroom 02', 'value' => '3.91 × 3.91' ],
                            [ 'name' => 'Living', 'value' => '4.29 × 6.56' ],
                            [ 'name' => 'Master Bedroom', 'value' => '3.91 × 4.06' ],
                            [ 'name' => 'Kitchen', 'value' => '4.00 × 3.97' ],
                            [ 'name' => 'M. Bathroom', 'value' => '1.98 × 2.68' ],
                            [ 'name' => 'Lobby 01', 'value' => '5.28 × 1.28' ],
                            [ 'name' => 'Dressing', 'value' => '1.99 × 2.68' ],
                        ],
                        'photos' => ['img/properties/floorplans/V2/k-horizon-apartment/a72b72.JPG']
                    ],
                ],
                'gallery' => [
                    'img/properties/interior/1.jpg'
                ]
            ],
        ];

        foreach($data as $item){
            $input = [];

            $slug = Str::slug($item['category']);

            $category = Category::where('slug',$slug)->first();

            if(!$category)
                $category = Category::create(['name'=>$item['category'],'slug'=>$slug]);


            $type = Type::where('slug',$item['type'])->first();
            $community = Community::where('slug',$item['community'])->first();

            $input = [
                'title' => $item['title'],
                'title_ar' => $item['title_ar'],
                'slug' => $this->generateSlug($item['title']),
                'photo' => null,
                'area' => $item['area'],
                'photo_full' => null,
                'bedrooms' => $item['bedrooms'],
                'category_id' => $category->id,
                'is_faved' => (rand(1,10) >= 5 ? 1 : 0),
                'description' => $item['description'],
                'description_ar' => $item['description_ar'],
                'price' => isset($item['price']) ? floatval($item['price']) : 0,
                'type_id' => $type->id,
                'community_id' => $community->id
            ];

            if(isset($item['photo'])){
                $image =  new UploadedFile( public_path($item['photo']), 'tmp.jpg', 'image/jpeg',null,true);

                $img = Image::make($image);
                $img->resize(260, 260);

                $destinationPath = 'public/uploads/properties';
                $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->fit(398, 274)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/properties/'. $newFileName;

                $image_full =  new UploadedFile( public_path($item['photo_full']), 'tmp.jpg', 'image/jpeg',null,true);

                $newFileName = Str::random(32).'.'.$image_full->getClientOriginalExtension();
                Image::make($image_full->getRealPath())->save($destinationPath.'/'.$newFileName);
                $input['photo_full'] = 'uploads/properties/'. $newFileName;
            }

            $newproduct = Product::create($input);

            if($newproduct){

                if(isset($item['floorplans'])){
                    foreach($item['floorplans'] as $floorplan){
                        $d = [];
                        $d['name_ar'] = $floorplan['name'];
                        $d['name'] = $floorplan['name'];
                        $d['area'] = $floorplan['area'];
                        $d['floors'] = $floorplan['floors'];
                        $newFloorplan = $newproduct->floorplans()->create($d);

                        if(isset($floorplan['photos'])){
                            foreach($floorplan['photos'] as $photo){
                                $image =  new UploadedFile( public_path($photo), 'tmp.jpg', 'image/jpeg',null,true);

                                $img = Image::make($image);
                                $img->resize(260, 260);

                                $destinationPath = 'public/uploads/properties/floorplans';
                                $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                                Image::make($image->getRealPath())->save($destinationPath.'/'.$newFileName);

                                $newFloorplan->photos()->create(['url'=>'uploads/properties/floorplans/'. $newFileName]);
                            }
                        }

                        foreach($floorplan['rooms'] as $room){
                            $room['name_ar'] = $room['name'];
                            $newFloorplan->rooms()->create($room);
                        }

                        if(isset($floorplan['levels'])){
                            foreach($floorplan['levels'] as $level){

                                $newLevel = $newFloorplan->levels()->create([
                                    'name' => $level['name'],
                                    'name_ar' => $level['name_ar']
                                ]);

                                if(isset($level['photos'])){
                                    foreach($level['photos'] as $photo){
                                        $image =  new UploadedFile( public_path($photo), 'tmp.jpg', 'image/jpeg',null,true);

                                        $img = Image::make($image);
                                        $img->resize(260, 260);

                                        $destinationPath = 'public/uploads/properties/floorplans';
                                        $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                                        Image::make($image->getRealPath())->save($destinationPath.'/'.$newFileName);

                                        $newLevel->photos()->create(['url'=>'uploads/properties/floorplans/'. $newFileName]);
                                    }
                                }

                                foreach($level['rooms'] as $room){
                                    $room['name_ar'] = $room['name'];
                                    $newLevel->rooms()->create($room);
                                }
                            }
                        }
                    }
                }

                if(isset($item['gallery'])){
                    foreach ($item['gallery'] as $photoUrl) {
                        $image =  new UploadedFile( public_path($photoUrl), 'tmp.jpg', 'image/jpeg',null,true);
                        $photo = ($image != null ? $this->uploader->upload($image) : false);
                        if ($photo)
                            $newproduct->uploads()->createMany($photo);
                    }
                }

            }
        }
    }

}
