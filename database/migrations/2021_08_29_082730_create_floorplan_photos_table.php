<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFloorplanPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('floorplan_photos', function (Blueprint $table) {
            $table->id();
            $table->string('caption')->nullable();
            $table->string('caption_ar')->nullable();
            $table->string('url');
            $table->bigInteger('floorplan_id')->unsigned()->index();
            $table->foreign('floorplan_id')->references('id')->on('floorplans')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('floorplan_photos');
    }
}
