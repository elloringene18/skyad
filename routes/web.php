<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'ar','as'=>'ar.'], function(){
    Route::get('/', 'PageController@showPageAr');
    Route::get('property-listings','PropertiesController@indexAr');
    Route::get('property-listings/{slug}','PropertiesController@showAr');
    Route::get('news-and-press','PressController@indexAr');
    Route::get('news-and-press/{slug}','PressController@showAr');
    Route::get('/{page}', 'PageController@showPageAr');
});

Route::get('property-listings','PropertiesController@index');
Route::get('property-listings/{slug}','PropertiesController@show');
Route::get('news-and-press','PressController@index');
Route::get('news-and-press/{slug}','PressController@show');


Route::get('admin/login', 'Admin\AuthController@index')->name('login');
Route::post('admin/login', 'Admin\AuthController@customLogin')->name('login.custom');
Route::post('/contact','FormController@contact');
Route::post('/careers/submit','ApplicationController@store');
Route::post('/subscribe','FormController@subscribe');

Route::group(['prefix' => 'admin','middleware' => ['auth']], function() {
    Route::get('/', function () {
        redirect('admin/products');
    });

    Route::get('dashboard', 'Admin\AuthController@dashboard');
    Route::get('registration', 'Admin\AuthController@registration');
    Route::post('custom-registration', 'Admin\AuthController@customRegistration');
    Route::get('signout', 'Admin\AuthController@signOut');


    Route::group(['prefix' => 'products'], function() {
        Route::get('/','Admin\ProductController@index');
        Route::get('/create','Admin\ProductController@create');
        Route::post('/store','Admin\ProductController@store');
        Route::post('/update','Admin\ProductController@update');
        Route::get('/delete/{id}','Admin\ProductController@delete');
        Route::get('/delete-slide/{id}','Admin\ProductController@deleteSlide');

        Route::group(['prefix' => 'floorplans'], function() {
            Route::get('/create/{id}','Admin\FloorPlanController@create');
            Route::post('/store','Admin\FloorPlanController@store');
            Route::post('/update','Admin\FloorPlanController@update');
            Route::get('/delete/{id}','Admin\FloorPlanController@delete');
            Route::get('/edit/{id}','Admin\FloorPlanController@edit');
            Route::get('/{id}','Admin\FloorPlanController@index');
        });

        Route::get('/{id}','Admin\ProductController@edit');
    });

    Route::group(['prefix' => 'categories'], function() {
        Route::get('/','Admin\CategoryController@index');
        Route::post('/store','Admin\CategoryController@store');
        Route::get('/create','Admin\CategoryController@create');
        Route::post('/update','Admin\CategoryController@update');
        Route::get('/delete/{id}','Admin\CategoryController@delete');
        Route::get('/attribute/delete/{id}','Admin\CategoryController@deleteAttribute');
        Route::get('/value/delete/{id}','Admin\CategoryController@deleteValue');
        Route::get('/{id}','Admin\CategoryController@edit');
    });

    Route::group(['prefix' => 'types'], function() {
        Route::get('/','Admin\TypeController@index');
        Route::post('/store','Admin\TypeController@store');
        Route::get('/create','Admin\TypeController@create');
        Route::post('/update','Admin\TypeController@update');
        Route::get('/delete/{id}','Admin\TypeController@delete');
        Route::get('/attribute/delete/{id}','Admin\TypeController@deleteAttribute');
        Route::get('/value/delete/{id}','Admin\TypeController@deleteValue');
        Route::get('/{id}','Admin\TypeController@edit');
    });

    Route::group(['prefix' => 'projects'], function() {
        Route::get('/','Admin\ProjectController@index');
        Route::post('/store','Admin\ProjectController@store');
        Route::get('/create','Admin\ProjectController@create');
        Route::post('/update','Admin\ProjectController@update');
        Route::get('/delete/{id}','Admin\ProjectController@delete');
        Route::get('/attribute/delete/{id}','Admin\ProjectController@deleteAttribute');
        Route::get('/value/delete/{id}','Admin\ProjectController@deleteValue');
        Route::get('/{id}','Admin\ProjectController@edit');
    });

    Route::group(['prefix' => 'news'], function() {

        Route::group(['prefix' => 'types'], function() {
            Route::get('/','Admin\NewsController@types');
            Route::post('/store','Admin\NewsController@storeTypes');
            Route::get('/create','Admin\NewsController@createTypes');
            Route::post('/update','Admin\NewsController@updateTypes');
            Route::get('/{id}','Admin\NewsController@editTypes');
            Route::get('/delete/{id}','Admin\NewsController@deleteTypes');
        });

        Route::get('/','Admin\NewsController@index');
        Route::post('/store','Admin\NewsController@store');
        Route::get('/create','Admin\NewsController@create');
        Route::post('/update','Admin\NewsController@update');
        Route::get('/month','Admin\NewsController@month');
        Route::get('/{id}','Admin\NewsController@edit');
        Route::get('/delete/{id}','Admin\NewsController@delete');
    });

    Route::group(['prefix' => 'partners'], function() {
        Route::get('/','Admin\PartnerController@index');
        Route::get('/create','Admin\PartnerController@create');
        Route::post('/store','Admin\PartnerController@store');
        Route::get('/{id}','Admin\PartnerController@edit');
        Route::post('/update','Admin\PartnerController@update');
        Route::get('/delete/{id}','Admin\PartnerController@delete');
    });

    Route::group(['prefix' => 'media'], function() {

        Route::group(['prefix' => 'types'], function() {
            Route::get('/','Admin\MediaController@types');
            Route::post('/store','Admin\MediaController@storeTypes');
            Route::get('/create','Admin\MediaController@createTypes');
            Route::post('/update','Admin\MediaController@updateTypes');
            Route::get('/{id}','Admin\MediaController@editTypes');
            Route::get('/delete/{id}','Admin\MediaController@deleteTypes');
        });

        Route::get('/','Admin\MediaController@index');
        Route::post('/store','Admin\MediaController@store');
        Route::get('/create','Admin\MediaController@create');
        Route::post('/update','Admin\MediaController@update');
        Route::get('/month','Admin\MediaController@month');
        Route::get('/{id}','Admin\MediaController@edit');
        Route::get('/delete/{id}','Admin\MediaController@delete');
    });

    Route::group(['prefix' => 'administrators'], function() {
        Route::get('/','Admin\AdminController@index');
        Route::post('/store','Admin\AdminController@store');
        Route::get('/create','Admin\AdminController@create');
        Route::post('/update','Admin\AdminController@update');
        Route::get('/{id}','Admin\AdminController@edit');
        Route::get('/delete/{id}','Admin\AdminController@delete');
    });

    Route::group(['prefix' => 'subscribers'], function() {
        Route::get('/','Admin\SubscriptionController@index');
        Route::get('/export','Admin\SubscriptionController@export');
        Route::get('/delete/{id}','Admin\SubscriptionController@delete');
    });

    Route::group(['prefix' => 'contacts'], function() {
        Route::get('/','Admin\ContactController@index');
        Route::get('/export','Admin\ContactController@export');
        Route::get('/view/{id}','Admin\ContactController@view');
        Route::get('/delete/{id}','Admin\ContactController@delete');
    });

    Route::group(['prefix' => 'settings'], function() {

        Route::group(['prefix' => 'home-items'], function() {
            Route::get('/','Admin\HomeItemController@index');
            Route::post('/store','Admin\HomeItemController@store');
            Route::get('/create','Admin\HomeItemController@create');
            Route::post('/update','Admin\HomeItemController@update');
            Route::get('/{id}','Admin\HomeItemController@edit');
            Route::get('/delete/{id}','Admin\HomeItemController@delete');
        });

    });

    Route::group(['prefix' => 'page-contents'], function() {
        Route::post('/update','Admin\PageContentController@update');
        Route::get('/{page}','Admin\PageContentController@edit');
    });

    Route::get('/{page}', function () {
        return view('404');
    });
});

Route::get('/ar/{page}', function ($page) {
    $lang = 'ar';
    if(view()->exists($page)){
        return view($page,compact('lang'));
    }
    return view('404');
});

Route::get('/','PageController@showPage');

Route::get('/{page}', function ($page) {
    $lang = 'en';
    if(view()->exists($page)){
        return view($page,compact('lang'));
    }
    return view('404');
});

