@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/') }}css/home.css?v=1.2">
    <link rel="stylesheet" href="{{ asset('/') }}css/swiper.css">
@endsection

@section('content')

    <section id="intro-img">
        <div id="player-overlay">
            <div class="swiper-container">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide video">
                        <video src="{{ asset('/') }}videos/Capital-Ave.mp4#t=0.5" autoplay="autoplay" loop="true" muted defaultmuted playsinline></video>
                    </div>
                    <div class="swiper-slide video">
                        <video src="{{ asset('/') }}videos/SkyadConstructiUpdateSep.mp4" autoplay="autoplay" loop="true" muted defaultmuted playsinline></video>
                    </div>
                    <div class="swiper-slide video">
                        <video src="{{ asset('/') }}videos/1-opt.mp4" autoplay="autoplay" loop="true" muted defaultmuted playsinline></video>
                    </div>
                    <div class="swiper-slide video">
                        <video src="{{ asset('/') }}videos/2-opt.mp4" autoplay="autoplay" loop="true" muted defaultmuted playsinline></video>
                    </div>
                    <div class="swiper-slide video">
                        <video src="{{ asset('/') }}videos/sky_ad_9.mp4" autoplay="autoplay" loop="true" muted defaultmuted playsinline></video>
                    </div>
                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination"></div>

                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>

                <!-- If we need scrollbar -->
                <div class="swiper-scrollbar"></div>
            </div>
        </div>
    </section>

    <section id="featured-projects">
        <div class="container padded">
            <p class="sub-heading" id="featured-title">{{  $lang=='ar' ? 'المشاريع المميزة' : 'FEATURED PROJECTS' }}</p>
            <h1 class="heading">{{  $lang=='ar' ? 'ارتقِ بمستوى عيشك. ' : 'Elevate everyday living.' }}</h1>
        </div>
        <div class="container conts">
        </div>
        <div class="container-fluid imgs">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12 top-left boximg" style="background-color: #CCBEB3;">
                            <div class="content">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8">

                                        <h2>
                                            @if($lang=='ar')
                                                جمال متقن. <br/>رفاهية متكاملة.<br/>مجتمع آمن.
                                            @else
                                                Intricate beauty.<br/>
                                                Handpicked luxury.<br/>
                                                Mindful community.
                                            @endif
                                        </h2>

                                            @if($lang=='ar')
                                                <p class="mb-3">
                                                    يقع Residence Eight في العاصمة الإدارية الجديدة، وتم إنشاءه بعناية ليمتد على مدى 23 فداناً. ويدعَّم هذا المشروع بأفخم الصيحات وأحدثها، وتحيط به المناظر الطبيعية الخضراء من كل صوب. ويتميز المشروع بموقعه المميز المواجه للنهر الأخضر لينعم السكان بالرفاهية والسعادة.
                                                </p>
                                                <p>
                                                    يتميز المشروع بموقعه الاستراتيجي المميز بالقرب من المدخل الغربي لحي R8 الدبلوماسي. ومن الشمال، يطل على النادي الرياضي الوحيد في المنطقة، ويتصل بمحور الشيخ محمد بن زايد الشمالي موفراً سهولة الوصول إلى الأحياء الحيوية في القاهرة.
                                                </p>
                                            @else
                                                <p class="mb-2">
                                                    Located in the New Administrative Capital, Residence Eight is mindfully masterplanned on 23 acres of land, surrounded by smart luxury and greenery. The development stretches along the Green River to surround you with non-stop recreation.
                                                </p>
                                                <p>
                                                    The project enjoys a strategic position on the West entrance of the R8 Diplomatic District, facing the only sports club in the area from the North with ultimate accessibility to all vital points in Cairo.
                                                </p>
                                            @endif

                                    </div>
                                    <div class="col-lg-4 col-md-4 text-xl-end text-lg-end text-md-end relative">
                                        <a href="{{ $lang == 'ar' ? url('/ar/projects') : url('/projects') }}" class="arrow-button">{{  $lang=='ar' ? 'اكتشف المزيد ' : 'Discover more' }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 bottom-left boximg" style="background-image: url('{{ asset('/') }}img/abstract/a1.png')">
                            <div class="imgslide animate" style="background-image: url('{{ asset('/') }}img/abstract/a5.png')"></div>
                            <div class="imgslide animate" style="background-image: url('{{ asset('/') }}img/abstract/a1.png')"></div>
                        </div>
                        <div class="col-md-6 col-sm-6 bottom-center boximg" style="background-image: url('{{ asset('/') }}img/abstract/b1.png')">
                            <div class="imgslide animate" style="background-image: url('{{ asset('/') }}img/abstract/b5.png')"></div>
                            <div class="imgslide animate" style="background-image: url('{{ asset('/') }}img/abstract/b1.png')"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 right boximg" style="background-image: url('{{ asset('/') }}img/abstract/c1.png')">
                    <div class="imgslide animate" style="background-image: url('{{ asset('/') }}img/abstract/c5.png')"></div>
                    <div class="imgslide animate" style="background-image: url('{{ asset('/') }}img/abstract/c1.png')"></div>
                </div>
            </div>
        </div>
    </section>

    <section id="featured-listing">
        <div class="container padded">
            <p class="sub-heading">{{  $lang=='ar' ? 'القوائم المميزة' : 'Featured listings' }}</p>
            <h1 class="heading">{{  $lang=='ar' ? 'اعثر على مساحتك الخاصة. ' : 'Find a place to call home.' }}</h1>
        </div>

        <div class="container-fluid gray-bg">
            <div class="container padded">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        @include('partials.searchbox')
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>

        <div class="container-fluid  gray-bg padded pt-3">
            <div class="row">
                <div id="scroll-items">
                    <div class="slide">
                        <img src="{{ asset('/') }}img/home-scroll/full/{{$lang=='ar'?'1s-ar.jpg':'1s.png'}}" width="100%;">
                    </div>
                    <div class="slide">
                        <img src="{{ asset('/') }}img/home-scroll/full/2s.png" width="100%;">
                    </div>
                    <div class="slide">
                        <img src="{{ asset('/') }}img/home-scroll/full/{{$lang=='ar'?'3s-ar.jpg':'3s.png'}}" width="100%;">
                    </div>
                    <div class="slide">
                        <img src="{{ asset('/') }}img/home-scroll/full/{{$lang=='ar'?'4s-ar.jpg':'4s.png'}}" width="100%;">
                    </div>
                    <div class="slide">
                        <img src="{{ asset('/') }}img/home-scroll/full/5s.png" width="100%;">
                    </div>
                </div>
                {{--<div id="scroll-items">--}}
                    {{--<div class="slide">--}}
                        {{--<div class="full item">--}}
                            {{--<img src="{{ asset('/') }}img/home-scroll/1.jpg" width="100%;">--}}
                        {{--</div>--}}
                        {{--<div class="half item">--}}
                            {{--<div class="text-box">--}}
                                {{--<span>Live the best Of tomorrow.</span>--}}
                            {{--</div>--}}
                            {{--<img src="{{ asset('/') }}img/home-scroll/2.jpg" width="100%;">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="slide">--}}
                        {{--<div class=" full item">--}}
                            {{--<img src="{{ asset('/') }}img/home-scroll/3.jpg" width="100%;">--}}
                        {{--</div>--}}
                        {{--<div class=" full item">--}}
                            {{--<img src="{{ asset('/') }}img/home-scroll/4.jpg" width="100%;">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="slide">--}}
                        {{--<div class=" half item">--}}
                            {{--<div class="text-box">--}}
                                {{--<span>An integrated vision of futuristic flow.</span>--}}
                            {{--</div>--}}
                            {{--<img src="{{ asset('/') }}img/home-scroll/5.jpg" width="100%;">--}}
                        {{--</div>--}}
                        {{--<div class=" full item">--}}
                            {{--<img src="{{ asset('/') }}img/home-scroll/6.jpg" width="100%;">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="slide">--}}
                        {{--<div class=" full item">--}}
                            {{--<img src="{{ asset('/') }}img/home-scroll/7.jpg" width="100%;">--}}
                        {{--</div>--}}
                        {{--<div class=" half item">--}}
                            {{--<div class="text-box">--}}
                                {{--<span>Reside where every moment counts.</span>--}}
                            {{--</div>--}}
                            {{--<img src="{{ asset('/') }}img/home-scroll/8.jpg" width="100%;">--}}
                        {{--</div>--}}
                        {{--<div class=" full item">--}}
                            {{--<img src="{{ asset('/') }}img/home-scroll/9.jpg" width="100%;">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-md-12">
                    <div class="arrows">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="news" class=" padded">
        <div class="container">
            <p class="sub-heading">{{  $lang=='ar' ? 'الأخبار ' : 'News' }}</p>
            <h1 class="heading">{{  $lang=='ar' ? 'آخر أخبار سكاي أبوظبي للتطوير العقاري. ' : 'The latest about SKY AD. Developments' }}</h1>
        </div>
        <div class="container articles">
            <div class="row">

                @inject('contentService', 'App\Services\ContentProvider')
                <?php $data = $contentService->getLatestNews(3,$lang); ?>

                <?php $count = 1 ?>
                @foreach($data as $article)
                    @if($count==1)
                        <div class="col-lg-6">
                            <div class="article">
                                <a href="{{ $lang == 'ar' ? url('ar/news-and-press/'.$article['slug']) : url('/news-and-press/'.$article['slug']) }}"><img src="{{ asset('/'.$article['photo_full']) }}" class="thumbnail mb-3"></a>
                                {{--<span class="type">News</span>--}}
                                <h2 class="title">{{ $lang=='en' ? $article['title'] : $article['title_ar'] }}</h2>
                                <p class="excerpt">{{\Illuminate\Support\Str::words(strip_tags($lang=='en' ? $article['content'] : $article['content_ar'] ),40)}}... <a href="{{ $lang == 'ar' ? url('ar/news-and-press/'.$article['slug']) : url('/news-and-press/'.$article['slug']) }}" class="readmoretxt">{{  $lang=='ar' ? '' : 'Read more' }}</a></p>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-3">
                            <div class="article">
                                <a href="{{ $lang == 'ar' ? url('ar/news-and-press/'.$article['slug']) : url('/news-and-press/'.$article['slug']) }}"><img src="{{ asset('/'.$article['photo']) }}" class="thumbnail mb-3"></a>
                                {{--<span class="type">News</span>--}}
                                <h2 class="title">{{$lang=='en' ? $article['title'] : $article['title_ar']}}</h2>
                                <p class="excerpt">{{\Illuminate\Support\Str::words(strip_tags($lang=='en' ? $article['content'] : $article['content_ar']),20)}}... <a href="{{ $lang == 'ar' ? url('ar/news-and-press/'.$article['slug']) : url('/news-and-press/'.$article['slug']) }}" class="readmoretxt">{{  $lang=='ar' ? '' : 'Read more' }}</a></p>
                            </div>
                        </div>
                    @endif
                    <?php $count++; ?>
                @endforeach
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a href="{{ $lang == 'ar' ? url('ar/news-and-press/') : url('/news-and-press/') }}" class="blue-bt mt-5">{{  $lang=='ar' ? 'اقرأ الكل ' : 'Read all' }}</a>
                </div>
            </div>
        </div>
    </section>

    @include('partials.newsletter')
    @include('partials.clients')

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js"></script>
    <script>
        var video = document.getElementById('video');
        var source = document.getElementById('source');

        function nextVideo() {
            video.pause();
            source.setAttribute('src', '{{ asset('videos/final-2.mp4') }}');
            video.load();
            video.play();
            $('#nextVideo').hide();
            $('#prevVideo').show();
        }

        $('#nextVideo').on('click',function(){ nextVideo() });

        function prevVideo() {
            video.pause();
            source.setAttribute('src', '{{ asset('videos/final-1.mp4') }}');
            video.load();
            video.play();
            $('#prevVideo').hide();
            $('#nextVideo').show();
        }

        $('#prevVideo').on('click',function(){ prevVideo() });

        $(window).on('resize',function(){
            alignContent();
        });

        $(window).on('load',function(){
            alignContent();

            // animateBoxes();
            setInterval(function(){
                animateBoxes();
            },5000);
        });

        function alignContent(){
            adjustmnt = $('#featured-title').offset().left;
            @if($lang=='ar')
                $('.top-left.boximg .content').css('right',adjustmnt);
            @else
                $('.top-left.boximg .content').css('left',adjustmnt);
            @endif
            $('.top-left.boximg .content').css('width',($('.top-left.boximg').width()-adjustmnt-40)+'px');

            // imgHeight = $('#scroll-items .item .text-box').closest('.item').find('img').height();
            // $('#scroll-items .item .text-box').css('height',imgHeight);
        }

        $(document).ready(function(){
            $('#scroll-items').slick({
                slidesToScroll: 1,
                slidesToShow: 1,
                variableWidth: true,
                infinite: true,
                appendArrows: '.arrows',
                autoplay: false,
                @if($lang=='ar')
                    rtl: true,
                @endif
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
                autoplaySpeed: 3000,
                speed: 1000,
                adaptiveHeight: true
            });
            $('#clients').slick({
                slidesToScroll: 1,
                slidesToShow: 1,
                infinite: true,
                dots: false,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 3000,
                speed: 1000,
                @if($lang=='ar')
                rtl: true,
                @endif
            });

            alignContent();
        });

        // When the window has finished loading create our google map below


        var animatingBoxes = false;

        function animateBoxes() {
            if(!animatingBoxes){
                animatingBoxes = true;

                $('.boximg.bottom-left .imgslide.animate').last().animate({
                    width: "0",
                }, 2000, function() {
                    if($('.boximg.bottom-left .imgslide.animate').length==1){
                        $('.boximg.bottom-left .imgslide').css('width','100%');
                        $('.boximg.bottom-left .imgslide').addClass('animate');
                    }
                    else
                        $('.boximg.bottom-left .imgslide.animate').last().removeClass('animate');
                });

                setTimeout(function(){
                    $('.boximg.bottom-center .imgslide.animate').last().animate({
                        height: "0",
                    }, 2000, function() {
                        if($('.boximg.bottom-center .imgslide.animate').length==1){
                            $('.boximg.bottom-center .imgslide').css('height','100%');
                            $('.boximg.bottom-center .imgslide').addClass('animate');
                        }
                        else
                            $('.boximg.bottom-center .imgslide.animate').last().removeClass('animate');
                    })
                },700);

                setTimeout(function(){
                    $('.boximg.right .imgslide.animate').last().animate({
                        height: "0",
                    }, 2000, function() {
                        if($('.boximg.right .imgslide.animate').length==1){
                            $('.boximg.right .imgslide').css('height','100%');
                            $('.boximg.right .imgslide').addClass('animate');
                        }
                        else
                            $('.boximg.right .imgslide.animate').last().removeClass('animate');

                        animatingBoxes = false;
                    });
                },300);
            }

        }

        $(function () {

            var mySwiper = new Swiper('.swiper-container', {
                // Optional parameters
                direction: 'horizontal',
                loop: false,
                autoHeight: true, //enable auto height

                // If we need pagination
                pagination: '.swiper-pagination',

                // Navigation arrows
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',

                // And if we need scrollbar
                // scrollbar: '.swiper-scrollbar',

                autoplay: false
            });


            var v = document.getElementsByTagName("video")[0];

            v.addEventListener("canplay", function () {
                mySwiper.stopAutoplay();
            }, true);

            v.addEventListener("ended", function () {
                mySwiper.startAutoplay();
            }, true);

        });
    </script>
@endsection
