@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/') }}css/inner.css?v=1.1">
    <style>
        .innercontent {
            margin-top: 110px;
        }

        #topdivide .left, #topdivide .right {
            background-color: #ebebeb;
            padding: 0;
            position: relative;
        }

        #topdivide .left .content {
            /*position: absolute;*/
            /*top: 60px;*/
            /*right: 30px;*/
            width: 100%;
            max-width: 600px;
            float: right;
            margin: 100px 0;
            padding-right: 30px;
        }

        #topdivide .left .content p {
            width: 85%;
            margin-right: 15%;
        }

        #topdivide .right {
            background-image: url('{{ asset('/') }}img/about/slide/main.jpg');
            background-repeat: no-repeat;
            background-position: right center;
            background-size: 100% auto;
        }

        #slider .slick-slide {
            position: relative;
        }

        #slider .slick-slide .slide-img {
            width: 100%;
        }

        #slider .slide-content {
            background-color: #000;
            color: #fff;
            padding: 30px 40px;
            position: absolute;
            bottom: 0;
            width: 100%;
            box-sizing: border-box;
        }

        #slider .slide-content h4 {
            font-size: 24px;
            font-family: 'Aeroport';
            margin-bottom: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }

        #tabs {
            overflow: hidden;
            position: relative;
        }

        .tab-nav {
            list-style: none;
            padding: 0;
            margin: 0;
            width: 100%;
        }

        .tab-nav .tab-link {
            background-color: #C8C8C8;
            color: #fff;
            float: left;
            text-align: center;
            padding: 70px 0;
            min-height: 240px;
            transition: background-color 300ms;
        }

        .tab-nav a {
            text-transform: uppercase;
            font-weight: bold;
            color: #fff;
            font-size: 36px;
        }

        .tab-nav .tab-link img {
            width: 60px;
            margin-bottom: 20px;
        }

        .tab-nav .tab-link.active {
            background-color: #B2E1D8;
        }

        .tab-box {
            background-color: #B2E1D8;
            padding: 40px;
            color: #fff;
            width: 100%;
            position: absolute;
            display: none;
        }

        .tab-box.active {
            display: block;
        }

        .tab-box h2 {
            max-width: 400px;
            margin-bottom: 0;
        }

        #team .member {
            position: relative;
            margin-bottom: 30px;
            text-align: left;
            cursor: pointer;
        }
        #team .member .wrap {
            position: relative;
            display: inline-block;
        }

        #team .member .wrap img {
            max-width: 300px;
        }

        #team .member .details {
            width: 100%;
            padding: 20px 0;
            bottom: 0;
            text-align: left;
        }

        #team .member img, .modal-content img {
            -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
            filter: grayscale(100%);
        }

        #team .member .title {
            font-size: 16px;
            line-height: 18px;
        }

        #team .member .details .name {
            color: #646464;
            margin-bottom: 5px;
            font-size: 16px;
        }

        .data-table {
            text-align: center;
            padding: 60px 0;
            background-color: #EBEBEB;
        }

        .data-table .data {
            display: inline-block;
            margin-right: 20px;
            padding-right: 23px;
            text-align: left;
            vertical-align: top;
            position: relative;
            text-align: center;
        }

        .data-table .data:nth-of-type(1) {
            width: 15%;
        }

        .data-table .data:nth-of-type(2) {
            width: 15%;
        }

        .data-table .data:nth-of-type(3) {
            width: 9%;
        }

        .data-table .data:nth-of-type(4) {
            width: 25%;
        }

        .data-table .data:nth-of-type(5) {
            width: 16%;
            margin-right: 0;
            padding-right: 0;
        }

        .data-table .value {
            color: #646464;
            font-weight: 900;
            font-size: 44px;
            margin-bottom: 0;
        }

        .data-table .value span {
            height: 50px;
            display: inline-block;
        }

        .data-table .sup {
            font-size: 30px;
            vertical-align: top;
            font-weight: 400;
            color: #B0B3AF;
            line-height: 53px;
            margin-left: 5px;
        }

        .data-table .copy {
            color: #B0B3AF;
            font-weight: 400;
            text-transform: uppercase;
            margin-bottom: 0;
        }

        .data-table .divider {
            width: 2px;
            height: 70px;
            position: absolute;
            top: 20px;
            right: 0;
            background-color: #B2E1D8;
            display: block;
        }

        .slide-content img {
            opacity: 0;
        }

        .right .arrows {
            position: absolute;
            bottom: 30px;
            right: 30px;
        }

        #project-slider {
            padding: 20px 10px;
        }

        #project-slider .conts {
            background-color: #000;
            padding: 30px 20px;
            min-height: 140px;
        }

        #project-slider h3 {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
        }

        #slider .slide  {
            text-align: right;
        }

        #slider .slide img {
            max-width: 540px;
            display: inline-block;
        }

        #project-slider .slick-slide {
            padding: 0 10px;
        }

        @media only screen and (max-width: 1400px) {
            #topdivide .left .content {
                max-width: 520px;
            }
            .tab-box h2 {
                max-width: 320px;
                font-size: 28px;
            }
            .data-table .value {
                font-size: 50px;
            }

            .data-table .copy {
                font-size: 16px;
                line-height: 19px;
            }
        }

        @media only screen and (max-width: 1200px) {
            #topdivide .left .content {
                max-width: 430px;
            }

            .tab-box h2 {
                max-width: 260px;
                font-size: 24px;
            }

            .tab-nav a {
                font-size: 34px;
            }

            .data-table .data {
                width: 46% !important;
                margin-right: 0;
                padding-right: 0;
                text-align: center;
                padding: 0 30px;
            }

            .data-table .data:nth-of-type(2) .divider {
                display: none;
            }

            .data-table .data:nth-of-type(4) .divider {
                display: none;
            }

            #topdivide .left .content {
                max-width: 470px;
            }
        }

        @media only screen and (max-width: 1080px) {
            #slider .slide-content {
                position: relative;
            }
            #slider {
                background-color: #000;
            }
            #topdivide .left .content {
                max-width: 90%;
            }

        }

        @media only screen and (max-width: 991px) {

            #topdivide .left .content {
                position: relative;
                max-width: 100%;
                right: auto;
                top: auto;
                padding: 30px;
            }

            #topdivide .left .content p {
                margin: 0;
                width: 100%;
            }

            .tab-box {
                position: relative;
            }

            .tab-box h2 {
                max-width: 100%;
                font-size: 20px;
            }

            .innercontent {
                min-height: 40vh;
            }

            .tab-nav a {
                font-size: 22px;
            }

            .tab-nav .tab-link img {
                width: 50px;
                margin-bottom: 10px;
            }

            .tab-nav .tab-link {
                padding: 20px 0;
                min-height: 130px;
            }
            #slider .slide img {
                max-width: 1000px;
            }

            #project-slider h3 {
                font-size: 15px;
                line-height: 22px;
            }

            #topdivide .left h1 {
                font-size: 32px;
            }
        }

        @media only screen and (max-width: 860px) {
            #project-slider .conts {
                min-height: 140px;
            }
        }

        @media only screen and (max-width: 767px) {


            .data-table .data {
                width: 100% !important;
                margin-bottom: 30px;
            }

            .data-table .divider {
                display: none;
            }

            #slider .slide-content h4 {
                position: relative;
                top: auto;
                -ms-transform: none;
                transform: none;
                width: 100%;
            }

            #slider .slide-content img {
                width: 40px;
                margin-top: 20px;
            }
        }

        @media only screen and (max-width: 460px) {

            .tab-nav a {
                font-size: 13px;
            }


            .tab-nav .tab-link img {
                width: 40px;
            }

            .tab-nav .tab-link {
                min-height: 110px;
            }
        }
    </style>
@endsection

@section('content')
    <section class="container-fluid innercontent">
        <div id="topdivide">
            <div class="row">
                <div class="col-lg-6 left">
                    <div class="content">
                        <h1 class="mb-3">{!!   $lang=='ar' ? 'مجتمع بأعلى معاير الرفاه' : 'SHAPING COMMUNITIES<br/> ACROSS THE REGION'  !!}</h1>
                        @if($lang=='ar')
                            <p>تعتبر شركة سكاي أبوظبي للتطوير العقاري من أحد الشركات الرائدة في المنطقة وهي تابعة لمجموعة دايموند التي تعد مطوراً معروفاً للشركات التجارية والصناعية في دولة الإمارات العربية المتحدة. </p>
                            <p>تتميز مشاريع مجوعة دايموند التنموية متعددة الاستخدامات بنماذجٍ مختلفة من الوحدات السكنية والمرافق الترفيهية داخل مجتمعات متكاملة، كما تحتوي على المدارس والمجمعات التجارية والمرافق الممتدة من أبوظبي إلى العين. </p>
                            <p>شرعت سكاي أبوظبي بعمليات تطوير في مصر من خلال طرحها لمشروعها الأول الذي يقع في أحد أكثر المواقع الواعدة في العاصمة الجديدة لعام 2021، ويوفر المشروع شققاً فاخرة مصحوبة بأفضل سبل الراحة والرفاهية. </p>
                            <p>تتمثل رؤيتنا في إنشاء مجتمعات متكاملة في مصر لخدمة عملائنا وتوفير حياة الرفاهية والاستدامة لسكاننا.</p>

@else

                            <p>
                                SKY AD. Developments is a leading regional real estate developer, under the direction of the Diamond Group, a renowned developer of local trading and industrial firms with a strong portfolio in the UAE.
                            </p>
                            <p>
                                Diamond group portfolio features different models of residential units and leisure facilities that include schools, business parks, clubs, hospitals, residential and commercial projects that stretch from Abu Dhabi to Al Ain.
                            </p>
                            <p>SKY AD. Developments launched operations in Egypt with its first project located in one of the most promising locations in the New Capital in 2021. The project offers apartments, luxury duplexes along with a wide range of amenities.
                            </p>
@endif
                    </div>
                </div>
                <div class="col-lg-6 right" style=" ">
                    {{--<div id="slider">--}}
                        {{--<div class="slide">--}}
                            {{--<img src="{{ asset('/') }}img/about/slide/5s.jpg" class="slide-img">--}}
                            {{--<div class="slide-content">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-8 col-sm-10 relative">--}}
                                        {{--<h4>Abu Dhabi-reem island offices--}}
                                            {{--& commercial buildings</h4>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 col-sm-2 text-right">--}}
                                        {{--<img src="{{asset('img/arrow-bt-white.png')}}" width="60" class="pull-right">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="arrows">--}}
                        {{--<img src="{{asset('img/arrow-bt-white.png')}}" width="60" class="pull-right">--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </section>

    <div id="project-slider" class="center-mobile">
        <div class="slide">
            <img src="{{ asset('img/about/projects/najmat.png') }}" width="100%">
            <div class="conts">
                <h3>{{  $lang=='ar' ? 'برج نجمة أبوظبي السكني' : 'Najmat Abu Dhabi (residential)' }}</h3>
            </div>
        </div>
        <div class="slide">
            <img src="{{ asset('img/about/projects/ayia.png') }}" width="100%">
            <div class="conts">
                <h3>{{  $lang=='ar' ? 'مجمع أيا نابا في أبوظبي (سكني)' : 'AYIA NAPA Compound Abu Dhabi (residential)' }}</h3>
            </div>
        </div>
        <div class="slide">
            <img src="{{ asset('img/about/projects/mushrif.png') }}" width="100%">
            <div class="conts">
                <h3>{{  $lang=='ar' ? 'نادي المشرف (تجاري) ' : 'Al-Mushrif Club (Commercial)' }}</h3>
            </div>
        </div>
        <div class="slide">
            <img src="{{ asset('img/about/projects/ladies.png') }}" width="100%">
            <div class="conts">
                <h3>{{  $lang=='ar' ? 'نادي أبوظبي للسيدات (تجاري)' : 'Abu Dhabi Ladies Club (Commercial)' }}</h3>
            </div>
        </div>
        <div class="slide">
            <img src="{{ asset('img/about/projects/reem.png') }}" width="100%">
            <div class="conts">
                <h3>{{  $lang=='ar' ? 'المكاتب والأبراج التجارية في جزيرة الريم في أبوظبي' : 'Reem Island Offices Abu Dhabi &amp; Commercial Buildings' }}</h3>
            </div>
        </div>
        <div class="slide">
            <img src="{{ asset('img/about/projects/danet.png') }}" width="100%">
            <div class="conts">
                <h3>{{  $lang=='ar' ? 'برج دانيت السكني في أبوظبي' : 'Danet Abu Dhabi Residential Tower' }}</h3>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="tab-nav">
            <div class="row">
                <div class="col-4 tab-link active"><a href="#"><img src="{{asset('img/about/mission.png')}}"><br/>{{  $lang=='ar' ? 'رسالتنا ' : 'Our mission' }}</a></div>
                <div class="col-4 tab-link"><a href="#"><img src="{{asset('img/about/vision.png')}}"><br/>{{  $lang=='ar' ? 'رؤيتنا' : 'Our vision' }}</a></div>
                <div class="col-4 tab-link"><a href="#"><img src="{{asset('img/about/values.png')}}"><br/>{{  $lang=='ar' ? 'قيمنا ' : 'Our values' }}</a></div>
            </div>
        </div>

        <div class="container">
            <div class="row padded">
                <div class="col-lg-12">
                    <div class="tab-content active">
                        <div class="row pt-2 pb-2">
                            <div class="col-md-12">
                                @if($lang=='ar')
                                    <p>تتمثل رسالتنا في تلبية ما يفوق توقعات العملاء ورفع مستوى المعيشة المتكاملة في مصر وزيادة رضا السكان عن منازلهم. ويمكن تحقيق ذلك من خلال تنفيذ العمليات التنموية وتسليم مشاريعنا المبتكرة في الوقت المناسب وبتوفير مزيج خاص بين الحياة العصرية والهدوء لتمكين السكان من العيش والعمل واللعب براحة.  </p>
                                @else
                                    <p>Our mission is to exceed customers’ expectations and raise the benchmark for integrated living in Egypt and provide the end-user with added value beyond their homes. This is achievable through developments, timely delivery of our innovative projects, a distinctive product-mix and thriving communities where people can live, work and play.</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="row pt-2 pb-2">
                            <div class="col-md-12">
                                @if($lang=='ar')
                                    <p>تطوير سوق العقارات المصري من خلال تطوير مشاريع مميزة تلبي احتياجات السكان المتنوعة بما يتوافق مع المعايير الدولية. حرصت الشركة على إحراز المزيد من التقدم والنجاح إلى أن أصبحت مطوراً محلياً معروفاً في سوق العقارات، كما أنها تركز على تحسين خلفيتها الإقليمية لتطوير مشاريع مستقبلية في مصر.</p>
                                @else
                                    <p>To evolve the Egyptian real estate market through developing unique projects that fulfil diverse consumer needs according to international standards. The company has evolved to become a solid regional developer within the real estate market and is building on its strong regional background and legacy to develop futuristic projects in Egypt.</p>

                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="row pt-2 pb-2">
                            <div class="col-md-4">
                                <h5>{{  $lang=='ar' ? '	الاحترام' : 'Respect' }}</h5>
                                <p>{{  $lang=='ar' ? 'نؤمن بأهمية سيادة الاحترام في جميع تعاملاتنا مع موظفينا والأطراف الخارجية بغض النظر عن العمر والعرق والخلفية. ' : 'We believe that all employees and third parties should be treated with the same level of respect regardless of their age, position and background.' }}</p>
                            </div>
                            <div class="col-md-4">
                                <h5>{{  $lang=='ar' ? 'الجودة' : 'Quality' }}</h5>
                                <p>{{  $lang=='ar' ? 'نتميز بجودتنا التي لا مثيل لها، ونلبي ما يفوق توقعات العملاء عبر مبانينا وتصاميمنا وتأثيثنا وإطلالاتنا والمزيد.' : 'Our exceptional quality is what makes us stand out and exceed clients’ expectations, across construction, design, furnishing, landscaping, and more.' }}</p>
                            </div>
                            <div class="col-md-4">
                                <h5>{{  $lang=='ar' ? '	الاحتراف' : 'Professionalism' }}</h5>
                                <p>{{  $lang=='ar' ? 'نمهد طريقنا إلى النجاح بوفائنا بوعودنا واحترامنا لمواعيدنا وحفاظنا على جودتنا وخدماتنا.' : 'The route to success is paved by our ability to always keep our promises and live up to them whether it is delivery dates, quality, or any additional services.' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="col-lg-4">--}}
                {{--<div class="tab-box active">--}}
                {{--<h2>Exceed customers’ expectations and raise the benchmark for integrated living.</h2>--}}
                {{--</div>--}}
                {{--<div class="tab-box">--}}
                {{--<h2>fulfil diverse consumer needs according to international standards.</h2>--}}
                {{--</div>--}}
                {{--<div class="tab-box">--}}
                {{--<h2>The route to success is paved by our ability to always keep our promises.</h2>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>


    <div class="data-table">
        <div class="data">
            <h4 class="value"><span>20</span><span class="sup">+</span></h4>
            <p class="copy">{{  $lang=='ar' ? 'عاماً من الخبرة حول العالم  ' : 'Years of international experience.' }} </p>
            <span class="divider"></span>
        </div>
        <div class="data">
            <h4 class="value"><span>1,500</span><span class="sup">+</span></h4>
            <p class="copy">{{  $lang=='ar' ? 'وحدة سكنية ' : 'residential units delivered.' }}</p>
            <span class="divider"></span>
        </div>
        <div class="data">
            <h4 class="value"><span>7</span></h4>
            <p class="copy">{{  $lang=='ar' ? 'مشاريع تجارية ' : 'commercial projects.' }}</p>
            <span class="divider"></span>
        </div>
        <div class="data">
            <h4 class="value"><span>180,000</span><span class="sup">+{{$lang == 'ar' ? '' : 'SQM' }}</span></h4>
            <p class="copy">{!!   $lang=='ar' ? 'متر مربع يشمل على الخدمات التعليمية والصحية والترفيهية' : 'of corporate, educational,<br/>healthcare and leisure space.'  !!}</p>
            <span class="divider"></span>
        </div>
        <div class="data">
            <h4 class="value"><span>$1{{$lang == 'ar' ? '' : 'B' }}</span><span class="sup">+</span></h4>
            <p class="copy">{{  $lang=='ar' ? 'مليار دولار أمريكي قيمة المحفظة الاستثمارية في جميع أنحاء العالم' : 'investment portfolio worldwide.' }}</p>
        </div>
    </div>

    <div class="padded" id="team">
        <div class="container center-mobile">
            <div class="row">
                <p class="sub-heading mt-3">{{  $lang=='ar' ? '' : 'About us' }}</p>
                <h1 class="heading mb-5">{{  $lang=='ar' ? 'تعرفوا على فريقنا' : 'Meet our team' }}</h1>
            </div>
        </div>
        <div class="container">
            <div class="row">

                <div class="col-lg-4 member">
                    <div class="wrap" data-bs-toggle="modal" data-bs-target="#saleh">
                        <img src="{{ asset('img/about/saleh.jpg') }}" width="100%">
                        <div class="details fade-in">
                            <h5 class="name">{{  $lang=='ar' ? 'سعادة صالح محمد بن نصرة العامري ' : 'H.E Saleh Mohammed bin Nasra Al Ameri' }}</h5>
                            <p class="title">{{  $lang=='ar' ? 'رئيس مجلس إدارة مجموعة دايموند وشركة سكاي أبوظبي للتطوير العقاري' : 'Chairman of Diamond Group and SKY AD. Developments' }}</p>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="saleh" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="{{ asset('img/about/saleh.jpg') }}" width="250">
                                <br/>
                                <br/>
                                <h5 class="name">{{  $lang=='ar' ? 'سعادة صالح محمد بن نصرة العامري ' : 'H.E Saleh Mohammed bin Nasra Al Ameri' }}</h5>
                                <p class="title">{{  $lang=='ar' ? 'رئيس مجلس إدارة مجموعة دايموند وشركة سكاي أبوظبي للتطوير العقاري' : 'Chairman of Diamond Group and SKY AD. Developments' }}</p>
                                @if($lang=='ar')
                                    <p>سعادة صالح محمد بن نصرة العامري، صاحب شركة دايموند، والاقتصادي المعروف، ورئيس مجلس إدارة عدد من الشركات العقارية، الشرفة، إشراق، وجولدن جروب. شغل سعادته العديد من المناصب الدبلوماسية ومن أبرزها السفير السابق لدولة الإمارات العربية المتحدة في قطر، ومؤسس سفارة الدولة في السويد عام 2004. كما شغل منصب قنصل الإمارات في لندن عام 1998، وقبلها عمل بممثلية الإمارات بجنيف.</p>
                                    <p>ولد سعادة صالح محمد بن نصرة العامري في مدينة العين عام 1968، وبدأ مسيرته المهنية في سن مبكر حيث كان يبلغ عمره 14 عاماً، وكان يعمل في إدارة أملاك والده. نشأ سعادته في بيئة علمته الصبر والمثابرة واتخاذ القرار المناسب في الوقت المناسب. وفي أوائل التسعينات، عمل صالح بن محمد بن نصرة العامري في العديد من المجالات والشركات مثل شركات النقليات والمقاولات والشبكات، بالإضافة إلى العمل على العقارات التي ورثها عن والده والتي شهدت نجاحاً ونمواً تحت إدارته.</p>
                                    <p>بفضل خبراته الطويلة والواسعة ومهاراته الإدارية الاستثنائية، نجح سعادته في تحويل "دايموند" إلى مجموعة رائدة في مجال العقارات بإدارتها العديد من المشاريع والأنشطة، بإجمالي استثمارات بلغ 1 مليار دولار، وبالإشراف على أكثر من 17 مشروعاً في الإمارات العربية المتحدة.  </p>
                                @else
                                    <p>H.E Saleh Mohammed bin Nasra Al Ameri is the owner of Diamond Group and a well-known economist. He is the Chairman of several real estate companies including Al Shorfa, Ishraq, and Golden Group. Saleh held many diplomatic positions, most notably the former ambassador of the United Arab Emirates to Qatar and the founder of the country's embassy in Sweden in 2004. He also served as the UAE consul in London in 1998, and a representative of the UAE in Geneva.</p>
                                    <p>
                                        H.E Saleh Mohammed bin Nasra Al Ameri was born in the city of Al-Ain in 1968, and he began his career journey at a young age, when he was only 14 years old, managing his father's property. Saleh was raised in an environment that taught in him patience, tenacity, and the ability to make the right decision in the right time. He worked in transportation, contracting, and networks companies in the early 1990s in addition to managing the real estate assets he inherited from his father which saw unprecedented growth under his management.</p>
                                    <p>
                                        Owing to his long, extensive experience and his exceptional management skills, he succeeded in turning "Diamond" into a leading group in the field of real estate with a variety of projects and activities, with a total investment worth $ 1bn and a portfolio of more than 17 projects in the UAE.
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 member">
                    <div class="wrap" data-bs-toggle="modal" data-bs-target="#abdi">
                        <img src="{{ asset('img/about/abdi.png') }}" width="100%">
                        <div class="details fade-in">
                            <h5 class="name">{{  $lang=='ar' ? 'المهندس عبدالرحمن عجمي' : 'Eng. Abdel Rahman Agami' }}</h5>
                            <p class="title">{{  $lang=='ar' ? 'الرئيس التنفيذي لمجموعة دايموند وشركة سكاي أبوظبي للتطوير العقاري' : 'CEO of Diamond Group and SKY AD. Developments' }}</p>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="abdi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="{{ asset('img/about/abdi.png') }}" width="250">
                                <br/>
                                <br/>
                                <h5 class="name">{{  $lang=='ar' ? 'المهندس عبدالرحمن عجمي' : 'Eng. Abdel Rahman Agami' }}</h5>
                                <p class="title">{{  $lang=='ar' ? 'الرئيس التنفيذي لمجموعة دايموند وشركة سكاي أبوظبي للتطوير العقاري' : 'CEO of Diamond Group and SKY AD. Developments' }}</p>
                                @if($lang=='ar')
                                    <p>المهندس عبدالرحمن عجمي هو الرئيس التنفيذي لمجموعة دايموند وشركة سكاي أبوظبي للتطوير العقاري. بالاعتماد على خبرته الواسعة في التخطيط وإدارة الأعمال، يشرف عبد الرحمن عجمي على إدارة الشركة، بما في ذلك تطوير الأعمال والتخطيط الاستراتيجي. قبل انضمامه إلى سكاي أبوظبي للتطوير العقاري، شغل المهندس عبد الرحمن منصب المدير العام لشركة NGCC في العين في الإمارات العربية المتحدة.</p>
                                    <p>مهندس معتمد ومتمرس بخبرة تزيد عن 25 عاماً، بدأت مسيرة المهندس عبد الرحمن المهنية في عام 1994 كخبير هندسي بوزارة العدل. وفي يناير 2012، أصبح الرئيس التنفيذي لمجموعة دايموند، ويشغل حاليًا منصب الرئيس التنفيذي لمجموعة دايموند وسكاي أبوظبي للتطوير العقاري بإجمالي استثمارات يصل إلى 950 مليون دولار خلال 8 سنوات.</p>
                                    <p>تخرج عبد الرحمن عجمي من كلية الهندسة بجامعة أسيوط في مصر عام 1996، وحصل على درجة الماجستير في إدارة الأعمال من كلية إدارة الأعمال بالجامعة الأمريكية في الشارقة.</p>
                                @else
                                    <p>Eng. Abdel Rahman Agami is the CEO of Diamond Group and SKY AD. Developments. Drawing on his extensive planning and business management experience, Eng. Abdel Rahman Agami supervises the day‐to‐day management of the company, including the business development and the strategic planning. Prior to joining SKY AD. Developments, Eng. Abdel Rahman was the General Manager of NGCC in Al Ain UAE. </p>
                                    <p>An established engineer and expert with more than 25 years of expertise, Eng. Abdel Rahman’s professional career began in 1994, as an engineering expert at Ministry of Justice. In January 2012, he became the CEO of Diamond Group, and currently He is the CEO of Diamond Group and Sky AD. Developments with a total investment of 950 million dollars within 8 years.
                                    </p>
                                    <p>Abdel Rahman Agami graduated from Assuit University, Faculty of Engineering in 1996 in Egypt and holds an MBA from AUS (American University of Sharjah) School of Business.</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 member">
                    <div class="wrap" data-bs-toggle="modal" data-bs-target="#salam">
                        <img src="{{ asset('img/about/mostafa.jpg') }}" width="100%">
                        <div class="details fade-in">
                            <h5 class="name">{{  $lang=='ar' ? 'الأستاذ مصطفى صلاح' : 'Mostafa Salah' }}</h5>
                            <p class="title">{{  $lang=='ar' ? 'الرئيس التنفيذي للقطاع التجاري لشركة سكاي أبوظبي للتطوير العقاري' : 'CCO of SKY AD. Developments' }}</p>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="salam" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="{{ asset('img/about/mostafa.jpg') }}" width="250">
                                <br/>
                                <br/>
                                <h5 class="name">{{  $lang=='ar' ? 'الأستاذ مصطفى صلاح' : 'Mostafa Salah' }}</h5>
                                <p class="title">{{  $lang=='ar' ? 'الرئيس التنفيذي للقطاع التجاري لشركة سكاي أبوظبي للتطوير العقاري' : 'CCO of SKY AD. Developments' }}</p>


                                @if($lang=='ar')
                                    <p>الأستاذ مصطفى صلاح هو الرئيس التنفيذي للقطاع التجاري لشركة سكاي أبوظبي للتطوير العقاري. </p>
                                    <p>في عام 2019، وقبل شغله لمنصبه الحالي، شغل السيد صلاح منصب الرئيس التجاري لشركة إمكان مصر. </p>
                                    <p>إن خبرة الأستاذ مصطفى البالغة 21 عاماً في التسويق والمبيعات مبنية على التطوير وبناء الذات المستمر، فطوال حياته المهنية، واصل الأستاذ صلاح حضور الدورات التعليمية وسعى للحصول على شهادات في المبيعات والتسويق من فرنسا ولبنان وإيطاليا والمملكة المتحدة ودبي وإسبانيا.</p>
                                    <p>تخرج عام 1999 من قسم اللغة الإنجليزية بكلية التجارة في جامعة عين شمس، وبدأ السيد صلاح حياته المهنية في المبيعات والتسويق في MTI Automotive. عمل في MTI Automotive لأكثر من 10 سنوات، وخلال هذه المدة، عمل على وضع الوظيفة التجارية للعلامات التجارية الفاخرة التابعة للشركة، وعمل على استراتيجيات التسويق والمبيعات، ووضع خطط تسويق ناجحة لتوعية الناس بالعلامة التجارية، وكل جهوده ساعدت في زيادة المبيعات أكثر فأكثر عاماً بعد عام. وفي حين عمله في MTI Automotive، حصل الأستاذ صلاح على درجة الماجستير في إدارة الأعمال والتسويق من الأكاديمية العربية للعلوم والتكنولوجيا والنقل البحري، بالإضافة إلى درجة الماجستير في المبيعات من أكاديمية جاكوار في المملكة المتحدة.</p>
                                    <p>في بداية عام 2010، انضم السيد صلاح إلى شركة سوديك للتطوير العقاري وفي غضون أربع سنوات، تم تعيينه مديرًا للمبيعات، حيث حقق أكثر من 3 مليارات جنيه مصري من المبيعات.</p>
                                    <p>وفي عام 2014، انضم السيد صلاح إلى شركة الديار القطرية للاستثمار العقاري كرئيس للمبيعات، وحققت الشركة مبيعات قياسية بلغت 2.7 مليار جنيه مصري في غضون 6 أشهر.</p>
                                    <p>وفي عام 2017 ، عُيِّن السيد صلاح في منصب الرئيس التجاري لشركة IWAN للتطوير العقاري.</p>
                                @else
                                    <p>Mostafa Salah is the CCO of SKY AD. Developments.</p>
                                    <p>
                                        In 2019 prior to this position, Mr. Salah was the Chief Commercial Officer of IMKAN MISR </p>
                                    <p>Mr. Mostafa’s 21‐year experience in Marketing and Sales is built on continuous development. Throughout his career, Mr. Salah has continued to attend courses and attain certificates in Sales and Marketing from France, Lebanon, Italy, United Kingdom, Dubai and Spain.
                                    </p>
                                    <p>A 1999 graduate of the Faculty of Commerce English Section, Ain Shams University, Mr. Salah began his career in sales and marketing at MTI Automotive. Over 10 years at MTI Automotive, during this period, he started up the commercial function for MTI Automotive luxury brands, created the ¬Marketing and Sales Strategy, built successful brand awareness and marketing strategies and plans which aided in growing the sales and achieving higher sales targets year over year. Simultaneously with working at MTI Automotive, Mr. Salah attained his MBA in marketing from the Arab Academy for Science, Technology and Maritime Transport, Sales Master from Jaguar Academy United Kingdom.
                                    </p>
                                    <p>At the beginning of 2010, Mr. Salah joined SODIC Real Estate. Within four years, he was named the Sales Director, where he achieved more than EGP 3 billion worth of sales.
                                    </p>
                                    <p>In 2014, Mr. Salah joined Qatari Diar Real Estate Investment Co. as Head of Sales. The company has achieved record‐high sales of EGP 2.7 billion within 6 months.
                                    </p>
                                    <p>In 2017, Mr. Salah was appointed as the Chief Commercial Officer of IWAN Developments. </p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        $(document).ready(function(){
            slider = $('#slider').slick({
                slidesToScroll: 1,
                slidesToShow: 1,
                infinite: false,
                arrows: false,
                autoplay: true,
                @if($lang=='ar')
                rtl: true,
                @endif
                adaptiveHeight: true,
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
                autoplaySpeed: 2000,
            });
            slider = $('#project-slider').slick({
                slidesToScroll: 1,
                slidesToShow: 4,
                infinite: true,
                arrows: false,
                autoplay: true,
                @if($lang=='ar')
                rtl: true,
                @endif
                adaptiveHeight: true,
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
                autoplaySpeed: 2000,
                responsive: [
                {
                    breakpoint: 1400,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                ]
            });

            $('.right .arrows').on('click', function () {
                $('#slider').slick('slickNext');
            });
        });
    </script>
@endsection
