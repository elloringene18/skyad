@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/') }}css/inner.css?v=1.1">
    <link rel="stylesheet" href="{{ asset('/') }}css/properties.css?v=1.1">

    <style>
        .this {
            opacity: 0;
            position: absolute;
            top: 0;
            left: 0;
            height: 0;
            width: 0;
            z-index: -1;
        }
    </style>
@endsection

@section('content')

    <div class=" innercontent">
        <section class="container">
            <div class="row">
                <div class="container padded center-mobile">
                    <h1 class="heading">{{  $lang=='ar' ? 'اعثر على ما تريد. ' : 'Find exactly what you’re looking for.' }}</h1>
                    <p class="sub-heading">{{  $lang=='ar' ? 'ابحث عن العقارات المتاحة. ' : 'Search available properties.' }}</p>
                </div>
            </div>
            <div class="row">
            </div>
        </section>

        <div class="container-fluid gray-bg">
            <div class="container padded">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        @include('partials.searchbox')
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>

        <section class="container-fluid gray-bg">
            <div class="container padded">
                <div class="properties">
                    <div class="tab-content  {{ isset($_GET['category']) ? ($_GET['category']=='residential' ? 'active' : '') : 'active' }} fade-in">
                        <div class="row">
                            @forelse($products as $product)
                                <div class="col-lg-3 col-md-4">
                                    <div class="property clearfix">
                                        <div class="image">
                                            <a href="{{ $lang == 'ar' ? url('/ar/property-listings/'.$product->slug) : url('property-listings/'.$product->slug) }}"><img src="{{ $product->photoUrl }}" class="thumbnail" width="100%"></a>
                                            {{--<span class="photos">4</span>--}}
                                        </div>
                                        <div class="copy">
                                            <h2 class="title">{{ $lang=='ar' ? strip_tags($product->title_ar) : strip_tags($product->title) }}</h2>
                                            <p class="size">{{  $lang=='ar' ? 'بدءاً من ' : 'Starting' }} {{ $product->area }}m<sup>2</sup></p>
                                            {{--<p class="price">from AED {{ $product->price }}</p>--}}
                                            {{--<p class="excerpt">{{ $product->description }}</p>--}}
                                            <a href="{{ $lang == 'ar' ? url('/ar/property-listings/'.$product->slug) : url('property-listings/'.$product->slug) }}" class="readmore">{{  $lang=='ar' ? 'اقرأ المزيد ' : 'View more' }}</a>
                                            {{--<i class="fa fa-share-alt"></i>--}}
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <div class="col-lg-12">
                                    <h5>{{  $lang=='ar' ? 'No results for this query.' : 'No results for this query.' }}</h5>
                                </div>
                            @endforelse

                            <div class="col-lg-12">
                                <div class="paginations mt-3">
                                    {!! $products->appends(request()->input())->links("pagination::bootstrap-4") !!}
                                </div>
                            </div>

                        </div>
                    </div>

                    {{-- <div class="tab-content fade-in active"> --}}
                    <div class="tab-content fade-in {{ isset($_GET['category']) ? ($_GET['category']=='commercial' ? 'active' : '') : '' }}">
                    {{--<div class="tab-content fade-in active">--}}
                        {{-- <div class="tab-content fade-in active center-mobile"> --}}
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="heading mb-5">{{  $lang=='ar' ? 'قريباً' : 'Coming soon' }}</h1>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="tab-content fade-in center-mobile">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="heading mb-5">Coming soon</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </section>


        <section class="container">
            <div class="row">
                <div class="container padded pt-5 center-mobile">
                    <p class="sub-heading mt-3">{{  $lang=='ar' ? 'نموذج التسجيل  ' : 'Register interest form' }}</p>
                    <h1 class="heading mb-5">{{  $lang=='ar' ? 'هل تريد المساعدة؟ تواصل معنا.' : 'Need help? Talk to us.' }}</h1>
                        <form class="flat-form" action="{{url('contact')}}" method="post" autocomplete="off">
                            @csrf
                            <input type="hidden" value="propert-listings" name="type">
                            <div class="row">
                                <div class="col-md-12">
                                    <input name="user_id" type="text" id="firstname" class="this" autocomplete="off" tabindex="-1">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                    @endif
                                    @if(Session::has('error'))
                                        <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                    @endif
                                    @csrf

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label>{{  $lang=='ar' ? '	الاسم الكامل' : 'Full Name' }}*</label>
                                        <input type="text" placeholder="{{ $lang == 'ar' ? 'الاسم' : 'Add your name' }}" name="name" class="form-control" required autocomplete="off">
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5">
                                        <label>{{  $lang=='ar' ? 'البريد الإلكتروني' : 'Email' }}*</label>
                                        <input type="text" placeholder="{{ $lang == 'ar' ? 'البريد الإلكتروني' : 'add your email' }}" name="email" class="form-control" required autocomplete="off">
                                    </div>
                                    <div class="col-md-5">
                                        <label>{{  $lang=='ar' ? 'رقم التواصل' : 'Contact Number' }}*</label>
                                        <input type="text" placeholder="{{ $lang == 'ar' ? 'رقم التواصل' : 'add you contact number' }}" name="phone" class="form-control mobilenumber" required  value="" autocomplete="off">
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5">
                                        <label>{{  $lang=='ar' ? '	الدولة' : 'Country' }}*</label>
                                        <div class="custom-select">
                                            <select class="form-control" name="country">

                                                <?php
                                                $countries = array(
                                                    0 => array('Afghanistan', 'أفغانستان'),
                                                    1 => array('Albania', 'ألبانيا'),
                                                    2 => array('Algeria', 'الجزائر'),
                                                    3 => array('American Samoa', 'ساموا الأمريكية'),
                                                    4 => array('Andorra', 'أندورا'),
                                                    5 => array('Angola', 'أنغولا'),
                                                    6 => array('Anguilla', 'أنغيلا'),
                                                    7 => array('Antigua &amp; Barbuda', 'أنتيغوا وأمبير. باربودا'),
                                                    8 => array('Argentina', 'الأرجنتين'),
                                                    9 => array('Armenia', 'أرمينيا'),
                                                    10 => array('Aruba', 'أروبا'),
                                                    11 => array('Australia', 'أستراليا'),
                                                    12 => array('Austria', 'النمسا'),
                                                    13 => array('Azerbaijan', 'أذربيجان'),
                                                    14 => array('Bahamas', 'جزر البهاما'),
                                                    15 => array('Bahrain', 'البحرين'),
                                                    16 => array('Bangladesh', 'بنغلاديش'),
                                                    17 => array('Barbados', 'بربادوس'),
                                                    18 => array('Belarus', 'بيلاروسيا'),
                                                    19 => array('Belgium', 'بلجيكا'),
                                                    20 => array('Belize', 'بليز'),
                                                    21 => array('Benin', 'بنين'),
                                                    22 => array('Bermuda', 'برمودا'),
                                                    23 => array('Bhutan', 'بوتان'),
                                                    24 => array('Bolivia', 'بوليفيا'),
                                                    25 => array('Bonaire', 'بونير'),
                                                    26 => array('Bosnia & Herzegovina', 'البوسنة والهرسك'),
                                                    27 => array('Botswana', 'بوتسوانا'),
                                                    28 => array('Brazil', 'البرازيل'),
                                                    29 => array('British Indian Ocean Ter', 'إقليم المحيط البريطاني الهندي'),
                                                    30 => array('Brunei', 'بروناي'),
                                                    31 => array('Bulgaria', 'بلغاريا'),
                                                    32 => array('Burkina Faso', 'بوركينا فاسو'),
                                                    33 => array('Burundi', 'بوروندي'),
                                                    34 => array('Cambodia', 'كمبوديا'),
                                                    35 => array('Cameroon', 'الكاميرون'),
                                                    36 => array('Canada', 'كندا'),
                                                    37 => array('Canary Islands', 'جزر الكناري'),
                                                    38 => array('Cape Verde', 'الرأس الأخضر'),
                                                    39 => array('Cayman Islands', 'جزر كايمان'),
                                                    40 => array('Central African Republic', 'جمهورية افريقيا الوسطى'),
                                                    41 => array('Chad', 'تشاد'),
                                                    42 => array('Channel Islands', 'جزر القناة'),
                                                    43 => array('Chile', 'تشيلي'),
                                                    44 => array('China', 'الصين'),
                                                    45 => array('Christmas Island', 'جزيرة كريسماس'),
                                                    46 => array('Cocos Island', 'جزيرة كوكوس'),
                                                    47 => array('Colombia', 'كولومبيا'),
                                                    48 => array('Comoros', 'جزر القمر'),
                                                    49 => array('Congo', 'الكونغو'),
                                                    50 => array('Cook Islands', 'جزر كوك'),
                                                    51 => array('Costa Rica', 'كوستا ريكا'),
                                                    52 => array('Cote DIvoire', 'ساحل العاج'),
                                                    53 => array('Croatia', 'كرواتيا'),
                                                    54 => array('Cuba', 'كوبا'),
                                                    55 => array('Curacao', 'كوراكاو'),
                                                    56 => array('Cyprus', 'قبرص'),
                                                    57 => array('Czech Republic', 'الجمهورية التشيكية'),
                                                    58 => array('Denmark', 'الدنمارك'),
                                                    59 => array('Djibouti', 'جيبوتي'),
                                                    60 => array('Dominica', 'دومينيكا'),
                                                    61 => array('Dominican Republic', 'جمهورية الدومينيكان'),
                                                    62 => array('East Timor', 'تيمور الشرقية'),
                                                    63 => array('Ecuador', 'الاكوادور'),
                                                    64 => array('Egypt', 'مصر'),
                                                    65 => array('El Salvador', 'السلفادور'),
                                                    66 => array('Equatorial Guinea', 'غينيا الإستوائية'),
                                                    67 => array('Eritrea', 'إريتريا'),
                                                    68 => array('Estonia', 'إستونيا'),
                                                    69 => array('Ethiopia', 'أثيوبيا'),
                                                    70 => array('Falkland Islands', 'جزر فوكلاند'),
                                                    71 => array('Faroe Islands', 'جزر فاروس'),
                                                    72 => array('Fiji', 'فيجي'),
                                                    73 => array('Finland', 'فنلندا'),
                                                    74 => array('France', 'فرنسا'),
                                                    75 => array('French Guiana', 'غيانا الفرنسية'),
                                                    76 => array('French Polynesia', 'بولينيزيا الفرنسية'),
                                                    77 => array('French Southern Ter', 'الأراضي الفرنسية الجنوبية'),
                                                    78 => array('Gabon', 'الجابون'),
                                                    79 => array('Gambia', 'غامبيا'),
                                                    80 => array('Georgia', 'جورجيا'),
                                                    81 => array('Germany', 'ألمانيا'),
                                                    82 => array('Ghana', 'غانا'),
                                                    83 => array('Gibraltar', 'جبل طارق'),
                                                    84 => array('Great Britain', 'بريطانيا العظمى'),
                                                    85 => array('Greece', 'اليونان'),
                                                    86 => array('Greenland', 'الأرض الخضراء'),
                                                    87 => array('Grenada', 'غرينادا'),
                                                    88 => array('Guadeloupe', 'جوادلوب'),
                                                    89 => array('Guam', 'غوام'),
                                                    90 => array('Guatemala', 'غواتيمالا'),
                                                    91 => array('Guinea', 'غينيا'),
                                                    92 => array('Guyana', 'غيانا'),
                                                    93 => array('Haiti', 'هايتي'),
                                                    94 => array('Hawaii', 'هاواي'),
                                                    95 => array('Honduras', 'هندوراس'),
                                                    96 => array('Hong Kong', 'هونج كونج'),
                                                    97 => array('Hungary', 'هنغاريا'),
                                                    98 => array('Iceland', 'أيسلندا'),
                                                    99 => array('Indonesia', 'إندونيسيا'),
                                                    100 => array('India', 'الهند'),
                                                    101 => array('Iran', 'إيران'),
                                                    102 => array('Iraq', 'العراق'),
                                                    103 => array('Ireland', 'أيرلندا'),
                                                    104 => array('Isle of Man', 'جزيرة مان'),
                                                    105 => array('Israel', 'إسرائيل'),
                                                    106 => array('Italy', 'إيطاليا'),
                                                    107 => array('Jamaica', 'جامايكا'),
                                                    108 => array('Japan', 'اليابان'),
                                                    109 => array('Jordan', 'الأردن'),
                                                    110 => array('Kazakhstan', 'كازاخستان'),
                                                    111 => array('Kenya', 'كينيا'),
                                                    112 => array('Kiribati', 'كيريباتي'),
                                                    113 => array('Korea North', 'كوريا الشمالية'),
                                                    114 => array('Korea South', 'كوريا الجنوبية'),
                                                    115 => array('Kuwait', 'الكويت'),
                                                    116 => array('Kyrgyzstan', 'قيرغيزستان'),
                                                    117 => array('Laos', 'لاوس'),
                                                    118 => array('Latvia', 'لاتفيا'),
                                                    119 => array('Lebanon', 'لبنان'),
                                                    120 => array('Lesotho', 'ليسوتو'),
                                                    121 => array('Liberia', 'ليبيريا'),
                                                    122 => array('Libya', 'ليبيا'),
                                                    123 => array('Liechtenstein', 'ليختنشتاين'),
                                                    124 => array('Lithuania', 'ليتوانيا'),
                                                    125 => array('Luxembourg', 'لوكسمبورغ'),
                                                    126 => array('Macau', 'ماكاو'),
                                                    127 => array('Macedonia', 'مقدونيا'),
                                                    128 => array('Madagascar', 'مدغشقر'),
                                                    129 => array('Malaysia', 'ماليزيا'),
                                                    130 => array('Malawi', 'ملاوي'),
                                                    131 => array('Maldives', 'جزر المالديف'),
                                                    132 => array('Mali', 'مالي'),
                                                    133 => array('Malta', 'مالطا'),
                                                    134 => array('Marshall Islands', 'جزر مارشال'),
                                                    135 => array('Martinique', 'مارتينيك'),
                                                    136 => array('Mauritania', 'موريتانيا'),
                                                    137 => array('Mauritius', 'موريشيوس'),
                                                    138 => array('Mayotte', 'مايوت'),
                                                    139 => array('Mexico', 'المكسيك'),
                                                    140 => array('Midway Islands', 'جزر ميدواي'),
                                                    141 => array('Moldova', 'مولدوفا'),
                                                    142 => array('Monaco', 'موناكو'),
                                                    143 => array('Mongolia', 'منغوليا'),
                                                    144 => array('Montserrat', 'مونتسيرات'),
                                                    145 => array('Morocco', 'المغرب'),
                                                    146 => array('Mozambique', 'موزمبيق'),
                                                    147 => array('Myanmar', 'ميانمار'),
                                                    148 => array('Nambia', 'نامبيا'),
                                                    149 => array('Nauru', 'ناورو'),
                                                    150 => array('Nepal', 'نيبال'),
                                                    151 => array('Netherland Antilles', 'جزر الأنتيل الهولندية'),
                                                    152 => array('Netherlands', 'هولندا'),
                                                    153 => array('Nevis', 'نيفيس'),
                                                    154 => array('New Caledonia', 'كاليدونيا الجديدة'),
                                                    155 => array('New Zealand', 'نيوزيلاندا'),
                                                    156 => array('Nicaragua', 'نيكاراغوا'),
                                                    157 => array('Niger', 'النيجر'),
                                                    158 => array('Nigeria', 'نيجيريا'),
                                                    159 => array('Niue', 'نيوي'),
                                                    160 => array('Norfolk Island', 'جزيرة نورفولك'),
                                                    161 => array('Norway', 'النرويج'),
                                                    162 => array('Oman', 'سلطنة عمان'),
                                                    163 => array('Pakistan', 'باكستان'),
                                                    164 => array('Palau Island', 'جزيرة بالاو'),
                                                    165 => array('Palestine', 'فلسطين'),
                                                    166 => array('Panama', 'بنما'),
                                                    167 => array('Papua New Guinea', 'بابوا غينيا الجديدة'),
                                                    168 => array('Paraguay', 'باراغواي'),
                                                    169 => array('Peru', 'بيرو'),
                                                    170 => array('Philippines', 'فيلبيني'),
                                                    171 => array('Pitcairn Island', 'جزيرة بيتكيرن'),
                                                    172 => array('Poland', 'بولندا'),
                                                    173 => array('Portugal', 'البرتغال'),
                                                    174 => array('Puerto Rico', 'بورتوريكو'),
                                                    175 => array('Qatar', 'قطر'),
                                                    176 => array('Republic of Montenegro', 'جمهورية الجبل الأسود'),
                                                    177 => array('Republic of Serbia', 'جمهورية صربيا'),
                                                    178 => array('Romania', 'رومانيا'),
                                                    179 => array('Russia', 'روسيا'),
                                                    180 => array('Rwanda', 'رواندا'),
                                                    181 => array('St Barthelemy', 'سانت بارتيليمي'),
                                                    182 => array('St Eustatius', 'القديس يوستاتيوس'),
                                                    183 => array('St Helena', 'سانت هيلانة'),
                                                    184 => array('St Kitts-Nevis', 'سانت كيتس ونيفيس'),
                                                    185 => array('St Lucia', 'شارع لوسيا'),
                                                    186 => array('St Maarten', 'سانت مارتن'),
                                                    187 => array('St Pierre & Miquelon', 'سانت بيير وميكلون'),
                                                    188 => array('St Vincent & Grenadines', 'سانت فنسنت وغرينادين'),
                                                    189 => array('Saipan', 'سايبان'),
                                                    190 => array('Samoa', 'ساموا'),
                                                    191 => array('Samoa American', 'ساموا الأمريكية'),
                                                    192 => array('San Marino', 'سان مارينو'),
                                                    193 => array('Sao Tome & Principe', 'ساو تومي وبرينسيبي'),
                                                    194 => array('Saudi Arabia', 'المملكة العربية السعودية'),
                                                    195 => array('Senegal', 'السنغال'),
                                                    196 => array('Seychelles', 'سيشيل'),
                                                    197 => array('Sierra Leone', 'سيرا ليون'),
                                                    198 => array('Singapore', 'سنغافورة'),
                                                    199 => array('Slovakia', 'سلوفاكيا'),
                                                    200 => array('Slovenia', 'سلوفينيا'),
                                                    201 => array('Solomon Islands', 'جزر سليمان'),
                                                    202 => array('Somalia', 'الصومال'),
                                                    203 => array('South Africa', 'جنوب أفريقيا'),
                                                    204 => array('Spain', 'إسبانيا'),
                                                    205 => array('Sri Lanka', 'سيريلانكا'),
                                                    206 => array('Sudan', 'السودان'),
                                                    207 => array('Suriname', 'سورينام'),
                                                    208 => array('Swaziland', 'سوازيلاند'),
                                                    209 => array('Sweden', 'السويد'),
                                                    210 => array('Switzerland', 'سويسرا'),
                                                    211 => array('Syria', 'سوريا'),
                                                    212 => array('Tahiti', 'تاهيتي'),
                                                    213 => array('Taiwan', 'تايوان'),
                                                    214 => array('Tajikistan', 'طاجيكستان'),
                                                    215 => array('Tanzania', 'تنزانيا'),
                                                    216 => array('Thailand', 'تايلاند'),
                                                    217 => array('Togo', 'توجو'),
                                                    218 => array('Tokelau', 'توكيلاو'),
                                                    219 => array('Tonga', 'تونغا'),
                                                    220 => array('Trinidad & Tobago', 'ترينيداد توباغو'),
                                                    221 => array('Tunisia', 'تونس'),
                                                    222 => array('Turkey', 'تركيا'),
                                                    223 => array('Turkmenistan', 'تركمانستان'),
                                                    224 => array('Turks & Caicos Is', 'جزر توركس وكايكوس'),
                                                    225 => array('Tuvalu', 'توفالو'),
                                                    226 => array('Uganda', 'أوغندا'),
                                                    227 => array('United Kingdom', 'المملكة المتحدة'),
                                                    228 => array('Ukraine', 'أوكرانيا'),
                                                    229 => array('United Arab Emirates', 'الإمارات العربية المتحدة'),
                                                    230 => array('United States of America', 'الولايات المتحدة الأمريكية'),
                                                    231 => array('Uruguay', 'أوروغواي'),
                                                    232 => array('Uzbekistan', 'أوزبكستان'),
                                                    233 => array('Vanuatu', 'فانواتو'),
                                                    234 => array('Vatican City State', 'دولة مدينة الفاتيكان'),
                                                    235 => array('Venezuela', 'فنزويلا'),
                                                    236 => array('Vietnam', 'فيتنام'),
                                                    237 => array('Virgin Islands (Brit)', 'جزر فيرجن (بريطانيا)'),
                                                    238 => array('Virgin Islands (USA)', 'جزر فيرجن (الولايات المتحدة الأمريكية)'),
                                                    239 => array('Wake Island', 'جزيرة ويك'),
                                                    240 => array('Wallis & Futana Is', 'واليس وفوتونا'),
                                                    241 => array('Yemen', 'اليمن'),
                                                    242 => array('Zaire', 'زائير'),
                                                    243 => array('Zambia', 'زامبيا'),
                                                    244 => array('Zimbabwe', 'زيمبابوي'),
                                                );

                                                ?>
                                                @foreach($countries as $country)
                                                    <option value="{{$country[0]}}">{{ $lang == 'ar' ? $country[1] : $country[0] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-end">
                                <input type="hidden" name="message" value="N/A">
                                <input type="submit" value="{{  $lang=='ar' ? 'إرسال ' : 'Submit' }}" class="mt-3">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
            </div>
        </section>

    </div>
@endsection

@section('js')
    <script>

    </script>
@endsection
