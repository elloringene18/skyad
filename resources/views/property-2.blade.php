@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/') }}css/inner.css?v=1.1">
    <style>
        .paginations {
            width: auto;
            float: right;
        }

        hr.gray {
            background-color: #ccc;
            height: 1px;
        }

        .f20 {
            font-size: 20px;
        }

        .innercontent {
            margin-top: 110px;
        }

        .tab-box {
            background-color: #B2E1D8;
            padding: 40px;
            color: #fff;
            width: 100%;
            position: absolute;
        }

        .tab-box.active {
            display: block;
        }

        .tab-box h3 {
            max-width: 450px;
            margin-bottom: 0;
        }

        .icon-txt {
            margin-bottom: 20px;
        }

        .icon-txt img {
            /*margin-bottom: 20px;*/
        }

        .icon-txt .img {
            min-width: 60px;
        }

        .icon-txt h4 {
            color: #808080;
            text-transform: capitalize;
            margin-bottom: 0;
        }

        .icon-txt p {
            color: #808080;
            margin-bottom: 0;
            font-size: 24px;
        }

        .blue-wrap {
            background-image: url({{ asset('img/about/foot.jpg') }});
            text-align: center;
            font-size: 24px;
            line-height: 30px;
            height: 100%;
            position: absolute;
            right: 0;
            top: 0;
            width: 42%;
            z-index: 1;
            transition: background-color 300ms;
            background-repeat: no-repeat;
        }

        .blue-wrap img {
            top: 50%;
            margin-top: -30px;
            height: 60px;
            position: relative;
        }

        .download-box {
            background-color: #EBEBEB;
            position: relative;
            margin-top: 10px;
        }

        .download-box .bg {
            position: absolute;
            height: 100%;
            width: 100%;
        }

        .download-box .bg .left {
            height: 100%;
            width: 50%;
            background-color: #EBEBEB;
            position: absolute;
        }

        .download-box .bg .right {
            height: 100%;
            width: 50%;
            background-color: #B2E1D8;
            position: absolute;
            right: 0;
        }

        .download-box .left {
            background-color: #EBEBEB;
            padding: 60px 12px;
        }

        .download-box .right {
            background-color: #B2E1D8;
        }

        .sizeTexts .item {
            float: left;
            margin-right: 20px;
            padding-right: 20px;
            position: relative;
            margin-bottom: 20px;
        }

        .sizeTexts span {
            color: #808080;
        }

        .sizeTexts .big {
            font-weight: 400;
            font-size: 40px;
        }

        .sizeTexts .small {

        }

        .sizeTexts .bot {
            font-weight: 400;
            font-size: 25px;
        }

        .sizeTexts .divider {
            width: 2px;
            height: 50px;
            background-color: #fff;
            position: absolute;
            right: 0;
            top: 5px;
        }

        .arrows {
            width: 100%;
            float: right;
        }
        #intro-img {
            background-repeat: no-repeat;
            background-position: center;
            background-size: 100% auto;
            margin-top: 110px;
        }

        #intro-img video {
            width: 100%;
            max-height: 100%;
        }

        #downloadBt {
            background-image: url({{asset('img/arrow-bt-white.png')}});
            background-color: #b2e1d8;
            color: #fff;
            background-repeat: no-repeat;
            height: 70px;
            display: inline-block;
            width: 300px;
            background-position: right center;
            background-size: 40px;
            background-position: 93% center;
            line-height: 70px;
            padding-right: 60px;
            position: absolute;
            left: 50%;
            margin-left: -150px;
            top: 50%;
            margin-top: -35px;
        }

        #downloadBt:hover {
            background-color: #000;
        }

        .modal-dialog {
            max-width: 760px;
        }

        #slides .slide {
            /*margin-right: 10px;*/
        }

        .viewfloorplan {
            background-color: #B2E1D8;
            color: #575757;
            border: 0;
            height: 40px;
            padding: 0 30px;
            text-align: center;
            font-weight: bold;
            font-size: 20px;
            margin-left: 10px;
            min-width: 240px;
        }

        .icon-txt img {
            height: 35px;
        }

        .icon-txt h4 {
            font-size: 18px;
        }

        .icon-txt p {
            font-size: 16px;
        }

        .rightstuff {
            text-align: right;
        }

        @media only screen and (max-width:1720px) {
            #intro-img {
            }
        }

        @media only screen and (max-width:1610px) {
            .viewfloorplan {
                left: 445px;
            }
        }

        @media only screen and (max-width:1510px) {
            #intro-img {
                background-size: auto 100%;
            }
        }

        @media only screen and (max-width:1360px) {

            .blue-wrap {
                width: 35%;
            }
        }

        @media only screen and (max-width:1200px) {

            .tab-box h3 {
                max-width: 400px;
            }

            .blue-wrap {
                position: relative;
                right: auto;
                top: auto;
                width: 100%;
                z-index: 1;
                transition: background-color 300ms;
                background-image: none;
                text-align: left;
            }

            #downloadBt {
                position: initial;
                left: 0;
                top: 0;
                margin-left: 0;
                margin-top: 0;
                padding-left: 20px;
                margin-bottom: 60px;
            }

            .download-box .left {
                width: 100%;
            }
        }

        @media only screen and (max-width:991px) {

            .tab-box h3 {
                max-width: 280px;
            }
        }

        @media only screen and (max-width:767px) {
            .tab-box {
                position: relative;
            }


            .viewfloorplan {
                right: 12px;
                left: auto;
            }
        }

        @media only screen and (max-width:751px) {
            .viewfloorplan {
                margin-top: 10px;
            }
        }

        @media only screen and (max-width:576px) {
            .icon-txt {
            }

            #slides .slide img {
                height: auto;
                width: 100%;
            }

            #slides .slide {
                margin-right: 0;
            }

            .viewfloorplan {
                width: 100%;
                right: auto;
                margin-left: 0;
            }
        }

    </style>
@endsection

@section('content')

    <section class="container innercontent">
        <div class="row">
            <div class="container mt-5">
                <a href="{{ url('property-listings') }}" class="backbutton">< BACK</a>
                <p class="sub-heading" id="featured-title">PROPERTY</p>
                <h1 class="heading">{{ $post->title }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="container mt-3">
                <div id="slides">
                    <div class="slide">
                        <img src="{{asset('/'.$post->photo_full)}}" width="100%">
                    </div>
                    @if($post->galleryPhotos)
                        @foreach($post->galleryPhotos as $slide)
                            <div class="slide">
                                <img src="{{$slide}}" width="100%">
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-12">
                    <div class="arrows">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container mt-3 pb-3">
                <div class="row">
                    <div class="col-md-4 relative">
                        Starting {{ $post->area }}m<sup>2</sup>
                    </div>
                    <div class="col-md-8 rightstuff">
                        <button  data-bs-toggle="modal" data-bs-target="#saleh" class="viewfloorplan">View Floorplan</button>
                        <a href="{{ asset('files/Residence-Eight-The-New-Capital-Brochure.pdf') }}" target="_blank">
                            <button class="viewfloorplan">Download Brochure</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        {{--<p class="mt-3 mb-1">{!! $post->description !!}</p>--}}
                        <hr class="gray"/>
                        <br/>

                        <div class="modal fade" id="saleh" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body ">
                                        @foreach($post->floorplans as $fp)
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <h4 class="mb-3 gray-text"><b>{!! $fp->name !!} {!!  count($fp->levels) ? '(1<sup>st</sup> level)' : ''  !!}</b></h4>
                                                    <hr/>
                                                    @if($fp->photos)
                                                        @foreach($fp->photos as $photo)
                                                            <img src="{{ asset($photo->url) }}" width="100%" class="mb-3"><br/>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                <div class="col-lg-12 mb-5">
                                                    <p class="mb-1 darkgray-text"><b>Total Area: {{$fp->area}} m<sup>2</sup></b></p>
                                                    <p class="mb-1 darkgray-text"><b>Available on {{$fp->floors}}</b></p>

                                                    @if(count($fp->rooms))
                                                        <?php
                                                        $divider = ceil(count($fp->rooms->toArray())/2);
                                                        $roomGroups = array_chunk($fp->rooms->toArray(),$divider);
                                                        ?>
                                                        <div class="row mt-5">
                                                            @foreach($roomGroups as $group)
                                                                <?php $index = $loop->index; ?>
                                                                <div class="col-md-6">
                                                                    @foreach($group as $room)
                                                                        <p class="{{ $index == 0 ? 'f20' : '' }}"><b>{{ $room['name'] }}</b>: {{$room['value']}} m<sup>2</sup></p>
                                                                    @endforeach
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>

                                                @if($fp->levels)
                                                    @foreach($fp->levels as $level)
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <h5 class="mb-3 gray-text"><b>{!! $fp->name !!} (2<sup>nd</sup> level)</b></h5>
                                                                <hr/>
                                                                @foreach($level->photos as $photo)
                                                                    <img src="{{ asset($photo->url) }}" width="100%" class="mb-3"><br/>
                                                                @endforeach
                                                            </div>
                                                            <div class="col-lg-12 mb-5">
                                                                <?php
                                                                $divider = ceil(count($level->rooms->toArray())/2);
                                                                $roomGroups = array_chunk($level->rooms->toArray(),$divider);
                                                                ?>
                                                                <div class="row mt-5">
                                                                    @foreach($roomGroups as $group)
                                                                        <?php $index = $loop->index; ?>
                                                                        <div class="col-md-6">
                                                                            @foreach($group as $room)
                                                                                <p class="{{ $index == 0 ? 'f20' : '' }}"><b>{{ $room['name'] }}</b>: {{$room['value']}} m<sup>2</sup></p>
                                                                            @endforeach
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        {{--<div class="row">--}}
        {{--<div class="container mb-5 mt-5">--}}
        {{--<div class="row">--}}
        {{--<div class="col-lg-4">--}}
        {{--<h3>OTHER PROPERTIES</h3>--}}
        {{--<hr class=""/>--}}
        {{--</div>--}}
        {{--<div class="col-lg-8">--}}
        {{--</div>--}}
        {{--<div class="col-lg-12">--}}
        {{--<div class="row">--}}
        {{--@foreach($others as $other)--}}
        {{--<div class="col-lg-4">--}}
        {{--<div class="article">--}}
        {{--<a href="{{ url('/property-listings/'.$other->slug) }}"><img src="{{ asset('/'.$other->photo) }}" class="thumbnail"></a>--}}
        {{--<span class="type">Residential</span>--}}
        {{--<h2 class="title">{{ $other->title }}</h2>--}}
        {{--<p class="excerpt">{{ \Illuminate\Support\Str::words(strip_tags($other->description),20) }}... <a href="{{ url('/property-listings/'.$other->slug) }}" class="">Read more</a></p>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--@endforeach--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
    </section>

    <div class="container pb-1  center-mobile">
        <div class="row">
            <p class="sub-heading mt-1">CHERISHED EXPERIENCES & MULTI-SENSORY LUXURIES.</p>
            <h1 class="heading">Residence Eight.</h1>
        </div>
    </div>

    <div class="overflow-hidden relative">
        <div class="container">
            <div class="row">
                <div class="col-md-6 pt-3 center-mobile">
                    <p>Here, every element is mindfully chosen to create a community of ultimate privacy, built around its residents &  thoughtfully crafted to showcase unobstructed views of lush, green landscapes while evoking sentiments of serenity.</p>
                </div>
                <div class="col-md-1 pt-3 center-mobile"></div>
                <div class="col-md-5 pt-3 center-mobile">
                    <div class="row">
                        <div class="col-md-12 icon-txt">
                            <table border="0">
                                <tr>
                                    <td class="img">
                                        <img src="{{asset('img/projects/tree.png')}}">
                                    </td>
                                    <td>
                                        <h4>Breathing</h4>
                                        <p>Parks</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12 icon-txt">
                            <table border="0">
                                <tr>
                                    <td class="img">
                                        <img src="{{asset('img/projects/fountain.png')}}">
                                    </td>
                                    <td>
                                        <h4>Flowing Water</h4>
                                        <p>Features</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12 icon-txt">
                            <table border="0">
                                <tr>
                                    <td class="img">
                                        <img src="{{asset('img/projects/pool.png')}}">
                                    </td>
                                    <td>
                                        <p>Shimmering</p>
                                        <h4>Pools</h4>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12 icon-txt">
                            <table border="0">
                                <tr>
                                    <td class="img">
                                        <img src="{{asset('img/projects/man.png')}}">
                                    </td>
                                    <td>
                                        <p>Lush</p>
                                        <h4>Pedestrian Walkways</h4>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="download-box">
        <div class="bg">
            <div class="left"></div>
        </div>
        <div class="container relative" style="z-index: 1;">
            <div class="row full-mobile">
                <div class="col-md-7 left center-mobile">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="heading">A PREMIUM LOCATION.</h1>
                            <p>Residence Eight is minutes away from famous landmarks,
                                prestigious venues and the soon-to-be the center of New Cairo, in the heart of the New Capital.</p>
                            <div class="sizeTexts mt-5">
                                <div class="item">
                                    <span class="big">20</span> <span class="small">min. from</span><br/>
                                    <span class="bot">New Cairo</span>
                                    <div class="divider"></div>
                                </div>
                                <div class="item">
                                    <span class="big">25</span> <span class="small">min. from</span><br/>
                                    <span class="bot">Suez Road</span>
                                    <div class="divider"></div>
                                </div>
                                <div class="item">
                                    <span class="big">35</span> <span class="small">min. from</span><br/>
                                    <span class="bot">Airport</span>
                                    <div class="divider"></div>
                                </div>
                                <div class="item">
                                    <span class="big">40</span> <span class="small">min. from</span><br/>
                                    <span class="bot">Heliopolis</span>
                                    <div class="divider"></div>
                                </div>
                                <div class="item">
                                    <span class="big">60</span> <span class="small">min. from</span><br/>
                                    <span class="bot">Downtown</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="blue-wrap center-mobile">
            </div>
        </div>
    </div>
    <div class="container padded center-mobile">
        <div class="row">
            <h1 class="heading">Right where you need to be.</h1>
        </div>
    </div>

    <img src="{{asset('img/projects/Location-Map.jpg')}}" width="100%">

@endsection

@section('js')
    <script>
        $('#slides').slick({
            slidesToScroll: 1,
            slidesToShow: 1,
            appendArrows: '.arrows',
            adaptiveHeight: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 4000,
            speed: 2000,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 576,
                    settings: {
                        variableWidth: false,
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
        //
        // $(document).ready(function(){
        //     $('#slider').slick({
        //         slidesToScroll: 1,
        //         slidesToShow: 1,
        //         infinite: true,
        //         arrows: false,
        //         autoplay: true,
        //         adaptiveHeight: true,
        //         prevArrow: '<button type="button" class="slick-prev"></button>',
        //         nextArrow: '<button type="button" class="slick-next"></button>',
        //     });
        // });


        $(window).on('resize',function(){
            alignContent();
        });

        $(window).on('load',function(){
            alignContent();
        });
        //
        // function alignContent(){
        //     imgHeight = $('#scroll-items .item .text-box').closest('.item').find('img').height();
        //     $('#scroll-items .item .text-box').css('height',imgHeight);
        // }
        //
        // $(document).ready(function(){
        //     $('#scroll-items').slick({
        //         slidesToScroll: 1,
        //         slidesToShow: 1,
        //         variableWidth: true,
        //         infinite: true,
        //         appendArrows: '.arrows',
        //         autoplay: true,
        //         prevArrow: '<button type="button" class="slick-prev"></button>',
        //         nextArrow: '<button type="button" class="slick-next"></button>',
        //         autoplaySpeed: 3000,
        //         speed: 1000,
        //     });
        //
        //     alignContent();
        // });

    </script>
@endsection
