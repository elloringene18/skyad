@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/') }}css/inner.css?v=1.1">
    <style>
        .paginations {
            width: auto;
            float: right;
        }

        .article a .title {
            color: #575757;
        }

        .article a .title:hover {
            color: #70C3B3;
        }
        .pagebanner h2, .pagebanner p {
            text-shadow: 1px 1px 5px #000;
        }

        .pagebanner h2 {
            font-size: 22px;
        }
        .pagebanner p {
            text-shadow: 1px 1px 5px #000;
        }

        .innercontent .mani {
            font-size: 30px;
        }

        .newsletters .title p {
            display: inline-block;
            margin-bottom: 0;
            float: right;
            font-weight: normal;
            color: #fff;
            background-color: #b2e1d8;
            padding: 0 20px;
        }
    </style>
@endsection

@section('content')

    <section class="container innercontent">
        <div class="row">
            <div class="container center-mobile">
                <p class="sub-heading" id="featured-title">{{  $lang=='ar' ? 'الأخبار' : 'NEWS' }}</p>
                <h1 class="heading mani">{{  $lang=='ar' ? 'آخر أخبار سكاي أبوظبي للتطوير العقاري. ' : 'the latest about SKY AD. Developments' }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="container padded center-mobile">
                <a href="{{ $lang=='ar' ? url('/ar/news-and-press/'.$latest->slug) : url('/news-and-press/'.$latest->slug) }}"><div class="pagebanner" style="background-image: url({{asset('/'.$latest->photo_full)}})">
                    <div class="copy">
                        <h2 class="heading">{{ $lang == 'ar' ? $latest->title_ar : $latest->title }}</h2>
                        <p class="details">
                            {{ \Illuminate\Support\Str::words(strip_tags($lang == 'ar' ? $latest->content_ar : $latest->content),40) }}
                        </p>
                    </div>
                    </div></a>
            </div>
        </div>
        <div class="row">
            <div class="container mb-5">
                <div class="row">
                    <div class="col-lg-12 center-mobile">
                        <p class="sub-heading">{{  $lang=='ar' ? 'آخر منشور ' : 'LATEST POST' }}</p>
                        <hr class="mb-0"/>
                        <div class="row">

                            @foreach($articles as $index=>$article)
                                <div class="col-lg-4">
                                    <div class="article">
                                        <a href="{{ $lang=='ar' ? url('/ar/news-and-press/'.$article->slug) : url('/news-and-press/'.$article->slug) }}"><img src="{{ asset('/'.$article->photo) }}" class="thumbnail mb-3"></a>
                                        <a href="{{ $lang=='ar' ? url('/ar/news-and-press/'.$article->slug) : url('/news-and-press/'.$article->slug) }}"><h2 class="title">{{ $lang == 'ar' ? $article->title_ar : $article->title }}</h2></a>
                                        <p class="excerpt">{{ \Illuminate\Support\Str::words(strip_tags($lang == 'ar' ? $article->content_ar : $article->content),20) }}... <a href="{{ $lang=='ar' ? url('/ar/news-and-press/'.$article->slug) : url('/news-and-press/'.$article->slug) }}" class="">{{ $lang == 'ar' ? 'اقرأ المزيد ': 'Read more'}}</a></p>
                                    </div>
                                </div>
                            @endforeach
                            {{----}}
                            {{--<div class="col-lg-4">--}}
                                {{--<div class="article">--}}
                                    {{--<img src="{{ asset('/') }}img/press/2.png" class="thumbnail">--}}
                                    {{--<span class="type">Press Releases</span>--}}
                                    {{--<h2 class="title">Residence Eight unit selection at Sky Ad office</h2>--}}
                                    {{--<p class="excerpt">Snippets from Residence Eight official units selection event. Honored to have our clients... <a href="#" class="">Read more</a></p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-4">--}}
                                {{--<div class="article">--}}
                                    {{--<img src="{{ asset('/') }}img/press/3.png" class="thumbnail">--}}
                                    {{--<span class="type">Press Releases</span>--}}
                                    {{--<h2 class="title">SKY AD. Launch event</h2>--}}
                                    {{--<p class="excerpt">It was our absolute pleasure having you all at SKY. AD. Developments launching event at JW Marriott Hotel... <a href="#" class="">Read more</a></p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-4">--}}
                                {{--<div class="article">--}}
                                    {{--<img src="{{ asset('/') }}img/press/4.png" class="thumbnail">--}}
                                    {{--<span class="type">Press Releases</span>--}}
                                    {{--<h2 class="title">Residence Eight groundbreaking event </h2>--}}
                                    {{--<p class="excerpt">Residence Eight is setting its cornerstone at the New Capital, Residence Eight's site is progressing around the clock... <a href="#" class="">Read more</a></p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-4">--}}
                                {{--<div class="article">--}}
                                    {{--<img src="{{ asset('/') }}img/press/5.png" class="thumbnail">--}}
                                    {{--<span class="type">Press Releases</span>--}}
                                    {{--<h2 class="title">A Brighter Future starts Now!</h2>--}}
                                    {{--<p class="excerpt">Yesterday has witnessed The launch press conference event of SKY AD. Developments at ST. Regis Hotel, Cairo. In the... <a href="#" class="">Read more</a></p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-4">--}}
                                {{--<div class="article">--}}
                                    {{--<img src="{{ asset('/') }}img/press/6.png" class="thumbnail">--}}
                                    {{--<span class="type">Press Releases</span>--}}
                                    {{--<h2 class="title">Counselor Mohamed Abdel Wahab met with H.E Saleh Mohamed Bin Nasra</h2>--}}
                                    {{--<p class="excerpt">Counselor Mohamed Abdel Wahab, CEO of the General Authority for Investment (GAFI), met with H.E Saleh Mohamed Bin Nasra... <a href="#" class="">Read more</a></p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-4">--}}
                                {{--<div class="article">--}}
                                    {{--<img src="{{ asset('/') }}img/press/7.png" class="thumbnail">--}}
                                    {{--<span class="type">Press Releases</span>--}}
                                    {{--<h2 class="title">SKY AD. Developments and First Abu Dhabi Bank (FAB) Collaboration</h2>--}}
                                    {{--<p class="excerpt">A glance at signing the collaboration protocol between SKY AD. Developments and First Abu Dhabi Bank (FAB), to provide... <a href="#" class="">Read more</a></p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="col-lg-12">

                                <div class="paginations mt-3">
                                    {!! $articles->appends(request()->input())->links("pagination::bootstrap-4") !!}

                                    {{--<a href="#" class="prev arrow"></a>--}}
                                    {{--<a href="#" class="nav active">1</a>--}}
                                    {{--<a href="#" class="nav">2</a>--}}
                                    {{--<a href="#" class="nav">3</a>--}}
                                    {{--<a href="#" class="nav">4</a>--}}
                                    {{--<a href="#" class="nav">5</a>--}}
                                    {{--<a href="#" class="next arrow"></a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-8 center-mobile">
                        <p class="sub-heading">{{  $lang=='ar' ? 'مستجدات أعمال البناء' : 'Construction Updates' }}</p>
                        <hr class="mb-0"/>
                        <div class="row">
                            <div class="col-lg-12 mt-3">
                                <div class="article">
                                    <video width="100%" height="auto" controls>
                                        <source src="{{ asset('/') }}videos/Skyad-Construction-Update-02V-11.mp4" type="video/mp4">
                                    </video>
                                    <p class="">{{  $lang=='ar' ? 'SEPT 2022' : 'SEPT 2022' }}</p>
                                    <h2 class="title">{{  $lang=='ar' ? '18 أشهر من الاستمرار في أعمال البناء' : '18 months of construction progress' }}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 center-mobile">
                        <p class="sub-heading">{{  $lang=='ar' ? 'Newsletters' : 'Newsletters' }}</p>
                        <hr class="mb-0"/>
                        <div class="row">
                            <div class="col-lg-12 mt-3 newsletters">
                                <div class="article">
                                    <a href="{{ asset('files/SKY-AD-2021-Newsletter.pdf') }}" target="_blank"><img src="{{ asset('/files/SKY-AD-2021-Newsletter-1.jpg') }}" width="100%" alt="SKY-AD-2021-Newsletter" title="SKY AD2021 Newsletter" class="mb-3"></a>
                                    <h2 class="title">{{  $lang=='ar' ? 'SKY AD Newsletter 2021' : 'SKY AD Newsletter 2021' }} <a href="{{ asset('files/SKY-AD-2021-Newsletter.pdf') }}" target="_blank"><p class="">{{  $lang=='ar' ? 'Download' : 'Download' }}</p></a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('partials.newsletter')

@endsection

@section('js')
@endsection
