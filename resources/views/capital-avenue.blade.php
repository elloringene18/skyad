@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/') }}css/inner.css?v=1.1">
    <style>

        .innercontent {
            margin-top: 110px;
        }

        .tab-box {
            background-color: #B2E1D8;
            padding: 40px;
            color: #fff;
            width: 100%;
            position: absolute;
        }

        .tab-box.active {
            display: block;
        }

        .tab-box h3 {
            max-width: 450px;
            margin-bottom: 0;
        }

        .icon-txt {
            margin-bottom: 20px;
        }

        .icon-txt img {
            margin-bottom: 20px;
        }

        .icon-txt h4 {
            color: #808080;
            text-transform: capitalize;
            margin-bottom: 0;
        }

        .icon-txt p {
            color: #808080;
            margin-bottom: 0;
            font-size: 24px;
        }

        .blue-wrap {
            background-image: url({{ asset('img/about/foot.jpg') }});
            text-align: center;
            font-size: 24px;
            line-height: 30px;
            height: 100%;
            position: absolute;
            right: 0;
            top: 0;
            width: 450px;
            z-index: 1;
            transition: background-color 300ms;
        }

        .blue-wrap img {
            top: 50%;
            margin-top: -30px;
            height: 60px;
            position: relative;
        }

        .download-box {
            background-color: #EBEBEB;
            position: relative;
            margin-top: 10px;
        }

        .download-box .bg {
            position: absolute;
            height: 100%;
            width: 100%;
        }

        .download-box .bg .left {
            height: 100%;
            width: 50%;
            background-color: #EBEBEB;
            position: absolute;
        }

        .download-box .bg .right {
            height: 100%;
            width: 50%;
            background-color: #B2E1D8;
            position: absolute;
            right: 0;
        }

        .download-box .left {
            background-color: #EBEBEB;
            padding: 60px 12px;
        }

        .download-box .right {
            background-color: #B2E1D8;
        }

        .sizeTexts .item {
            float: left;
            margin-right: 20px;
            padding-right: 20px;
            position: relative;
            margin-bottom: 20px;
        }

        .sizeTexts span {
            color: #808080;
        }

        .sizeTexts .big {
            font-weight: 400;
            font-size: 40px;
        }

        .sizeTexts .small {

        }

        .sizeTexts .bot {
            font-weight: 400;
            font-size: 25px;
        }

        .sizeTexts .divider {
            width: 2px;
            height: 50px;
            background-color: #fff;
            position: absolute;
            right: 0;
            top: 5px;
        }

        .arrows {
            width: 80px;
            float: right;
        }
        #intro-img {
            background-repeat: no-repeat;
            background-position: center;
            background-size: 100% auto;
            margin-top: 110px;
        }

        #intro-img video {
            width: 100%;
            max-height: 100%;
        }

        #downloadBt {
            background-image: url({{asset('img/arrow-bt-white.png')}});
            background-color: #b2e1d8;
            color: #fff;
            background-repeat: no-repeat;
            height: 70px;
            display: inline-block;
            width: 300px;
            background-position: right center;
            background-size: 40px;
            background-position: 93% center;
            line-height: 70px;
            padding-right: 60px;
            position: absolute;
            left: 50%;
            margin-left: -150px;
            top: 50%;
            margin-top: -35px;
        }

        #topnav {
            margin-top: 110px;
        }

        #topnav .slick-prev, #scroll-items .slick-prev {
            position: absolute;
            background-color: #fff;
            z-index: 1;
            border-radius: 50%;
            padding: 20px;
            background-size: 70%;
            top: 50%;
            margin-top: -20px;
            left: 10px;
        }

        #topnav .slick-next, #scroll-items .slick-next {
            position: absolute;
            background-color: #fff;
            z-index: 1;
            border-radius: 50%;
            padding: 20px;
            background-size: 70%;
            top: 50%;
            margin-top: -20px;
            right: 10px;
        }

        #downloadBt:hover {
            background-color: #000;
        }

        @media only screen and (max-width:1720px) {
            #intro-img {
            }
        }

        @media only screen and (max-width:1510px) {
            #intro-img {
                background-size: auto 100%;
            }
        }

        @media only screen and (max-width:1200px) {

            .tab-box h3 {
                max-width: 400px;
            }

            .blue-wrap {
                position: relative;
                right: auto;
                top: auto;
                width: 100%;
                z-index: 1;
                transition: background-color 300ms;
                background-image: none;
                text-align: left;
            }

            #downloadBt {
                position: initial;
                left: 0;
                top: 0;
                margin-left: 0;
                margin-top: 0;
                padding-left: 20px;
                margin-bottom: 60px;
            }

            .download-box .left {
                width: 100%;
            }
        }

        @media only screen and (max-width:991px) {

            .tab-box h3 {
                max-width: 280px;
            }
        }

        @media only screen and (max-width:767px) {
            .tab-box {
                position: relative;
            }
        }

        @media only screen and (max-width:576px) {
            .icon-txt {
            }

        }
    </style>

    @if($lang=='ar')
    <style>
        .sizeTexts .divider {
            left: 0;
            right: auto;
        }
    </style>
    @endif
@endsection

@section('content')
{{--    <section id="intro-img">--}}
{{--        <div id="player-overlay">--}}
{{--            <video  autoplay="autoplay" loop="true" muted defaultmuted playsinline>--}}
{{--                <source src="{{ asset('/') }}/img/projects/final-opt-1080.mp4" />--}}
{{--            </video>--}}
{{--            <img src="{{ asset('img/projects/capital-avenue.jpg') }}" width="100%">--}}
{{--        </div>--}}
{{--    </section>--}}


    <div id="topnav">
        @for($x=1;$x<9;$x++)
            <div class="slide">
                <img src="{{ asset('/') }}img/projects/capital-slides/{{$x}}.jpg" width="100%;">
            </div>
        @endfor
    </div>

    <div class="container padded pb-1  center-mobile">
        <div class="row">
            <h1 class="heading">{{  $lang=='ar' ? 'CAPITAL AVENUE' : 'CAPITAL AVENUE' }}</h1>
            <h5 class="heading">{{  $lang=='ar' ? 'تجربة مميزة في Residence Eight' : 'A WALK SETTLED IN RESIDENCE EIGHT' }}</h5>
        </div>
    </div>

    <div class="overflow-hidden relative">
        <div class="container">
            <div class="row">
                <div class="col-md-6 pt-3 center-mobile">
                    @if($lang=='ar')
                        <p>بروعته وجماله، يعد Capital Avenue مركز تجاري مثالي تابع لـ Residence Eight. مع Capital Avenue، كل ما تحلم به بالقرب منك. يتميز هذا المركز التجاري بالمطاعم المتنوعة والعلامات التجارية المميزة وعيادات طبية والمكاتب الفريدة. تجربتك فيCapital Avenue تمدك بفرصة الاستمتاع بإطلالات خلابة والاجتماع بمن تحب. إنه أفضل تجربة تعيشها.</p>
                    @else
                        <p>With an avant-garde persona, Capital Avenue is the perfect commercial center by Residence Eight. Capital Avenue is where you find all you desire, impeccable and close with various renowned Food & Beverage outlets, premium retail brands, top-notch clinics and reputable administrative offices. As you enjoy pleasant sights of water features and connect with others at a central promenade, outdoor cafe area and kiosks, It’s the perfect fuse of all that life offers. </p>
                    @endif
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <div class="tab-box center-mobile">
                        <h3>{{  $lang=='ar' ? 'عالم من الرفاهية' : 'A bracing stroll through life' }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container padded pt-1 pb-0 ">
        <div class="row padded mt-3 pb-0">
            <div class="col-md-3 col-sm-6 icon-txt">
                <img src="{{asset('img/projects/capital/Cuisines.png')}}">
                <h4>{{  $lang=='ar' ? 'مطاعم' : 'Worldwide' }}</h4>
                <p>{{  $lang=='ar' ? 'عالمية' : 'Cuisines' }}</p>
            </div>
            <div class="col-md-3 col-sm-6 icon-txt">
                <img src="{{asset('img/projects/capital/Shopping.png')}}">
                <h4>{{  $lang=='ar' ? 'مركز ' : 'One Stop' }}</h4>
                <p>{{  $lang=='ar' ? 'للتسوق  ' : 'Shopping spot' }}</p>
            </div>
            <div class="col-md-3 col-sm-6 icon-txt">
                <img src="{{asset('img/projects/capital/Clinics.png')}}">
                <p>{{  $lang=='ar' ? 'عيادات' : 'Top-Notch ' }}</p>
                <h4>{{  $lang=='ar' ? ' طبية' : 'Clinics' }}</h4>
            </div>
            <div class="col-md-3 col-sm-6 icon-txt">
                <img src="{{asset('img/projects/capital/Offices.png')}}">
                <p>{{  $lang=='ar' ? 'مكاتب ' : 'Reputable ' }}</p>
                <h4>{{  $lang=='ar' ? 'إدارية فريدة' : 'Administrative offices' }}</h4>
            </div>
        </div>
    </div>

    <div class="padded pb-0">
        <div id="scroll-items" class="center-mobile">

            <div class="slide">
                <img src="{{ asset('/') }}img/projects/scroll/{{  $lang=='ar' ? '1c-ar.jpg' : '1c.jpg' }}" width="100%;">
            </div>
            <div class="slide">
                <img src="{{ asset('/') }}img/projects/scroll/{{  $lang=='ar' ? '2c-ar.jpg' : '2c.jpg' }}" width="100%;">
            </div>
            <div class="slide">
                <img src="{{ asset('/') }}img/projects/scroll/{{  $lang=='ar' ? '3c-ar3.jpg' : '3c2.jpg' }}" width="100%;">
            </div>
            {{--<div class="slide">--}}
            {{--<div class="full item">--}}
            {{--<img src="{{ asset('/') }}img/projects/1.jpg" width="100%;">--}}
            {{--</div>--}}
            {{--<div class="half item">--}}
            {{--<div class="text-box">--}}
            {{--<span><p class="big">23</p>acres of bespoke living. </span>--}}
            {{--</div>--}}
            {{--<img src="{{ asset('/') }}img/projects/2.jpg" width="100%;">--}}
            {{--</div>--}}
            {{--<div class="half item gray">--}}
            {{--<img src="{{ asset('/') }}img/projects/3.jpg" width="100%;">--}}
            {{--<div class="text-box bottom">--}}
            {{--<span><p class="big">360</p>degree views.</span>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="slide">--}}
            {{--<div class=" full item">--}}
            {{--<img src="{{ asset('/') }}img/projects/4.jpg" width="100%;">--}}
            {{--</div>--}}
            {{--<div class=" half item">--}}
            {{--<div class="text-box">--}}
            {{--<span><p class="big">82%</p>Greenery.</span>--}}
            {{--</div>--}}
            {{--<img src="{{ asset('/') }}img/projects/5.jpg" width="100%;">--}}
            {{--</div>--}}
            {{--<div class=" half item gray">--}}
            {{--<img src="{{ asset('/') }}img/projects/6.jpg" width="100%;">--}}
            {{--<div class="text-box bottom">--}}
            {{--<span><p class="big">15000m<sup>2</sup></p>of entertainment space.</span>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
        <div class="container-fluid">
            <div class="arrows">
            </div>
        </div>
    </div>
    <div class="download-box">
        <div class="bg">
            <div class="left"></div>
        </div>
        <div class="container relative" style="z-index: 1;">
            <div class="row full-mobile">
                <div class="col-md-8 left center-mobile">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="heading">{{  $lang=='ar' ? 'في قلب الغد' : 'IN THE HEART OF TOMORROW' }}</h1>
                            <div class="sizeTexts mt-5">
                                <div class="item">
                                    <span class="big">25</span> <span class="small">{{  $lang=='ar' ? 'دقيقة من ' : 'min. from' }}</span><br/>
                                    <span class="bot">{{  $lang=='ar' ? 'طريق السويس' : 'Suez Road' }}</span>
                                    <div class="divider"></div>
                                </div>
                                <div class="item">
                                    <span class="big">35</span> <span class="small">{{  $lang=='ar' ? 'دقيقة من ' : 'min. from' }}</span><br/>
                                    <span class="bot">{{  $lang=='ar' ? 'المطار ' : 'Airport' }}</span>
                                    <div class="divider"></div>
                                </div>
                                <div class="item">
                                    <span class="big">40</span> <span class="small">{{  $lang=='ar' ? 'دقيقة من ' : 'min. from' }}</span><br/>
                                    <span class="bot">{{  $lang=='ar' ? 'حي مصر الجديدة' : 'Heliopolis' }}</span>
                                    <div class="divider"></div>
                                </div>
                                <div class="item">
                                    <span class="big">60</span> <span class="small">{{  $lang=='ar' ? 'دقيقة من ' : 'min. from' }}</span><br/>
                                    <span class="bot">{{  $lang=='ar' ? 'وسط القاهرة' : 'Downtown Cairo' }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="blue-wrap center-mobile">
                <a href="{{ asset('files/Capital-Avenue-Brochure.pdf') }}" target="_blank" id="downloadBt">{{  $lang=='ar' ? 'حمِّل الكتيب ' : 'Download Brochure' }}</a>
            </div>
        </div>
    </div>
    <div class="container padded center-mobile">
        <div class="row">
            <h1 class="heading">{{  $lang=='ar' ? 'كل ما تحلم به، بالقرب منك ' : 'ALL YOU DESIRE, IMPECCABLE AND CLOSE.' }}</h1>
        </div>
    </div>

    <img src="{{asset('img/projects/capital-map.PNG')}}" width="100%">


@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('#slider').slick({
                slidesToScroll: 1,
                slidesToShow: 1,
                infinite: true,
                arrows: false,
                @if($lang=='ar')
                    rtl: true,
                @endif
                autoplay: true,
                adaptiveHeight: true,
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
                autoplaySpeed: 2000,
            });
        });


        $(window).on('resize',function(){
            alignContent();
        });

        $(window).on('load',function(){
            alignContent();
        });

        function alignContent(){
            imgHeight = $('#scroll-items .item .text-box').closest('.item').find('img').height();
            $('#scroll-items .item .text-box').css('height',imgHeight);
        }

        $(document).ready(function(){
            $('#scroll-items').slick({
                slidesToScroll: 1,
                slidesToShow: 1,
                variableWidth: true,
                infinite: true,
                @if($lang=='ar')
                rtl: true,
                touchMove: false,
                @endif
                autoplay: true,
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
                autoplaySpeed: 3000,
                speed: 1000,
            });

            $('#topnav').slick({
                slidesToScroll: 1,
                slidesToShow: 1,
                infinite: true,
                @if($lang=='ar')
                rtl: true,
                touchMove: false,
                @endif
                autoplay: true,
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
                arrows: true,
                autoplaySpeed: 3000,
                speed: 1000,
            });

            alignContent();
        });

    </script>
@endsection
