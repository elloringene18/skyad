@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/') }}css/inner.css?v=1.1">
    <style>
        .paginations {
            width: auto;
            float: right;
        }

        hr.gray {
            background-color: #ccc;
            height: 1px;
        }

        .innercontent .mani {
            font-size: 24px;
        }

        .article-content p, .article-content div {
            font-family: 'TT Commons';
            font-size: 18px;
            line-height: 21px;

            @if($lang=='ar')
                font-family: 'Cairo';
                line-height: 25px;
            @endif
        }

        @if($lang=='ar')
            .heading.mani {
                line-height: 1.5;
            }
        @endif
    </style>
@endsection

@section('content')

    <section class="container innercontent  center-mobile">
        <div class="row">
            <div class="container mt-5">
                <a href="{{ $lang=='ar' ? url('/ar/news-and-press/') : url('/news-and-press/') }}" class="backbutton">< {{ $lang == 'ar' ? 'رجع' : 'BACK' }}</a>
                <p class="sub-heading" id="featured-title">{{ $lang == 'ar' ? 'الأخبار' : 'NEWS' }} </p>
                <h1 class="heading mani">{{ $lang == 'ar' ? $post->title_ar : $post->title }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="container mt-3">
                <img src="{{asset('/'.$post->photo_full)}}" width="100%">
            </div>
        </div>
        <div class="row">
            <div class="container mb-5 mt-3">
                <div class="row">
                    <div class="col-lg-12">
                        <hr class="gray"/>
                        <br/>
                        <div class="row article-content">
                            {!! $lang == 'ar' ? $post->content_ar : $post->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container mb-5 mt-5">
                <div class="row">
                    <div class="col-lg-4">
                        <b>{{  $lang=='ar' ? 'الأخبار ذات الصلة' : 'RELATED NEWS' }}</b>
                        <hr class=""/>
                    </div>
                    <div class="col-lg-8">
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            @foreach($others as $other)
                                <div class="col-lg-4">
                                    <div class="article">
                                        <a href="{{ $lang=='ar' ? url('/ar/news-and-press/'.$other->slug) : url('/news-and-press/'.$other->slug) }}"><img src="{{ asset('/'.$other->photo) }}" class="thumbnail mb-3"></a>
                                        {{--<span class="type">Press Releases</span>--}}
                                        <h2 class="title">{{ $lang == 'ar' ? $other->title_ar : $other->title }}</h2>
                                        <p class="excerpt">{{ \Illuminate\Support\Str::words(strip_tags($lang=='ar' ? $other->content_ar : $other->content),20) }}... <a href="{{ url('/news-and-press/'.$other->slug) }}" class="">{{ $lang == 'ar' ? 'اقرأ المزيد ': 'Read more'}}</a></p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
@endsection
