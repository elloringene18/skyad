@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/') }}css/inner.css?v=1.1">
    <style>
        #map {
            height: 400px;
        }

        .divider {
            width: 2px;
            background-color: #C8C8C8;
            height: 100px;
            display: inline-block;
        }

        .tab-link.active,
        .tab-link:hover {
            border-bottom: 2px solid #70C3B3;
        }

        .tab-link h3 {
            color: #C8C8C8;
        }

        .tab-link.active h3 {
            color: #70C3B3;
        }

        .tab-link {
            padding: 0;
            padding-bottom: 20px;
            cursor: pointer;
        }

        .tab-content.active {
            padding: 0;
        }

        .tab-links {
            padding: 0 12px;
        }

        .open {
            position: absolute;
            top: 20px;
            right: 20px;
        }

        input[type=submit] {
            width: 140px;
        }

        @media only screen and (max-width: 767px) {
            .divider {
                display: none;
            }

            .tab-link {
                margin-bottom: 20px;
                border-bottom: 2px solid #C8C8C8;
            }
        }
    </style>
    <style>
        .this {
            opacity: 0;
            position: absolute;
            top: 0;
            left: 0;
            height: 0;
            width: 0;
            z-index: -1;
        }

        .border_new{
            color: #575757;
            padding: 0;
            height: 36px;
            line-height: 36px;
            border: 1px solid transparent;
            border-top-color: transparent;
            border-right-color: transparent;
            border-bottom-color: transparent;
            border-left-color: transparent;
            border-color: transparent transparent #ccc transparent;
            cursor: pointer;
            margin-bottom: 10px;
            font-weight: 500;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')

    <section id="contact" class="innercontent">
        <div class="container padded  center-mobile">
            <h1 class="heading mb-5">{{ $lang == 'ar' ? 'زرنا' : 'Visit us' }}</h1>
            <div class="tab-links">
                <div class="row">
                    <div class="col-md-5 tab-link active">
                        <h3><img src="{{ asset('img/contact/flag-uae.png') }}" width="40"
                                style="vertical-align: top;margin-top: 4px;">
                            {{ $lang == 'ar' ? 'الإمارات العربية المتحدة ' : 'UAE' }}</h3>
                        <p class="sub-heading">
                            {!! $lang == 'ar'
                                ? '<br/>مركز العاصمة، برج البادية، مكتب رقم 1101
                                                        مجموعة دايموند، أبوظبي، الإمارات العربية المتحدة
                                                        '
                                : 'Capital Center Al Badi Tower Office 1101,<br/>Diamond Group, Abu Dhabi, UAE.' !!}
                        </p>
                    </div>
                    <div class="col-md-2 text-center">
                        <span class="divider"></span>
                    </div>
                    <div class="col-md-5 tab-link">
                        <h3><img src="{{ asset('img/contact/flag-egypt.png') }}" width="40"
                                style="vertical-align: top;margin-top: 4px;"> {{ $lang == 'ar' ? 'مصر' : 'Egypt' }}</h3>
                        <p class="sub-heading">

                            {!! $lang == 'ar'
                                ? 'مول O1، محور محمد نجيب<br/>
                                                        مدينة القاهرة الجديدة<br/>
                                                        محافظة القاهرة، مصر
                                                        '
                                : 'O1 Mall, Mohammed Nagib Axis, First New Cairo.<br/> Cairo Governorate, Egypt.' !!}

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid  center-mobile">
            <div class="row">
                <div class="tab-content active relative">
                    <a href="https://www.google.com/maps/place/Diamond+Group/@24.4178236,54.4401008,18.83z/data=!4m9!1m2!2m1!1sCAPITAL+CENTER+AL+BADI+TOWER+OFFICE+1101,+DIAMOND+GROUP,+ABU+DHABI,+UAE.!3m5!1s0x3e5e4397710e73fd:0x9d3b1ea9697297e8!8m2!3d24.4178704!4d54.4400565!15sCkhDQVBJVEFMIENFTlRFUiBBTCBCQURJIFRPV0VSIE9GRklDRSAxMTAxLCBESUFNT05EIEdST1VQLCBBQlUgREhBQkksIFVBRS6SARBjb3Jwb3JhdGVfb2ZmaWNl"
                        target="_blank">
                        <img src="{{ asset('img/contact/map-ab.png') }}" width="100%">
                        <img src="{{ asset('img/contact/open.png') }}" width="120" class="open">
                    </a>
                </div>
                <div class="tab-content relative">
                    <a href="https://www.google.com/maps/place/Jones+the+Grocer+-+O1+Mall/@30.0491298,31.4754289,15z/data=!4m2!3m1!1s0x0:0x285fedd38ca24f8c?sa=X&ved=2ahUKEwj4yfikocXyAhUStRoKHSpJAGcQ_BIwEnoECFMQBQ"
                        target="_blank">
                        <img src="{{ asset('img/contact/map-eg.png') }}" width="100%">
                        <img src="{{ asset('img/contact/open.png') }}" width="120" class="open">
                    </a>
                </div>
            </div>
        </div>
        <div class="container padded  center-mobile">

            <form action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
                <input type=hidden name="oid" value="00D4L000000CsPN">
                <input type=hidden name="retURL" value="https://skyabudhabi.ae/">


                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="heading mb-5">{{ $lang == 'ar' ? 'تواصل معنا. ' : 'get in touch.' }}</h1>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                    @endif
                                    @if (Session::has('error'))
                                        <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                    @endif
                                    @csrf

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <!--- Row 1 ---->
                            <div class="col-md-5">
                                <label for="first_name">{{ $lang == 'ar' ? ' الاسم الأول' : 'First Name' }}*</label>
                                <input id="first_name" maxlength="40" name="first_name" size="20" type="text" 
                                      class="form-control border_new" placeholder="{{ $lang == 'ar' ? 'الاسم الأول' : 'Add your First Name' }}" required>
                            </div>

                            <div class="col-md-1"></div>

                            <div class="col-md-5">
                                <label for="last_name">{{ $lang == 'ar' ? ' الاسم الثاني' : 'Last Name' }}*</label>
                                <input id="last_name" maxlength="80" name="last_name" size="20" type="text"
                                    class="form-control border_new" placeholder="{{ $lang == 'ar' ? 'الاسم الثاني' : 'Add your Last Name' }}" required>
                            </div>

                            <!--- End Row 1 ---->


                            <!--- Row 2 ---->
                            <div class="col-md-5">
                                <label for="email">{{ $lang == 'ar' ? ' البريد الإلكتروني' : 'Email' }}*</label>
                                <input id="email" maxlength="80" name="email" size="20" type="text"
                                    class="form-control border_new" placeholder="{{ $lang == 'ar' ? 'البريد الإلكتروني' : 'Add your email' }}" required>
                            </div>

                            <div class="col-md-1"></div>

                            <div class="col-md-5">
                                <label for="mobile">{{ $lang == 'ar' ? ' رقم التواصل' : 'Contact Number' }}*</label>
                                <input id="mobile" maxlength="40" name="mobile" size="20" type="text"
                                    class="form-control border_new mobilenumber" placeholder="{{ $lang == 'ar' ? ' ادخل رقم التواصل' : 'Add your contact number' }}"
                                    required>
                            </div>

                            <!--- End Row 2 ---->

                            <!--- Row 3 ---->
                            <div class="col-md-11">
                                <label for="00N4L000009OLYD">{{ $lang == 'ar' ? 'الدولة' : 'Resident Country' }}*</label>
                                <div class="custom-select">
                                    <select id="00N4L000009OLYD" name="00N4L000009OLYD" title="Resident Country"
                                        class="form-control">
                                        <option value="">--None--</option>
                                        <option value="Egypt">Egypt</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Azerbaijan">Azerbaijan</option>
                                        <option value="Bahamas">Bahamas</option>
                                        <option value="Bahrain">Bahrain</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Belgium">Belgium</option>
                                        <option value="Belize">Belize</option>
                                        <option value="Benin">Benin</option>
                                        <option value="Bermuda">Bermuda</option>
                                        <option value="Bhutan">Bhutan</option>
                                        <option value="Bolivia">Bolivia</option>
                                        <option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba
                                        </option>
                                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                        <option value="Botswana">Botswana</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                                        <option value="Bulgaria">Bulgaria</option>
                                        <option value="Burkina Faso">Burkina Faso</option>
                                        <option value="Burundi">Burundi</option>
                                        <option value="Cambodia">Cambodia</option>
                                        <option value="Cameroon">Cameroon</option>
                                        <option value="Canada">Canada</option>
                                        <option value="Cape Verde">Cape Verde</option>
                                        <option value="Cayman Islands">Cayman Islands</option>
                                        <option value="Central African Republic">Central African Republic</option>
                                        <option value="Chad">Chad</option>
                                        <option value="Chile">Chile</option>
                                        <option value="China">China</option>
                                        <option value="Christmas Island">Christmas Island</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="Comoros">Comoros</option>
                                        <option value="Congo">Congo</option>
                                        <option value="Congo, the Drc">Congo, the Drc</option>
                                        <option value="Cook Islands">Cook Islands</option>
                                        <option value="Costa Rica">Costa Rica</option>
                                        <option value="Cote d&#39;Ivoire">Cote d&#39;Ivoire</option>
                                        <option value="Croatia">Croatia</option>
                                        <option value="CuraÃƒÆ’Ã‚Â§ao">CuraÃƒÆ’Ã‚Â§ao</option>
                                        <option value="Cyprus">Cyprus</option>
                                        <option value="Czech Republic">Czech Republic</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Djibouti">Djibouti</option>
                                        <option value="Dominica">Dominica</option>
                                        <option value="Dominican Republic">Dominican Republic</option>
                                        <option value="Ecuador">Ecuador</option>
                                        <option value="El Salvador">El Salvador</option>
                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                        <option value="Eritrea">Eritrea</option>
                                        <option value="Estonia">Estonia</option>
                                        <option value="Ethiopia">Ethiopia</option>
                                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                        <option value="Faroe Islands">Faroe Islands</option>
                                        <option value="Fiji">Fiji</option>
                                        <option value="Finland">Finland</option>
                                        <option value="France">France</option>
                                        <option value="French Guiana">French Guiana</option>
                                        <option value="French Polynesia">French Polynesia</option>
                                        <option value="Gabon">Gabon</option>
                                        <option value="Gambia">Gambia</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="Ghana">Ghana</option>
                                        <option value="Gibraltar">Gibraltar</option>
                                        <option value="Greece">Greece</option>
                                        <option value="Greenland">Greenland</option>
                                        <option value="Grenada">Grenada</option>
                                        <option value="Guadeloupe">Guadeloupe</option>
                                        <option value="Guam">Guam</option>
                                        <option value="Guatemala">Guatemala</option>
                                        <option value="Guinea">Guinea</option>
                                        <option value="Guinea-Bissau">Guinea-Bissau</option>
                                        <option value="Guyana">Guyana</option>
                                        <option value="Haiti">Haiti</option>
                                        <option value="Honduras">Honduras</option>
                                        <option value="Hong Kong">Hong Kong</option>
                                        <option value="Hungary">Hungary</option>
                                        <option value="Iceland">Iceland</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Iran (Islamic Republic Of)">Iran (Islamic Republic Of)</option>
                                        <option value="Iraq">Iraq</option>
                                        <option value="Ireland">Ireland</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Jamaica">Jamaica</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Jordan">Jordan</option>
                                        <option value="Kazakhstan">Kazakhstan</option>
                                        <option value="Kenya">Kenya</option>
                                        <option value="Kiribati">Kiribati</option>
                                        <option value="Kuwait">Kuwait</option>
                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                        <option value="Laos">Laos</option>
                                        <option value="Latvia">Latvia</option>
                                        <option value="Lebanon">Lebanon</option>
                                        <option value="Lesotho">Lesotho</option>
                                        <option value="Liberia">Liberia</option>
                                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                        <option value="Liechtenstein">Liechtenstein</option>
                                        <option value="Lithuania">Lithuania</option>
                                        <option value="Luxembourg">Luxembourg</option>
                                        <option value="Macao">Macao</option>
                                        <option value="Macedonia">Macedonia</option>
                                        <option value="Madagascar">Madagascar</option>
                                        <option value="Malawi">Malawi</option>
                                        <option value="Malaysia">Malaysia</option>
                                        <option value="Maldives">Maldives</option>
                                        <option value="Mali">Mali</option>
                                        <option value="Malta">Malta</option>
                                        <option value="Marshall Islands">Marshall Islands</option>
                                        <option value="Martinique">Martinique</option>
                                        <option value="Mauritania">Mauritania</option>
                                        <option value="Mauritius">Mauritius</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Micronesia, Federated States Of">Micronesia, Federated States Of
                                        </option>
                                        <option value="Moldova, Republic Of">Moldova, Republic Of</option>
                                        <option value="Monaco">Monaco</option>
                                        <option value="Montenegro">Montenegro</option>
                                        <option value="Montserrat">Montserrat</option>
                                        <option value="Morocco">Morocco</option>
                                        <option value="Mozambique">Mozambique</option>
                                        <option value="Myanmar">Myanmar</option>
                                        <option value="Namibia">Namibia</option>
                                        <option value="Nauru">Nauru</option>
                                        <option value="Nepal">Nepal</option>
                                        <option value="Netherlands">Netherlands</option>
                                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                                        <option value="New Caledonia">New Caledonia</option>
                                        <option value="New Zealand">New Zealand</option>
                                        <option value="Nicaragua">Nicaragua</option>
                                        <option value="Niger">Niger</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Norfolk Island">Norfolk Island</option>
                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Oman">Oman</option>
                                        <option value="Pakistan">Pakistan</option>
                                        <option value="Panama">Panama</option>
                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                        <option value="Paraguay">Paraguay</option>
                                        <option value="Peru">Peru</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Poland">Poland</option>
                                        <option value="Portugal">Portugal</option>
                                        <option value="Qatar">Qatar</option>
                                        <option value="Republic of Korea">Republic of Korea</option>
                                        <option value="Reunion">Reunion</option>
                                        <option value="Romania">Romania</option>
                                        <option value="Russian Federation">Russian Federation</option>
                                        <option value="Rwanda">Rwanda</option>
                                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                        <option value="Saint Lucia">Saint Lucia</option>
                                        <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines
                                        </option>
                                        <option value="Samoa">Samoa</option>
                                        <option value="San Marino">San Marino</option>
                                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                        <option value="Senegal">Senegal</option>
                                        <option value="Serbia">Serbia</option>
                                        <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                                        <option value="Seychelles">Seychelles</option>
                                        <option value="Sierra Leone">Sierra Leone</option>
                                        <option value="Singapore">Singapore</option>
                                        <option value="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option>
                                        <option value="Slovakia">Slovakia</option>
                                        <option value="Slovenia">Slovenia</option>
                                        <option value="Solomon Islands">Solomon Islands</option>
                                        <option value="Somalia">Somalia</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="South Sudan">South Sudan</option>
                                        <option value="Spain">Spain</option>
                                        <option value="Sri Lanka">Sri Lanka</option>
                                        <option value="St. Helena">St. Helena</option>
                                        <option value="Sudan">Sudan</option>
                                        <option value="Suriname">Suriname</option>
                                        <option value="Swaziland">Swaziland</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Switzerland">Switzerland</option>
                                        <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                        <option value="Taiwan">Taiwan</option>
                                        <option value="Tajikistan">Tajikistan</option>
                                        <option value="Tanzania, United Republic Of">Tanzania, United Republic Of</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Timor-Leste">Timor-Leste</option>
                                        <option value="Togo">Togo</option>
                                        <option value="Tokelau">Tokelau</option>
                                        <option value="Tonga">Tonga</option>
                                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                        <option value="Tunisia">Tunisia</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Turkmenistan">Turkmenistan</option>
                                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                        <option value="Tuvalu">Tuvalu</option>
                                        <option value="U.S. Minor Islands">U.S. Minor Islands</option>
                                        <option value="Uganda">Uganda</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                        <option value="Uruguay">Uruguay</option>
                                        <option value="Uzbekistan">Uzbekistan</option>
                                        <option value="Vanuatu">Vanuatu</option>
                                        <option value="Venezuela">Venezuela</option>
                                        <option value="Vietnam">Vietnam</option>
                                        <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                        <option value="Yemen">Yemen</option>
                                        <option value="Zambia">Zambia</option>
                                        <option value="Zimbabwe">Zimbabwe</option>
                                    </select>
                                </div>
                            </div>
                            <!--- End Row 3 ---->

                            <!-- Inputs Hidden --->
                            <select style="visibility: hidden" id="lead_source" name="lead_source">
                                <option value="">--None--</option>
                                <option selected value="Digital">Digital</option>
                            </select>

                            <select style="visibility: hidden" id="00N4L000009iiyA" name="00N4L000009iiyA" title="Channel">
                                <option value="">--None--</option>
                                <option value="Call Center">Call Center</option>
                                <option value="Facebook">Facebook</option>
                                <option value="Instagram">Instagram</option>
                                <option selected value="Website">Website</option>
                            </select>

                            <!-- End Inputs Hidden --->

                            <!--- Row 4 ---->
                            {{-- <div class="col-md-11">
                                <label>{{ $lang == 'ar' ? '	الرسالة' : 'Message*' }}</label><br />
                                <textarea class="form-control" required name="message"></textarea>
                            </div> --}}
                            <div class="col-md-11">
                                <div class="g-recaptcha" data-sitekey="6LcjmmwiAAAAAJtv8WgmWf3OmvkV0XHo-pxojd7O"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-end">
                        <input type="submit" name="submit" value="{{ $lang == 'ar' ? 'إرسال ' : 'Submit' }}" class="mt-3">
                    </div>
                </div>
            </form>
        </div>
    </section>


@endsection

@section('js')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
