<?php header("X-Frame-Options: SAMEORIGIN"); header("Content-Security-Policy: frame-ancestors 'self'"); header("X-XSS-Protection: 1");?>
    <!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>SKY AD. Developments</title>
    <meta name="description" content="SKY AD. Developments is a leading regional real estate developer, under the direction of the Diamond Group, a renowned developer of local trading and industrial firms with a strong portfolio in the UAE.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ asset('/') }}apple-touch-icon.png">

    <link rel="stylesheet" href="{{ asset('/') }}fonts/stylesheet.css">
    <link rel="stylesheet" href="{{ asset('/') }}css/bootstrap.min.css">
    <style>
    </style>
    <link rel="stylesheet" href="{{ asset('/') }}css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}css/font-awesome.css">
    <!--  Essential META Tags -->

    <meta property="og:title" content="SKY AD. Developments">
    <meta property="og:description" content="SKY AD. Developments is a leading regional real estate developer, under the direction of the Diamond Group, a renowned developer of local trading and industrial firms with a strong portfolio in the UAE.">
    <meta property="og:image" content="{{ asset('/img/sky-ad-full-logo.png') }}">
    <meta property="og:url" content="">
    <meta name="twitter:card" content="">

    <!--  Non-Essential, But Recommended -->

    <meta property="og:site_name" content="SKY AD. Developments">
    <meta name="twitter:image:alt" content="SKY AD. Developments">

    <link rel="icon"
          type="image/png"
          href="{{ asset('/img/favicon-32x32.png') }}">

    @yield('css')

    <script src="{{ asset('/') }}js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

</head>
<body>


@if (session('status'))
    <div class="mb-4 font-medium text-sm text-green-600">
        {{ session('status') }}
    </div>
@endif

<div class="container text-center">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form method="POST" action="{{ route('login.custom') }}" class="pt-5 mt-5">
                @csrf
                <img src="{{ asset('img/marker.png') }}" width="70">

                @if(Session::has('error'))
                    <div class="alert-danger alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                <br/>
                <label>Username</label>
                <input id="email" class="form-control mb-3" type="email"  autocomplete="off" name="email" required autofocus />
                <label>Password</label>
                <input id="password" class="form-control mb-4" type="password"  autocomplete="off" name="password" required />

                <input class="form-control" type="submit" value="Login"/>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>
