@section('css2')
    <style>
        .tab-form {
            display: none;
        }
        .tab-form.active {
            display: block;
        }
    </style>
@endsection

<div class="filterbox">
    <div class="btns">
        <button class="tab-link  {{ isset($_GET['category']) ? ($_GET['category']=='residential' ? 'active' : '') : 'active' }}">{{  $lang=='ar' ? 'سكني' : 'RESIDENTIAL' }}</button>
        <button class="tab-link  {{ isset($_GET['category']) ? ($_GET['category']=='commercial' ? 'active' : '') : '' }}">{{  $lang=='ar' ? 'تجاري' : 'COMMERCIAL' }}</button>
    </div>
    <div class="fields">
        <div class="border-gray">

            @inject('contentService', 'App\Services\ContentProvider')
            <?php $types = $contentService->getTypes('en'); ?>
            <?php $communities = $contentService->getCommunities('en'); ?>

            <form action="{{ $lang == 'ar' ? url('/ar/property-listings') : url('property-listings') }}" method="get" class="tab-form {{ isset($_GET['category']) ? ($_GET['category']=='residential' ? 'active' : '') : 'active' }}">
                <input type="hidden" name="category" value="residential">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12 col">
                        <label>{{  $lang=='ar' ? 'نوع السكن' : 'PROPERTY TYPE' }}</label>
                        <div class="custom-select">
                            <select class="form-control" name="type">
                                <option value="">{{  $lang=='ar' ? 'الكل' : 'Any' }}</option>
                                @foreach($types as $row)
                                    <option value="{{ $row->slug }}" {{ isset($_GET['type']) ? ($_GET['type']==$row->slug ? 'selected' : '') : '' }}>{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 col">
                        <label>{{  $lang=='ar' ? 'الغرف ' : 'bedrooms' }}</label>
                        <div class="custom-select">
                            <select class="form-control" name="bedrooms">
                                <option value="">{{  $lang=='ar' ? 'الكل' : 'Any' }}</option>
                                <option value="1" {{ isset($_GET['bedrooms']) ? ($_GET['bedrooms']==1 ? 'selected' : '') : '' }}>1</option>
                                <option value="2" {{ isset($_GET['bedrooms']) ? ($_GET['bedrooms']==2 ? 'selected' : '') : '' }}>2</option>
                                <option value="3" {{ isset($_GET['bedrooms']) ? ($_GET['bedrooms']==3 ? 'selected' : '') : '' }}>3</option>
                                <option value="4" {{ isset($_GET['bedrooms']) ? ($_GET['bedrooms']==4 ? 'selected' : '') : '' }}>4</option>
                                <option value="5" {{ isset($_GET['bedrooms']) ? ($_GET['bedrooms']==5 ? 'selected' : '') : '' }}>5</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 col">
                        <label>{{  $lang=='ar' ? 'السعر ' : 'Price range' }}</label>
                        <div class="custom-select">
                            <select class="form-control" name="price">
                                <option value="">{{  $lang=='ar' ? 'الكل' : 'Any' }}</option>
                                <option value="1000000-2000000" {{ isset($_GET['price']) ? ($_GET['price']=='1000000-2000000' ? 'selected' : '' ) : '' }}>1,000,000 - 2,000,000</option>
                                <option value="2000000-3000000" {{ isset($_GET['price']) ? ($_GET['price']=='2000000-3000000' ? 'selected' : '') : '' }}>2,000,000 - 3,000,000</option>
                                <option value="3000000-4000000" {{ isset($_GET['price']) ? ($_GET['price']=='3000000-4000000' ? 'selected' : '') : '' }}>3,000,000 - 4,000,000</option>
                                <option value="4000000-5000000" {{ isset($_GET['price']) ? ($_GET['price']=='4000000-5000000' ? 'selected' : '') : '' }}>4,000,000 - 5,000,000</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 col">
                        <label>{{  $lang=='ar' ? 'المجمع' : 'community' }}</label>
                        <div class="custom-select">
                            <select class="form-control" name="community">
                                @foreach($communities as $row)
                                    <option value="{{ $row->slug }}" {{ isset($_GET['type']) ? ($_GET['community']==$row->slug ? 'selected' : '') : '' }}>{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" value="{{ $lang=='ar' ? 'ابحث ' : 'Search' }}" class="filter-submit">
                    </div>
                </div>
            </form>

            <form action="{{ url('property-listings') }}" method="get" class="tab-form {{ isset($_GET['category']) ? ($_GET['category']=='commercial' ? 'active' : '') : '' }}">
                <input type="hidden" name="category" value="commercial">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-12 col">
                        <label>{{  $lang=='ar' ? 'نوع السكن' : 'PROPERTY TYPE' }}</label>
                        <div class="custom-select">
                            <select class="form-control">
                                <option value="">{{  $lang=='ar' ? 'الكل' : 'Any' }}</option>
                                <option value="">{{  $lang=='ar' ? '' : 'Retail' }} </option>
                                <option value="">{{  $lang=='ar' ? '' : 'F&B' }} </option>
                                <option value="">{{  $lang=='ar' ? '' : 'Clinics' }} </option>
                                <option value="">{{  $lang=='ar' ? '' : 'Administrative offices' }}</option>
{{--                                <option value="">{{  $lang=='ar' ? '' : 'Serviced Apartments' }}</option>--}}
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 col">
                        <label>{{  $lang=='ar' ? 'السعر ' : 'Price range' }}</label>
                        <div class="custom-select">
                            <select class="form-control">
                                <option value="">{{  $lang=='ar' ? 'الكل' : 'Any' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 col">
                        <label>{{  $lang=='ar' ? 'المجمع' : 'community' }}</label>
                        <div class="custom-select">
                            <select class="form-control">
                                    <option value="Capital Avenue">{{  $lang=='ar' ? '' : 'Capital Avenue' }}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" value="{{  $lang=='ar' ? 'ابحث ' : 'Search' }}" class="filter-submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
