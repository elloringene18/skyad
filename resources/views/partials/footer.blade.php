
<footer id="footer">
    <div class="container padded">
        <div class="row">
            <div class="col-md-6 left">
                <a href="{{ url('/') }}"><img src="{{ asset('/') }}img/sky-ad-full-logo.png" width="200" id="footer-logo"></a>
            </div>
            <div class="col-md-6 right">
                <div class="socials mb-1">
                    <a target="_blank" href="https://www.facebook.com/SkyADDevelopments/" class="fa fa-facebook"></a>
                    <a target="_blank" href="https://twitter.com/skyadevelopment?lang=en" class="fa fa-twitter"></a>
                    <a target="_blank" href="https://www.instagram.com/skyad.developments/?hl=en" class="fa fa-instagram"></a>
                    <a target="_blank" href="https://www.linkedin.com/company/sky-ad-developments/" class="fa fa-linkedin"></a>
                    <a target="_blank" href="https://www.youtube.com/channel/UCmPyMnbLtmEYx7oNQ8n1kSA" class="fa fa-youtube"></a>
                </div>

                <ul class="">
                    <li><a href="{{ $lang=='ar' ? url('/ar/about-us') : url('/about-us') }}">{{  $lang=='ar' ? 'حولنا ' : 'About Us' }}</a></li>
{{--                    <li><a href="{{ $lang=='ar' ? url('/ar/projects') : url('/projects') }}">{{  $lang=='ar' ? 'المشاريع' : 'Projects' }}</a></li>--}}
                    <li><a href="{{ $lang=='ar' ? url('/ar/property-listings') : url('/property-listings') }}">{{  $lang=='ar' ? 'قوائم العقارات' : 'Property Listings' }}</a></li>
                    <li><a href="{{ $lang=='ar' ? url('/ar/news-and-press') : url('/news-and-press') }}">{{  $lang=='ar' ? 'الأخبار' : 'News & Press' }}</a></li>
                    <li><a href="{{ $lang=='ar' ? url('/ar/careers') : url('/careers') }}">{{  $lang=='ar' ? 'الوظائف' : 'Careers' }}</a></li>
                    <li><a href="{{ $lang=='ar' ? url('/ar/contact') : url('/contact') }}">{{  $lang=='ar' ? 'تواصل معنا ' : 'Contact Us' }}</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<script src="{{ asset('/') }}js/vendor/jquery-1.11.2.min.js"></script>
<script src="{{ asset('/') }}js/vendor/bootstrap.min.js"></script>
<script src="{{ asset('/') }}js/vendor/slick.min.js"></script>
<script src="{{ asset('/') }}js/main.js"></script>
<script SameSite="None; Secure" src="https://static.landbot.io/landbot-3/landbot-3.0.0.js"></script>
<script>
    var myLandbot = new Landbot.Livechat({
        configUrl: 'https://chats.landbot.io/v3/H-1006043-5VXMUWL8IOB7UAHF/index.json',
    });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Q3FT19CREL"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-Q3FT19CREL');
</script>

@yield('js')
</body>
</html>
