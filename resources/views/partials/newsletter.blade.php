<section id="newsletter" class="blue-bg">
    <div class="container padded text-center">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h1 class="heading">{{  $lang=='ar' ? 'ابق على اطلاع' : 'Stay updated' }}</h1>
                <span class="sub-heading">{{  $lang=='ar' ? 'اشترك في نشرتنا الإخبارية لتصلك آخر الأخبار والعروض. ' : 'Sign up for our newsletter to get the latest news and offers.' }}</span>

                <form id="newsletterform" class="mt-2" action="{{ url('/subscribe') }}" method="post">
                    @csrf
                    <input type="text" placeholder="{{ $lang=='ar' ? 'أدخل بريدك الإلكتروني' : 'Add your email' }}" name="email" required>
                    <input type="submit" value="">
                    <div class="alert alert-success" id="subscribeSuccess" style="display: none;"></div>
                    <div class="alert alert-success" id="subscribeError" style="display: none;"></div>
                </form>
                <p class="mt-3">
                    @if($lang=='ar')
                        باشتراكك توافق على تلقي آخر الأخبار والمستجدات من سكاي أبوظبي للتطوير العقاري.<br/>
                        اضغط هنا للاطلاع على سياسة الخصوصية الخاصة بنا. سترفق روابط إلغاء الاشتراك مع كل بريد إلكتروني يصلك.
                    @else
                        BY SIGNING UP FOR OUR MAILERS, YOU ARE AGREEING TO RECEIVE  NEWS,AND INFORMATION FROM SKY AD. DEVELOPMENTS.<br/>
                        CLICK HERE TO VISIT OUR PRIVACY POLICY. EASY UNSUBSCRIBE LINKS ARE PROVIDED IN EVERY MAIL.
                    @endif
</p>
            </div>
        </div>
    </div>
</section>
