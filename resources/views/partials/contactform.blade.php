<section id="contact" class="innercontent">
    <div class="container padded">
        <h1 class="heading">{{  $lang=='ar' ? '' : '' }}get in touch.</h1>
        <p class="sub-heading">{{  $lang=='ar' ? '' : '' }}O1 Mall, Mohammed Nagib Axis, First New Cairo. Cairo Governorate, Egypt.</p>
    </div>
    <div class="container">
        <div id="map"></div>
    </div>
    <div class="container padded">
        <form class="flat-form">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-5">
                            <label>{{  $lang=='ar' ? '' : '' }}Full Name*</label>
                            <input type="text" placeholder="Add your name" name="name" class="form-control">
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-5">
                            <label>{{  $lang=='ar' ? '' : '' }}Email*</label>
                            <input type="text" placeholder="Add your email" name="email" class="form-control">
                        </div>
                        <div class="col-md-5">
                            <label>{{  $lang=='ar' ? '' : '' }}Contact Number*</label>
                            <input type="text" placeholder="Add your contact number" name="phone" class="form-control">
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-5">
                            <label>{{  $lang=='ar' ? '' : '' }}Country*</label>
                            <div class="custom-select">
                                <select class="form-control">
                                    <option value="residential">United Arab Emirates</option>
                                    <option value="residential">United Arab Emirates</option>
                                    <option value="residential">United Arab Emirates</option>
                                    <option value="residential">United Arab Emirates</option>
                                    <option value="residential">United Arab Emirates</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-end">
                    <input type="submit" value="Submit" class="mt-3">
                </div>
            </div>
        </form>
    </div>
</section>
