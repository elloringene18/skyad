<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>SKY AD. Developments</title>
    <meta name="description" content="SKY AD. Developments is a leading regional real estate developer, under the direction of the Diamond Group, a renowned developer of local trading and industrial firms with a strong portfolio in the UAE.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ asset('/') }}apple-touch-icon.png">

    <link rel="stylesheet" href="{{ asset('/') }}fonts/stylesheet.css">

    @if($lang=='ar')
        <link rel="stylesheet" href="{{ asset('/') }}css/bootstrap.rtl.min.css">
        <style>
            body {
                font-family: 'Cairo';
                font-size: 18px;
                color: #575757;
                line-height: 22px;
            }

            p {
                font-family: 'Cairo';
                font-size: 18px;
                line-height: 21px;
            }

            h1,h2,h3,h4,h5,h5 {
                font-family: 'Cairo';
                text-transform: uppercase;
                letter-spacing: 0;
                font-weight: 900;
                line-height: 1;
            }

        </style>
    @else
        <link rel="stylesheet" href="{{ asset('/') }}css/bootstrap.min.css">
        <style>

            body {
                font-family: 'TT Commons';
                font-size: 18px;
                color: #575757;
                line-height: 22px;
            }

            p {
                font-family: 'TT Commons';
                font-size: 18px;
                line-height: 21px;
            }

            h1,h2,h3,h4,h5,h5 {
                font-family: 'Aeroport';
                text-transform: uppercase;
                letter-spacing: 0;
                font-weight: 900;
                line-height: 1;
            }

        </style>
    @endif

    <style>
    </style>
    <link rel="stylesheet" href="{{ asset('/') }}css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}css/font-awesome.css">
    <link rel="stylesheet" href="{{ asset('/') }}css/main.css?v={{ rand(1,9999999) }}">

    <link rel="stylesheet" href="{{ asset('/') }}css/slick.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}css/animation.css">
    <!--  Essential META Tags -->

    <meta property="og:title" content="SKY AD. Developments">
    <meta property="og:description" content="SKY AD. Developments is a leading regional real estate developer, under the direction of the Diamond Group, a renowned developer of local trading and industrial firms with a strong portfolio in the UAE.">
    <meta property="og:image" content="{{ asset('/img/sky-ad-full-logo.png') }}">
    <meta property="og:url" content="">
    <meta name="twitter:card" content="">

    <!--  Non-Essential, But Recommended -->

    <meta property="og:site_name" content="SKY AD. Developments">
    <meta name="twitter:image:alt" content="SKY AD. Developments">

    <link rel="icon"
          type="image/png"
          href="{{ asset('/img/favicon-32x32.png') }}">

    @yield('css')
    @yield('css2')


    @if($lang=='ar')
        <link rel="stylesheet" href="{{ asset('/') }}css/main-ar.css?v={{ rand(1,9999999) }}">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;500;700&family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">
    @endif

    <style>
        .LandbotLivechat {
            opacity: 0;
        }
        .LandbotLivechat.is-open {
            opacity: 1;
        }
    </style>
    <script src="{{ asset('/') }}js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="row" id="toptexts"><span></span></div>
<header class="container-fluid" id="header">
    <div class="row">
        <div class="col-lg-5 col-md-5 col-3">
            <nav class="text-xl-start">
                <ul class="navigation float-start">
                    <li><a href="{{ $lang=='ar' ? url('/ar/about-us') : url('/about-us') }}">{{  $lang=='ar' ? 'حولنا ' : 'About Us' }}</a></li>
                    <li>
                        <a href="#">{{  $lang=='ar' ? 'المشاريع' : 'Projects' }}</a>
                        <ul>
                            <li><a href="{{ $lang=='ar' ? url('/ar/residence-eight') : url('/residence-eight') }}">{{  $lang=='ar' ? 'Residence Eight' : 'Residence Eight' }}</a> </li>
                            <li><a href="{{ $lang=='ar' ? url('/ar/capital-avenue') : url('/capital-avenue') }}">{{  $lang=='ar' ? 'Capital Avenue' : 'Capital Avenue' }}</a> </li>
                        </ul>
                    </li>
                    <li><a href="{{ $lang=='ar' ? url('/ar/property-listings') : url('/property-listings') }}">{{  $lang=='ar' ? 'قوائم العقارات' : 'Property Listings' }}</a></li>
                </ul>
                <a href="#" id="nav-bt" onclick="toggleMenu(this)" class="">
                    <div></div>
                    <div></div>
                    <div></div>
                </a>
            </nav>
        </div>
        <div class="col-lg-2 col-md-2 col-6 text-center">
            <a href="{{ $lang=='ar' ? url('/ar/') : url('/') }}"><img src="{{ asset('/') }}img/sky-ad-logo.png"  id="main-logo" /></a>
        </div>
        <div class="col-lg-5 col-md-5 col-3 right">
            <nav class="text-xl-end">
                <ul id="langswitch">
{{--                    @if($lang=='ar')--}}
{{--                        <li><a href="{{ url('/') }}"><b>EN</b></a>/<a href="#">AR</a></li>--}}
{{--                    @else--}}
                        <li><a href="{{ url('/') }}"><b>EN</b></a>/<a href="{{ url('/ar') }}">AR</a></li>
{{--                    @endif--}}
                </ul>
                <ul class="navigation">
                    <li><a href="tel:16881"><i class="fa fa-phone"></i> 16881</a></li>
                    <li><a href="{{ $lang=='ar' ? url('/ar/contact') : url('/contact') }}">{{  $lang=='ar' ? 'للاستفسار ' : 'INQUIRE' }}</a></li>
                    <li><a href="{{ $lang=='ar' ? url('/ar/news-and-press') : url('/news-and-press') }}">{{  $lang=='ar' ? 'الأخبار' : 'News & Press' }}</a></li>
                </ul>
            </nav>
        </div>
        <ul id="main-nav">
            <li><a href="{{ $lang=='ar' ? url('/ar/about-us') : url('/about-us') }}">{{  $lang=='ar' ? 'حولنا ' : 'About Us' }}</a></li>
            <li><a href="{{ $lang=='ar' ? url('/ar/projects') : url('/projects') }}">{{  $lang=='ar' ? 'المشاريع' : 'Projects' }}</a></li>
            <li><a href="{{ $lang=='ar' ? url('/ar/property-listings') : url('/property-listings') }}">{{  $lang=='ar' ? 'قوائم العقارات' : 'Property Listings' }}</a></li>
            <li><a href="{{ $lang=='ar' ? url('/ar/news-and-press') : url('/news-and-press') }}">{{  $lang=='ar' ? 'الأخبار' : 'News & Press' }}</a></li>
            <li><a href="{{ $lang=='ar' ? url('/ar/careers') : url('/careers') }}">{{  $lang=='ar' ? 'الوظائف ' : 'Careers' }}</a></li>
            <li><a href="{{ $lang=='ar' ? url('/ar/contact') : url('/contact') }}">{{  $lang=='ar' ? 'تواصل معنا ' : 'Contact Us' }}</a></li>
        </ul>
    </div>
</header>

<a href="#" id="chat-icon"></a>

