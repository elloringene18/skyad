<section id="who-we-work-with" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 left"></div>
            <div class="col-md-6 text-center center">
                <h1>{!!   $lang=='ar' ? 'مع من نعمل:' : '<span class="blue-text">WHO WE</span> WORK WITH:'  !!} </h1>
            </div>

            <?php $data = $contentService->getPartners(); ?>

            <div class="col-md-3 text-center right">
                <div id="clients">
                    @foreach($data as $item)
                        <div class="slide">
                            <img src="{{ asset('/'.$item->image) }}" width="100%;">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
