@extends('admin.partials.master')

@section('css')
    <style>
        .modalcolor {
            width: 20px;
            height: 20px;
            margin-right: 5px;
            margin-bottom: -4px;
            display: inline-block;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
            margin-left: 5px;
        }
    </style>
@endsection
@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Floorplan</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/products/floorplans/update') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                <input type="hidden" value="{{ $item->id }}" name="property_id">

                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                        </div>
                    </div>
                @endif

                @if($errors->any())
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">English</h4>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="name" value="{{ $item->name }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Arabic</label>
                                    <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="name_ar" value="{{ $item->name_ar }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Image</label>
                                    <br/>
                                    @foreach($item->photos as $photo)
                                        <img src="{{ asset($photo->url) }}" width="300" class="mb-3"><br/>
                                    @endforeach
                                    <input type="file" class="form-control" name="image" data-lang="en" accept="image/png, image/jpeg, image/gif">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Area</label>
                                    <input type="text" class="form-control" name="area" placeholder="100" value="{{ $item->area }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Floors</label>
                                    <input type="text" class="form-control" name="floors" placeholder="First" value="{{ $item->floors }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <h5>Rooms</h5>
                                    <br/>
                                    <div id="rooms">
                                        @foreach($item->rooms as $room)
                                            <div class="row pb-3 room">
                                                <div class="col-md-4">
                                                    <label>Name</label>
                                                    <input type="text" class="form-control" name="rooms[{{$room->id}}][name]" placeholder="Entrance" maxlength="" value="{{$room->name}}">
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Size</label>
                                                    <input type="text" class="form-control" name="rooms[{{$room->id}}][value]" placeholder="100" maxlength="" value="{{$room->value}}">
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#" class="deleteRoom" style="margin-top:30px;display: inline-block" data-id="'+$length+'">X</a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row pb-3">
                                        <div class="col-md-6">
                                            <a href="#" class="addRooms">+Add more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('.addRooms').on('click',function (e) {
            e.preventDefault();
            $length = Math.floor(Math.random()*100000000000);

            $el = '\n' +
                '                                        <div class="row pb-3 room">\n' +
                '                                            <div class="col-md-4">\n' +
                '                                                <label>Name</label>\n' +
                '                                                <input type="text" class="form-control" name="rooms['+$length+'][name]" placeholder="Entrance" maxlength="" required>\n' +
                '                                            </div>\n' +
                '                                            <div class="col-md-4">\n' +
                '                                                <label>Size</label>\n' +
                '                                                <input type="text" class="form-control" name="rooms['+$length+'][value]" placeholder="100" maxlength="" required>\n' +
                '                                            </div>\n' +
                '                                            <div class="col-md-4">\n' +
                '                                                <a href="#" class="deleteRoom" style="margin-top:30px;display: inline-block" data-id="'+$length+'" >X</a>' +
                '                                            </div>\n' +
                '                                        </div>';

            $('#rooms').append($el);
            addDeleteEvent();
        });

        function addDeleteEvent(){
            $('.deleteRoom').unbind('click');
            $('.deleteRoom').on('click',function (e) {
                e.preventDefault();
                $(this).closest('.room').remove();
            });
        }
        addDeleteEvent();
    </script>
@endsection
