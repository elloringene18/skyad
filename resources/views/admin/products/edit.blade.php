@extends('admin.partials.master')

@section('css')
    <style>
        .modalcolor {
            width: 20px;
            height: 20px;
            margin-right: 5px;
            margin-bottom: -4px;
            display: inline-block;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
            margin-left: 5px;
        }
    </style>
@endsection
@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit {{$model}}</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/products/update') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                <input type="hidden" value="{!! $item->id !!}" name="product_id">

                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">English</h4>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title" value="{{ $item->title }}">
                                </div>

                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Description</label>
                                    <input type="hidden" name="description" value="{{ $item->description }}"/>
                                    <div class="summernote">
                                        {!! $item->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Arabic</label>
                                    <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title_ar" value="{{ $item->title_ar }}">
                                </div>

                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Description</label>
                                    <input type="hidden" name="description_ar" value="{{ $item->description_ar }}"/>
                                    <div class="summernote">
                                        {!! $item->description_ar !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Image</label>
                                    <br/>
                                    @if($item->photo)
                                        <img src="{{ asset($item->photo) }}" width="300">
                                    @endif
                                    <input type="file" class="form-control" name="image" data-lang="en" accept="image/png, image/jpeg, image/gif">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Area</label>
                                    <input type="text" class="form-control" name="area" placeholder="100" value="{{ $item->area }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Bedrooms</label>
                                    <input type="number" class="form-control" name="bedrooms" maxlength="2" value="{{ $item->bedrooms }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price" placeholder="1000000" maxlength="8" value="{{ $item->price }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control type_select" name="category_id" data-lang="en" id="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" {{ $item->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="row" id="attributes">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="form-control type_select" name="type_id" data-lang="en">
                                        @foreach($types as $type)
                                            <option value="{{ $type->id }}"  {{ $item->type_id == $type->id ? 'selected' : '' }}>{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Project</label>
                                    <select class="form-control type_select" name="community_id" data-lang="en">
                                        @foreach($communities as $community)
                                            <option value="{{ $community->id }}"  {{ $item->community_id == $community->id ? 'selected' : '' }}>{{ $community->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Interior Images</label>
                                    <br/>
                                    @if($item->galleryPhotos)
                                        @foreach($item->galleryPhotosFull as $id=>$slide)
                                            <div class="slide">
                                                <img src="{{$slide}}" height="300"> <a href="{{ url('admin/products/delete-slide/'.$id) }}">Remove</a>
                                            </div>
                                        @endforeach
                                    @endif

                                    <input type="file" class="form-control" name="gallery[]" data-lang="en" accept="image/png, image/jpeg, image/gif" multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
@endsection
