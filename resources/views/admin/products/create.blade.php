@extends('admin.partials.master')

@section('css')
    <style>
        .modalcolor {
            width: 20px;
            height: 20px;
            margin-right: 5px;
            margin-bottom: -4px;
            display: inline-block;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
            margin-left: 5px;
        }
    </style>
@endsection
@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Add {{$model}}</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/products/store') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">English</h4>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title" >
                                </div>

                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Description</label>
                                    <input type="hidden" name="description" value=""/>
                                    <div class="summernote">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Arabic</label>
                                    <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title_ar">
                                </div>

                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Description</label>
                                    <input type="hidden" name="description_ar" value=""/>
                                    <div class="summernote">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image" data-lang="en" accept="image/png, image/jpeg, image/gif">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Area</label>
                                    <input type="text" class="form-control" name="area" placeholder="100">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Bedrooms</label>
                                    <input type="number" class="form-control" name="bedrooms" maxlength="2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price" placeholder="1000000" maxlength="8">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control type_select" name="category_id" data-lang="en" id="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="row" id="attributes">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="form-control type_select" name="type_id" data-lang="en">
                                        @foreach($types as $type)
                                            <option value="{{ $type->id }}" {{ isset($_GET['type_id']) ? ($_GET['type_id']==$type->id ? 'selected' : '') : ''  }}>{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Project</label>
                                    <select class="form-control type_select" name="community_id" data-lang="en">
                                        @foreach($communities as $community)
                                            <option value="{{ $community->id }}" {{ isset($_GET['community_id']) ? ($_GET['community_id']==$community->id ? 'selected' : '') : ''  }}>{{ $community->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Interior Images</label>
                                    <input type="file" class="form-control" name="gallery[]" data-lang="en" accept="image/png, image/jpeg, image/gif" multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
@endsection
