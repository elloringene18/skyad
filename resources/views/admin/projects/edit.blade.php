@extends('admin.partials.master')

@section('css')
    <style>
        .deleteBt {
            position: absolute;
            left: -10px;
            background-color: #ff8181;
            display: block;
            width: 20px;
            height: 20px;
            text-align: center;
            line-height: 20px;
            border-radius: 50%;
            color: #fff;
            font-size: 12px;
            top: 5px;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Add a Category</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/categories/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $item->id }}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">English</label>
                                            <input value="{{ $item->name }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Arabic</label>
                                            <input value="{{ $item->name_ar }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="name_ar">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Parent Category</label>
                                    <select class="form-control type_select" name="parent_category_id" data-lang="en" id="category_id">
                                        <option value="0">None</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" {{ $item->parent ? ($item->parent->id == $category->id ? 'selected' : '') : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="row" id="attributes">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @foreach($item->attributes as $id=>$attribute)
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <p>Attributes</p>
                                                <input value="{{ $attribute->id }}" type="hidden" name="currentattributes[{{$id}}][id]" placeholder="English" class="form-control">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a href="{{ url('admin/categories/attribute/delete/'.$attribute->id) }}" class="deleteBt deleteAttribute">X</a>
                                                        <input value="{{ $attribute->name }}" type="text" name="currentattributes[{{$id}}][name]" placeholder="English" class="form-control">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input value="{{ $attribute->name_ar }}" type="text" name="currentattributes[{{$id}}][name_ar]" placeholder="Arabic" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <p>Attributes Values: </p>

                                                @foreach($attribute->values as $valueId=>$value)
                                                    <div class="row">
                                                        <input value="{{ $value->id }}" type="hidden" name="currentattributes[{{$id}}][values][{{$valueId}}][id]" placeholder="English" class="form-control">

                                                        <div class="col-md-6">
                                                            <a href="{{ url('admin/categories/value/delete/'.$value->id) }}" class="deleteBt deleteValue">X</a>
                                                            <input value="{{ $value->value }}" type="text" name="currentattributes[{{$id}}][values][{{$valueId}}][value]" placeholder="English" class="form-control">
                                                        </div>
                                                        <div class="col-md-6 relative">
                                                            <input value="{{ $value->value_ar }}" type="text" name="currentattributes[{{$id}}][values][{{$valueId}}][value_ar]" placeholder="Arabic" class="form-control">
                                                        </div>

                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>New Attribute Name: (Material, Art Style, etc.)</p>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][name]" placeholder="English" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][name_ar]" placeholder="Arabic" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>New Attributes Values: </p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][0][value]" placeholder="English" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][0][value_ar]" placeholder="Arabic" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][1][value]" placeholder="English" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][1][value_ar]" placeholder="Arabic" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][2][value]" placeholder="English" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][2][value_ar]" placeholder="Arabic" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
@endsection
