<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#products" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Properties</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="products">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/products') }}">View Properties</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/products/create/') }}">Add an Property</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/projects') }}">View Projects</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/projects/create/') }}">Add a Project</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/categories') }}">View Categories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/categories/create/') }}">Add a Category</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/types') }}">View Types</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/types/create/') }}">Add a Type</a>
                    </li>
                </ul>
            </div>
        </li>
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#administrators" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">About Page</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="administrators">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/administrators') }}">View Team Members</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/administrators/create/') }}">Add a Team Member</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/partners') }}">View Partners</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/partners/create/') }}">Add a Partner</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/about/') }}">Other Page Contents</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#news" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">News & Press</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="news">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/news') }}">View All News</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/news/create/') }}">Add an Article</a>
                    </li>
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/news/types') }}">Event Types</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/news/types/create/') }}">Add Event Types</a>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#partners" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Partners</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="partners">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/partners') }}">View All Partners</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/partners/create/') }}">Add a Partner</a>
                    </li>
                </ul>
            </div>
        </li>
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#media" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Media</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="media">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/media') }}">View All Media</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/media/create/') }}">Add a Media</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/media/types') }}">Media Types</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/media/types/create/') }}">Add Media Types</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/subscribers') }}">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Subscribers</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/contacts') }}">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Inquiries</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#settings" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Settings</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="settings">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/settings/home-items') }}">Home Categories</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/header/') }}">Header Settings</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/footer/') }}">Footer Settings</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/contact/') }}">Contact Settings</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
    </ul>
</nav>
