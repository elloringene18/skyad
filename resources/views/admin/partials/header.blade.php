<?php header("X-Frame-Options: SAMEORIGIN"); header("Content-Security-Policy: frame-ancestors 'self'");  header("X-XSS-Protection: 1; mode=block");?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SKYAD Portal Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/') }}/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="{{ asset('admin/') }}/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('admin/') }}/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('admin/') }}/images/favicon.png" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- include summernote css/js -->
    <link href="{{ asset('admin/css') }}/summernote.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('admin/css') }}/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('admin/css') }}/admin.css">

    @yield('css')
</head>

<body>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-left" style="border-bottom:1px solid #ccc">
            <a href="#" style="color: #8e8e8e; font-size: 26px; margin-top:10px; text-align:left;padding-left: 40px;">SKYAD</a>
        </div>
        <div class="navbar-menu-wrapper align-items-center">
            <a href="{{ url('logout') }}" style="float:right; color: #fff; margin-top:20px">Logout</a>
        </div>
    </nav>
