
<!-- partial:partials/_footer.html -->
<footer class="footer">
    <div class="container-fluid clearfix">
		<span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
			<a href="http://www.thisishatch.com/" target="_blank">Hatch</a>. All rights reserved.
	    </span>
    </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="{{ asset('admin') }}/vendors/js/vendor.bundle.base.js"></script>
<script src="{{ asset('admin') }}/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ asset('admin') }}/js/off-canvas.js"></script>
<script src="{{ asset('admin') }}/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{ asset('admin') }}/js/dashboard.js"></script>
<!-- End custom js for this page-->
<!-- include summernote css/js -->
<script src="{{ asset('admin') }}/js/jquery.js"></script>
<script src="{{ asset('admin') }}/js/bootstrap.js"></script>
<script src="{{ asset('admin') }}/js/summernote.js"></script>
<script src="{{ asset('admin') }}/js//jquery-ui.js"></script>
<!-- include summernote-ko-KR -->
<script src="{{ asset('admin') }}/lang/summernote-ar-AR.js"></script>
<script src="{{ asset('admin') }}/js/main.js"></script>
</body>

@yield('js')
</html>
