@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Article</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/news/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $item->id }}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Title EN</label>
                                            <input value="{{$item->title}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="title">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Title AR</label>
                                            <input value="{{$item->title_ar}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="title_ar">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Content EN</label>
                                            <input type="hidden" name="content" value="{{ $item->content }}"/>
                                            <div class="summernote">
                                                {!! $item->content !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Content AR</label>
                                                <input type="hidden" name="content_ar" value="{{ $item->content_ar }}"/>
                                                <div class="summernote">
                                                    {!! $item->content_ar !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Date</label>
                                            <input value="{{$item->publish_date}}" type="text" id='datetimepicker4'  class="form-control" placeholder="Name" name="publish_date">

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Article Type</label>
                                                <select class="form-control type_select" name="article_type_id" data-lang="en" id="article_type_id">
                                                    @foreach($types as $type)
                                                        <option value="{{ $type->id }}" {{ $type->id == $item->type->id ? 'selected' : '' }}>{{ $type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 grid-margin stretch-card">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Photo</label>
                                                    @if($item->photo)
                                                        <br/>
                                                        <img src="{{asset(''.$item->photo)}}" width="200">
                                                        <br/>
                                                        <br/>
                                                        <label>Delete photo: <input type="checkbox" name="delete_photo" value="true"></label>
                                                        <br/>
                                                        <br/>
                                                    @endif
                                                    <input type="file" class="form-control" name="photo" data-lang="en" accept="image/png, image/jpeg, image/gif">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $('#datetimepicker4').datepicker();
    </script>
@endsection
