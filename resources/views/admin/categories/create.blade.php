@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Add a Category</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/categories/store') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">English</label>
                                            <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Arabic</label>
                                            <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="name_ar">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Parent Category</label>
                                    <select class="form-control type_select" name="parent_category_id" data-lang="en" id="category_id">
                                        <option value="0">None</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                        @endforeach
                                    </select>
                                    <div class="row" id="attributes">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Attribute Name: (Material, Art Style, etc.)</p>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][name]" placeholder="English" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][name_ar]" placeholder="Arabic" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Attributes Values: </p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][0][value]" placeholder="English" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][0][value_ar]" placeholder="Arabic" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][1][value]" placeholder="English" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][1][value_ar]" placeholder="Arabic" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][2][value]" placeholder="English" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="attributes[0][values][2][value_ar]" placeholder="Arabic" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('admin/js/file-upload.js') }}"></script>
@endsection
