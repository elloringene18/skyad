@if(count($data))
    <table>
        <thead>
        <tr>
            @foreach($data['headers'] as $item)
                <th>{{$item}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($data['rows'] as $entry)
            <tr>
                <td>{{$entry['date']->format('Y-m-d')}}</td>
                @foreach($entry['items'] as $item)
                    <td>{{ $item }}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <table>
        <thead>
        <tr>
            <th>No Entries</th>
        </tr>
        </thead>
    </table>
@endif
