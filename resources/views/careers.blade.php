@extends('partials.master')

@section('css')
<link rel="stylesheet" href="{{ asset('/') }}css/inner.css?v=1.1">
<style>

    #intro-img {
        margin-top: 110px;
    }
</style>
<style>
    .this {
        opacity: 0;
        position: absolute;
        top: 0;
        left: 0;
        height: 0;
        width: 0;
        z-index: -1;
    }
</style>
@endsection

@section('content')

<section id="intro-img">
    <img src="{{ asset('img/careers/banner.jpg') }}" width="100%">
</section>

<div class="container padded pb-1  center-mobile">
    <div class="row">
        <h1 class="heading">{{ $lang == 'ar' ? 'الوظائف' : 'Careers' }}</h1>
    </div>
</div>

<div class="overflow-hidden relative  center-mobile">
    <div class="container">
        <div class="row">
            <div class="col-md-12 pt-3">
                @if($lang=='ar')
                    <p>هل تبحث عن فرصتك المهنية التالية؟ نحن نتطلع لتوظيف أفراد ملتزمين وشغوفين. نحن نهدف لوضع معيار للعيش المتكامل في مصر ونسعى لتطوير سوق العقارات الوطني. انضم إلينا وكن جزءاً من هذه المهمة</p>
                @else
                    <p>Looking for your next professional opportunity? We’re looking to hire committed
                        individuals who are passionate about what they do. We aim to set the benchmark for integrated living
                        in Egypt and are on a mission to evolve our national
                        real estate market to exceed international
                        standards.</p>
                    <p>Join us to be a part of this, to develop, enhance our potential and boost your success.</p>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 pt-3 pb-5">
                <form class="flat-form" action="{{url('careers/submit')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-md-12">
                            @if(Session::has('message'))
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            @csrf

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-5">
                                    <label>{{  $lang=='ar' ? '	الاسم الكامل' : 'Full Name' }}</label>
                                    <input type="text" placeholder="{{ $lang == 'ar' ? 'الاسم' : 'Add your name' }}" name="name" class="form-control" required>
                                </div>
                                <div class="col-md-5">
                                    <label>{{  $lang=='ar' ? 'البريد الإلكتروني' : 'Email' }}*</label>
                                    <input type="text" placeholder="{{ $lang == 'ar' ? 'البريد الإلكتروني' : 'Add your email' }}" name="email" class="form-control" required>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <label>{{  $lang=='ar' ? '	رقم التواصل' : 'Contact Number' }}*</label>
                                    <input type="text" placeholder="{{ $lang == 'ar' ? ' ادخل رقم التواصل' : 'Add your contact number' }}" name="phone" class="form-control mobilenumber" required value="">
                                </div>
                                <div class="col-md-5">
                                    <label>{{  $lang=='ar' ? 'لتحميل السيرة الذاتية (الحد الأقصى لحجم الملف 2MB)' : 'Upload CV* (Max file size 2MB)' }}</label>
                                    <input type="file" placeholder="Word, PDF max size: 2mb" name="cv" required class="custom-file-input" accept=".doc,.docx,.pdf,application/msword">
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                </div>
                                <div class="col-md-10">
                                    <label>{{  $lang=='ar' ? '	الرسالة' : 'Message' }}*</label><br/>
                                    <textarea class="form-control" required name="message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input name="user_id" type="text" id="firstname" class="this" autocomplete="off" tabindex="-1">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 text-end">
                            <input type="submit" value="{{ $lang == 'ar' ? 'إرسال' : 'Submit'  }}" class="mt-3">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $('#slider').slick({
            slidesToScroll: 1,
            slidesToShow: 1,
            infinite: false,
            arrows: false,
            autoplay: true,
            adaptiveHeight: true,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            autoplaySpeed: 2000,
        });
    });


    $(window).on('resize',function(){
        alignContent();
    });

    $(window).on('load',function(){
        alignContent();
    });

    function alignContent(){
        imgHeight = $('#scroll-items .item .text-box').closest('.item').find('img').height();
        $('#scroll-items .item .text-box').css('height',imgHeight);
    }

    $(document).ready(function(){
        $('#scroll-items').slick({
            slidesToScroll: 1,
            slidesToShow: 1,
            variableWidth: true,
            infinite: false,
            appendArrows: '.arrows',
            autoplay: true,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            autoplaySpeed: 2000,
        });

        alignContent();
    });

</script>
@endsection
