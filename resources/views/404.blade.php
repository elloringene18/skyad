@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/') }}css/inner.css?v=1.1">
@endsection

@section('content')
    <section class="innercontent padded">
        <div class="container">
            <h1>{{  $lang=='ar' ? '' : '' }}Page not found</h1>
        </div>
    </section>
@endsection

@section('js')
@endsection
